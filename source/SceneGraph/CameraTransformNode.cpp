/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "CameraTransformNode.h"

#include "../Defines.h"

#include "gtc/matrix_access.hpp"
#include "gtc/matrix_transform.hpp"
#include "gtx/rotate_vector.hpp"
#include <cmath>

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

CameraTransformNode::CameraTransformNode(GLFWwindow* p_glfwWindow, double p_movementSpeed, double p_mouseSpeed)
  : TransformNode(), m_glfwWindow(p_glfwWindow), m_movementSpeed(p_movementSpeed), m_mouseSpeed(p_mouseSpeed),
    m_mouseControlGained(false), setCanFly(SET_CAN_FLY)
{}

CameraTransformNode::~CameraTransformNode()
{
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

std::string CameraTransformNode::GetNodeType()
{
  return "TransformNode";
}


void CameraTransformNode::Update(Timer* pTime)
{
  float elapsedTime = pTime->GetElapsedTime();

  // window size
  int windowWidth, windowHeight;
  glfwGetWindowSize(m_glfwWindow, &windowWidth, &windowHeight);


  // new mouse position
  double xpos, ypos;
  glfwGetCursorPos(m_glfwWindow, &xpos, &ypos);

  // reset cursor position
  glfwSetCursorPos(m_glfwWindow, double(windowWidth)/2.0, double(windowHeight)/2.0 );

  // positive for move right
  double horizontalRotAngle = m_mouseSpeed * (xpos - double(windowWidth)/2.0);
  // positive for move upwards
  double verticalRotAngle = m_mouseSpeed * (double(windowHeight)/2.0 - ypos);


  // need to wait until we get control of the mouse and have once set it to the center
  if (!m_mouseControlGained)
  {
    // no movement yet
    horizontalRotAngle = 0.0;
    verticalRotAngle = 0.0;

    // querry position again
    double xpos_, ypos_;
    glfwGetCursorPos(m_glfwWindow, &xpos_, &ypos_);

    if (xpos_ == double(windowWidth)/2.0 && ypos_ == double(windowHeight)/2.0)
    {
      // have captured the mouse
      m_mouseControlGained = true;
    }
  }


  // rotations
  glm::mat4 prevTransform = GetTransformation();

  // extract translation
  glm::vec4 prevTranslation = prevTransform[3];

  // etract directions from previous matrix
  glm::vec4 viewDir = - prevTransform[2];
  glm::vec3 viewDir_( viewDir[0], viewDir[1], viewDir[2] );

  glm::vec4 up = prevTransform[1];
  glm::vec3 up_( up[0], up[1], up[2] );

  glm::vec4 right = prevTransform[0];
  glm::vec3 right_( right[0], right[1], right[2] );

  // absolute up vector, for rotation and stabilization
  glm::vec3 up_abs(0.0, 1.0, 0.0);

  // rotate horizontally
  viewDir_ = glm::rotate(viewDir_, float(-horizontalRotAngle), up_abs);
  up_ = glm::rotate(up_, float(-horizontalRotAngle), up_abs);
  right_ = glm::rotate(right_, float(-horizontalRotAngle), up_abs);

  // correct right vector
  right_ = glm::cross( glm::cross(up_abs, right_), up_abs );
  right_ = glm::normalize(right_);

  // vector for forward movement
  glm::vec3 front_ = glm::cross(up_abs, right_);
  front_ = glm::normalize(front_);

  // rotate vertically
  viewDir_ = glm::rotate(viewDir_, float(verticalRotAngle), right_);
  up_ = glm::rotate(up_, float(verticalRotAngle), right_);

  // correct viewDir vector
  viewDir_ = glm::cross( glm::cross(right_, viewDir_), right_ );
  viewDir_ = glm::normalize(viewDir_);

  // ensure, one cannot view all up or all down
  float criticalDot = CRITICAL_DOT;
  float dot = glm::dot(up_abs, viewDir_);
  if (dot > criticalDot)
  {
    // set the vector back to a non-critical angle
    viewDir_[1] = criticalDot;
    viewDir_ = glm::normalize(viewDir_);
  }
  else if (dot < -criticalDot)
  {
    // set the vector back to a non-critical angle
    viewDir_[1] = -criticalDot;
    viewDir_ = glm::normalize(viewDir_);
  }

  // ensure we do not flip over to the other side
  if ( glm::dot(front_, viewDir_) < 0.0 )
  {
    viewDir_[0] = -viewDir_[0];
    viewDir_[2] = -viewDir_[2];
    // project on critical angle
    viewDir_[1] = criticalDot * viewDir_[1] / std::abs(viewDir_[1]);
    viewDir_ = glm::normalize(viewDir_);
  }

  // correct up vector
  up_ = glm::cross(right_, viewDir_);
  up_ = glm::normalize(up_);

  // 4D vectors
  viewDir = glm::vec4(viewDir_[0], viewDir_[1], viewDir_[2], 0.0);
  right = glm::vec4(right_[0], right_[1], right_[2], 0.0);
  up = glm::vec4(up_[0], up_[1], up_[2], 0.0);
  glm::vec4 front( front_[0], front_[1], front_[2], 0.0 );

  // cheater
  if (setCanFly)
  {
    front = viewDir;
  }


  // Move forward
  if (glfwGetKey( m_glfwWindow, GLFW_KEY_UP ) == GLFW_PRESS
      || glfwGetKey( m_glfwWindow, GLFW_KEY_W ) == GLFW_PRESS)
  {
    prevTranslation += front * elapsedTime * float(m_movementSpeed);
  }
  // Move backward
  if (glfwGetKey( m_glfwWindow, GLFW_KEY_DOWN ) == GLFW_PRESS
      || glfwGetKey( m_glfwWindow, GLFW_KEY_S ) == GLFW_PRESS)
  {
    prevTranslation -= front * elapsedTime * float(m_movementSpeed);
  }
  // Strafe right
  if (glfwGetKey( m_glfwWindow, GLFW_KEY_RIGHT ) == GLFW_PRESS
      || glfwGetKey( m_glfwWindow, GLFW_KEY_D ) == GLFW_PRESS)
  {
    prevTranslation += right * elapsedTime * float(m_movementSpeed);
  }
  // Strafe left
  if (glfwGetKey( m_glfwWindow, GLFW_KEY_LEFT ) == GLFW_PRESS
      || glfwGetKey( m_glfwWindow, GLFW_KEY_A ) == GLFW_PRESS)
  {
    prevTranslation -= right * elapsedTime * float(m_movementSpeed);
  }


  // set the new transformation matrix = inverse lookAt matrix
  glm::mat4 newTransformation(right, up, -viewDir, prevTranslation);
  SetTransformation(newTransformation);
}


glm::vec3 CameraTransformNode::GetViewDir()
{
  glm::mat4 m4Transform = this->GetTransformation();
  glm::vec4 v4ViewDir = glm::column(m4Transform, 2);

  return (- glm::vec3(v4ViewDir.x, v4ViewDir.y, v4ViewDir.z));
}

double CameraTransformNode::getMovementSpeed()
{
  return m_movementSpeed;
}

double CameraTransformNode::getMouseSpeed()
{
  return m_mouseSpeed;
}

void CameraTransformNode::setMovementSpeed(double p_movementSpeed)
{
  m_movementSpeed = p_movementSpeed;
}

void CameraTransformNode::setMouseSpeed(double p_mouseSpeed)
{
  m_mouseSpeed = p_mouseSpeed;
}

bool CameraTransformNode::isMouseControlGained()
{
  return m_mouseControlGained;
}

void CameraTransformNode::setMouseControlGained(bool p_mouseControlGained)
{
  m_mouseControlGained = p_mouseControlGained;
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
