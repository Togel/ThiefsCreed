#ifndef _SKYBOX_
#define _SKYBOX_

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/


/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/
class Node;
class PlayerNode;
class ResourceCollection;

/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class Skybox
{
public:
  /*
  * Constructor.
  */
  Skybox(PlayerNode* pPlayer, ResourceCollection* pCollection);

  /*
  * Destructor.
  */
  virtual ~Skybox();

private:

  /*
  * Construct the Skybox Geometry and places it in the SceneGraph.
  * @param 'pPlayer' defines the Player Node at which Parent the Skybox get rooted.
  * @param 'pCollection' Collection storing all Meshes and Textures.
  */
  void PlaceInSceneGraph(PlayerNode* pPlayer, ResourceCollection* pCollection);
};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
#endif