#ifndef _SHADOW_MAPPING_CALLBACK_
#define _SHADOW_MAPPING_CALLBACK_

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "Callback.h"

#include <glm.hpp>

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/
class LightManager;
class Node;
class CameraNode;
class ResourceCollection;
class Shader;

/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class ShadowMappingCallback : public Callback
{
public:
  ShadowMappingCallback(LightManager* pLightManager
    , Node* pSceneGraphNode
    , ResourceCollection* pResCollection
    , CameraNode* pCamNode);

  virtual ~ShadowMappingCallback();

  virtual void HandleEvents(Timer* pTimer);


private:
  LightManager* m_pLightManager;
  Node* m_pSceneGraphRoot;
  ResourceCollection* m_pResourceCollection;
  CameraNode* m_pCameraNode;

  Shader* m_pShader;

  glm::mat4 depthProjectionMatrix;
  glm::mat4 depthViewMatrix;

  void RecDraw(Node* pNode, glm::mat4 transformMat);

  void FillCameraUniformBuffer();


};


/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
#endif
