set( RelativeDir "source" )
set( RelativeSourceGroup "SourceFiles" )

set(Directories
  Callbacks
  DeferredShading
  ParticleSystem
  SceneGraph 
  RessourceManagement 
  Map 
)

set(DirFiles
  _Main.cpp
  AI.h
  AI.cpp
  CollisionHandling.cpp
  CollisionHandling.h
  Defines.h
  GameManager.h
  GameManager.cpp
  LightManager.cpp
  LightManager.h
  Player.h
  Player.cpp
  Settings.h
  Skybox.h
  Skybox.cpp
  TC.h
  TC.cpp
  TCSystem.h
  TCSystem.cpp
  ThreadDraw.h
  ThreadDraw.cpp
  ThreadGeometry.h
  ThreadGeometry.cpp
  Timer.h
  Timer.cpp
  _SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )
	
set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()

source_group( ${RelativeSourceGroup} FILES ${LocalSourceGroupFiles} )

foreach( subdir ${Directories})
	include("${RelativeDir}/${subdir}/_SourceFiles.cmake")
endforeach()

set( RelativeDir "")

