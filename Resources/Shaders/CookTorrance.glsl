#version 420 core

const int MAX_LIGHTS = 10;

struct Light
{
	vec3 position;
	float attenuation;
	vec3 direction;
	float coneAngle;
	vec3 intensity;
	float ambientCoefficient;
	float light_type;
	float padding1;
	float padding2;
	float padding3;
};

layout(std140) uniform Lights{
    Light lightArray[10];
	float numLights;
	float lightsPadding1;
	float lightsPadding2;
	float lightsPadding3;
};

vec4 CookTorrance(float RoughnessVal, float F0, float k, vec3 normal, vec3 position, vec3 eyeDir)
{
	vec4 color = vec4(0.1, 0.1, 0.1, 1.0); //  Ambient Term dark uniform-colored light.

	

	for(int i = 0; i < numLights; i++)
	{
	
		vec3 lightDirection = position - lightArray[i].position;
		vec3 lightColor = lightArray[i].intensity;

		// do the lighting calculation for each fragment.
		float NdotL = max(dot(normal, lightDirection), 0.0);
    
		float specular = 0.0;
		float roughness = 0.0;
		float geoAtt = 0.0;
		float fresnel = 0.0;
		float NdotV = 0.0;
		if(NdotL > 0.0)
		{
			

			// calculate intermediary values
			vec3 halfVector = normalize((-lightDirection) + eyeDir);
			float NdotH = max(dot(normal, -halfVector), 0.0); 
			NdotV = max(dot(normal, eyeDir), 0.0); // note: this could also be NdotL, which is the same value
			float VdotH = max(dot(eyeDir, halfVector), 0.0);
			float mSquared = RoughnessVal * RoughnessVal;
        
			// geometric attenuation
			float NH2 = 2.0 * NdotH;
			float g1 = (NH2 * NdotV) / VdotH;
			float g2 = (NH2 * NdotL) / VdotH;
			geoAtt = min(1.0, min(g1, g2));
     
			// roughness (or: microfacet distribution function)
			// beckmann distribution function
			float r1 = 1.0 / ( 4.0 * mSquared * pow(NdotH, 4.0));
			float r2 = (NdotH * NdotH - 1.0) / (mSquared * NdotH * NdotH);
			roughness = r1 * r2;
        
			// fresnel
			// Schlick approximation
			fresnel = pow(1.0 - VdotH, 5.0);
			fresnel *= (1.0 - F0);
			fresnel += F0;
        
			specular = (fresnel * geoAtt * roughness) / (NdotV * NdotL * 3.14);
		}
    
		//color += vec4(lightColor * NdotL * (k + specular * (1.0 - k)), 0.0);
		color += vec4(lightColor * k * NdotL * (fresnel * roughness * geoAtt) / (NdotV * NdotL), 1.0);
		//color += vec4(eyeDir , 1.0);
	}
	

	return color;
}

