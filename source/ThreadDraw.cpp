#define OPENGL_VERSION 42
#define SAMPLES 4

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "GL/glew.h"
#include "Callbacks/Callback.h"
#include "DeferredShading/GBuffer.h"
#include "DeferredShading/DeferredShading.h"
#include "RessourceManagement/Geometry.h"
#include "SceneGraph/CameraNode.h"
#include "SceneGraph/Node.h"
#include "SceneGraph/TransformNode.h"
#include "SceneGraph/DrawableNode.h"
#include "glm.hpp"
#include "ThreadDraw.h"
#include "TC.h"
#include "GameManager.h"
#include "SceneGraph/MeshNode.h"
#include "RessourceManagement/ResourceCollection.h"




/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

ThreadDraw::ThreadDraw(GameManager* pGameManager)
	: m_pGameManager(pGameManager)
{
  Init();
  m_tLastTime = std::chrono::high_resolution_clock::now();
}
  
ThreadDraw::~ThreadDraw()
{
  glfwTerminate();

  for (Callback* pCallback : m_vecPreDrawCallbacks)
  {
    delete pCallback;
  }
  m_vecPreDrawCallbacks.clear();

  for (Callback* pCallback : m_vecPostDrawCallbacks)
  {
    delete pCallback;
  }
  m_vecPostDrawCallbacks.clear();
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

void ThreadDraw::Init()
{
  // Create the Window
  if (!CreateGLFWWindow())
  {
    glfwTerminate();
    exit(-1);
  }

  glewExperimental = true;
  if (glewInit() != GLEW_OK)
  {
    tc::crs << "Glew could not be initialized!" << tc::endl;
    exit(-1);
  }

  // Enable or disable V-Sync.
  glfwSwapInterval(0); // REPLACE WITH CONFIG INFO.
  //tc::war << "ThreadDraw::Init -> Missing Code" << tc::endl;

  GLuint VertexArrayID;
  glGenVertexArrays(1, &VertexArrayID);
  glBindVertexArray(VertexArrayID);

  // OpenGL State.
  glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // Black Background.
  glEnable(GL_DEPTH_TEST);


  // pass window to game manager
  m_pGameManager->SetGlfwWindow(m_pWindow);

  // set cursor position
  glfwSetCursorPos(m_pWindow, double(m_v2WindowSize.x)/2.0, double(m_v2WindowSize.y)/2.0 );
  // hide cursor
  glfwSetInputMode(m_pWindow, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
}

void ThreadDraw::Run()
{
  m_pGBuffer->BindForWriting(); // bind Gbuffer
  m_pGBuffer->ClearAllBuffers(); // clear it
  glBindFramebuffer(GL_FRAMEBUFFER, 0); // bind standard buffer

  // clear standard framebuffer
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // Run Pre Draw Callbacks
  for (Callback* pCallback : m_vecPreDrawCallbacks)
    pCallback->Execute();

  // Draw Routine for deferred shading
  DSGeometryPass();
  DSLightPass();

  m_pGBuffer->CopyDepthToStandardBuffer();

  GLenum status = glGetError();
  if (status != 0)
  {
    tc::err << status << tc::endl;
  }

  // Draw routine for forward rendering
  // ***** vvv Draw whatever needs forward rendering vvv ***



  // ***** ^^^ Draw whatever needs forward rendering ^^^ ****

  // Run Post Draw Callbacks.
  for (Callback* pCallback : m_vecPostDrawCallbacks)
    pCallback->Execute();

  glfwSwapBuffers(m_pWindow);
  glfwPollEvents();

  // Update Window Title with FPS Information.
  m_iFrames++;
  std::chrono::high_resolution_clock::time_point tCurTime = std::chrono::high_resolution_clock::now();
  std::chrono::microseconds us = std::chrono::duration_cast<std::chrono::microseconds>(tCurTime - m_tLastTime);
  
  float fDeltaTus = static_cast<float>(us.count());

  // Update Title each Second.
  if (fDeltaTus >= 1000000.0f)
  {
    float m_fFPS = floor(100.0f * static_cast<float>(m_iFrames) / (fDeltaTus / 1000000.0f)) / 100.0f;

    std::stringstream sTitle;
    sTitle << "Thief's Creed - " << m_fFPS << "FPS";

    glfwSetWindowTitle(m_pWindow, sTitle.str().c_str());
    
    m_tLastTime = tCurTime;
    m_iFrames = 0;
  }  
}

void ThreadDraw::SetupDeferredShading()
{
  m_pGBuffer = new GBuffer();
  m_pGBuffer->Init(WINDOW_WIDTH, WINDOW_HEIGHT);

  m_pDeferredShading = new DeferredShading(m_pGameManager->GetLightManager(), m_pGBuffer, m_pGameManager->GetCameraNode());
}

void ThreadDraw::recDraw(Node* pNode, glm::mat4 matTransformation)
{
  // Only process active Nodes.
  if (!pNode->IsActive())
    return;

	if (pNode->GetNodeType() == "TransformNode")
	{
		TransformNode* pTransfNode = dynamic_cast<TransformNode*>(pNode);
		matTransformation = matTransformation * pTransfNode->GetTransformation();
	}
	else if (pNode->GetNodeType() == "DrawableNode")
	{
		DrawableNode* pDrawableNode = dynamic_cast<DrawableNode*>(pNode);

		// TODO: Draw Transparent should be considered also...
    pDrawableNode->DrawOpaque(matTransformation);
	}

	for (Node* child : pNode->GetChildren())
	{
		recDraw(child, matTransformation);
	}

}

void ThreadDraw::DSGeometryPass()
{

  m_pGBuffer->BindForWriting();

	Node* rootNode = m_pGameManager->GetSceneGraphRoot();
	recDraw(rootNode, glm::mat4());

}

void ThreadDraw::DSLightPass()
{

 // m_pGBuffer->CopyDepthToStandardBuffer();

  GLenum status = glGetError();
  if (status != 0)
  {
    tc::err << status << tc::endl;
  }

  glBindFramebuffer(GL_FRAMEBUFFER, 0);

  m_pDeferredShading->DoShading();
}

std::vector<Callback*>& ThreadDraw::GetPreDrawCallbacks()
{
  return m_vecPreDrawCallbacks;
}

std::vector<Callback*>& ThreadDraw::GetPostDrawCallbacks()
{
  return m_vecPostDrawCallbacks;
}

void ThreadDraw::AddPreDrawCallback(Callback* pCallback)
{
  m_vecPreDrawCallbacks.push_back(pCallback);
}

void ThreadDraw::AddPostDrawCallback(Callback* pCallback)
{
  m_vecPostDrawCallbacks.push_back(pCallback);
}

bool ThreadDraw::DeleteCallback(Callback* pCallback)
{
  // Search the Callback in the Vectors and delete it.
  for (std::vector<Callback*>::iterator it = m_vecPreDrawCallbacks.begin(); it != m_vecPreDrawCallbacks.end(); ++it)
  {
    if ((*it) == pCallback)
    {
      m_vecPreDrawCallbacks.erase(it);
      delete (*it);
      
      return true;
    }
  }

  for (std::vector<Callback*>::iterator it = m_vecPostDrawCallbacks.begin(); it != m_vecPostDrawCallbacks.end(); ++it)
  {
    if ((*it) == pCallback)
    {
      m_vecPostDrawCallbacks.erase(it);
      delete (*it);

      return true;
    }
  }

  // Callback not found.
  return false;
}

void ThreadDraw::SetGLFWHints(unsigned int iVersion)
{
#ifdef __APPLE__
#if (OPENGL_VERSION >= 30)
  // request OpenGL 3.2, will return a 4.1 context on Mavericks
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif
#else
  // non-apple
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
#endif
}

bool ThreadDraw::CreateGLFWWindow()
{
  // Initialize GLFW
  if (!glfwInit())
  {
    tc::err << "Failed to initialize GLFW" << tc::endl;
    exit(-1);
  }

  // Configure OpenGL context
  SetGLFWHints(OPENGL_VERSION);

  // Activate Multi-Sampling (second parameter is the number of samples):
  glfwWindowHint( GLFW_SAMPLES, SAMPLES );

  // Request an OpenGL debug context:
  glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, true);

  // Define whether the Window can get resized:
  glfwWindowHint( GLFW_RESIZABLE, false );

  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  // Non-decorated Windows can be used as Splash Screens:
  //glfwWindowHint( GLFW_DECORATED, false );

  // Request an sRGB Frame-Buffer:
  //glfwWindowHint( GLFW_SRGB_CAPABLE, true );

  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);


  // Try to create an OpenGL Context in a Window and check the supported OpenGL version:
  m_pWindow = glfwCreateWindow(m_v2WindowSize.x, m_v2WindowSize.y, "Thief's Creed", NULL, NULL);
  if (!m_pWindow) {
    tc::err << "Failed to open a GLFW window - requested OpenGL: " << OPENGL_VERSION << tc::endl;
    return false;
  }
  glfwMakeContextCurrent(m_pWindow);

  return true;
}


bool ThreadDraw::WindowShouldClose()
{
  bool exitButtonPressed = glfwWindowShouldClose(m_pWindow) != 0;
  bool esc_pressed = (glfwGetKey( m_pWindow, GLFW_KEY_ESCAPE ) == GLFW_PRESS);

  return exitButtonPressed || esc_pressed;
}


void ThreadDraw::InitEndScreen(ResourceCollection* pResourceCollection, bool bWon)
{
  Shader* pShader = pResourceCollection->GetShader("EndScreenShader");

  // Load Win or loose Texture
  Texture* pTexture;
  if (bWon)
    pTexture = pResourceCollection->GetTexture("Won");
  else
    pTexture = pResourceCollection->GetTexture("Lost");

  Mesh* pLostMesh = pResourceCollection->GetMesh("ScreenSizedQuad");
  m_pEndScreenNode = new MeshNode(pShader, pLostMesh, pTexture);
}


void ThreadDraw::DrawEndScreen()
{
  glBindFramebuffer(GL_FRAMEBUFFER, 0); // bind standard buffer
  // clear standard framebuffer
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  m_pEndScreenNode->DrawOpaque(glm::mat4());

  glfwSwapBuffers(m_pWindow);
  glfwPollEvents();
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/