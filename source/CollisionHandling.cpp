/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "CollisionHandling.h"

#include "TC.h"
#include "Defines.h"
#include "AI.h"
#include "SceneGraph/PlayerNode.h"
#include "SceneGraph/MeshNode.h"
#include "SceneGraph/ClothNode.h"
#include "Map/MapSegment.h"
#include "RessourceManagement/Mesh.h"

#include "gtc/matrix_transform.hpp"
#include "gtx/rotate_vector.hpp"
#include <cmath>

#include <iostream>

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

CollisionHandling::CollisionHandling()
  : m_player(nullptr)
{}

CollisionHandling::~CollisionHandling()
{}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

void CollisionHandling::setPlayerNode(PlayerNode* p_player)
{
  m_player = p_player;
}

void CollisionHandling::setRootNode(Node * pRootNode)
{
  m_pRootNode = pRootNode;
}


void CollisionHandling::performCollisionHandling()
{
  // player <-> map
  if (!SET_CAN_MOVE_THROUGH_WALLS)
  {
    performCH_player_map();
  }

  // player <-> cloth
  performCH_player_cloth();

  // player <-> AI
  std::vector<AI*> vecAINodes;
  collectNodes<AI>(m_pRootNode, vecAINodes);
  if (!SET_CAN_MOVE_THROUTH_AI)
  {
    performCH_player_AI(vecAINodes, false);
  }

  // AI <-> AI
  if (!SET_AI_CANNOT_COLLIDE)
  {
    performCH_AI_AI(vecAINodes);
  }

  // AI <-> map
  performCH_AI_map(vecAINodes);

  // again AI <-> palyer, this time push the player
  // (if AI bounces back from the wall)
  if (!SET_CAN_MOVE_THROUTH_AI)
  {
    performCH_player_AI(vecAINodes, true);
  }

}


void CollisionHandling::performCH_player_map()
{
  // no player node stored
  if (m_player == nullptr)
  {
    tc::err << "Collision Handling: No player stored." << tc::endl;
    return;
  }

  // player position and Map segment
  glm::vec3 playerPos = m_player->getPlayerPosition();
  MapSegment* currMapSeg = m_player->GetCurrentMapSegment();
  MeshNode* currMapMeshNode = m_player->GetCurrentMapMeshNode();

  // no map segment found
  if (currMapSeg == nullptr || currMapMeshNode == nullptr)
  {
    tc::war << "Collison Detection, no Map Segment found for player." << tc::endl;
    return;
  }

  // perform collision handling
  glm::vec3 push_vec;
  bool collision = performCH_point_map(playerPos, currMapSeg, currMapMeshNode, KEEP_DISTANCE_TO_WALLS_PLAYER, push_vec);

  // move player away from walls
  if (collision)
  {
    m_player->Translate(push_vec);
  }
}


void CollisionHandling::performCH_AI_map(std::vector<AI*>& vecAINodes)
{
  for (size_t i = 0; i < vecAINodes.size(); ++i)
  {
    AI* pAi = vecAINodes[i];

    // current position and map segment
    glm::vec3 pos = pAi->GetAiPosition();
    MapSegment* currMapSeg = pAi->GetSegment();
    MeshNode* currMapMeshNode = pAi->GetCurrentMapMeshNode();

    // no map segment found
    if (currMapSeg == nullptr || currMapMeshNode == nullptr)
    {
      tc::war << "Collison Detection, no Map Segment found for AI." << tc::endl;
      return;
    }

    // perform collision handling
    glm::vec3 push_vec;
    bool collision = performCH_point_map(pos, currMapSeg, currMapMeshNode, static_cast<float>(KEEP_DISTANCE_TO_WALLS_AI), push_vec);

    // move AI away from walls
    if (collision)
    {
      pAi->Collision(push_vec);
    }
  }
}


bool CollisionHandling::performCH_point_map(glm::vec3 v3Point, MapSegment* currMapSeg, MeshNode* currMapMeshNode, float distanceToKeep,
                                            glm::vec3& v3Push)
{
  // set to appropriate height
  v3Point[1] = HEIGHT_FOR_COL_DET_WALLS + MAP_OFFSET;

  // center of the map segment
  glm::vec2 mapSegCenter_ = currMapSeg->GetPosition();
  glm::vec3 mapSegCenter(mapSegCenter_[0], v3Point[1], mapSegCenter_[1]);

  // the mesh
  Mesh* mesh = currMapMeshNode->getFirstStoredMesh();

  // no mesh stored
  if (mesh == nullptr)
  {
    tc::err << "Collision Handling: Cannot get MapSegment Mesh." << tc::endl;
    return false;
  }

  // transform the mesh according to its position and orientation
  glm::mat4 transform = currMapMeshNode->getAncestorTransformation();
  // get vertices and transform
  std::vector<glm::vec3> vertices =  mesh->GetVertices();
  for (size_t i = 0; i < vertices.size(); ++i)
  {
    glm::vec4 transVert = transform * glm::vec4( vertices[i], 1.0 );
    vertices[i] = glm::vec3(transVert[0], transVert[1], transVert[2]);
  }

  // get indices
  std::vector<unsigned int> indices = mesh->getFaces_indices();
  // if not given as index mesh, add indices
  if (indices.size() == 0)
  {
    indices.resize( vertices.size() );
    for (size_t i = 0; i < indices.size(); ++i)
    {
      indices[i] = i;
    }
  }

  // no proper indices
  if (indices.size() % 3 != 0)
  {
    tc::err << "Collision Handling: Mesh indices not dividable by 3." << tc::endl;
    return false;
  }

  // store maximum push away from a wall
  glm::vec3 push_vec(0.0, 0.0, 0.0);

  bool collision = false;

  // test all faces for intersection
  for (size_t i = 0; i < indices.size() / 3; ++i)
  {
    // face's vertices
    glm::vec3 vec1 = vertices[ indices[3*i] ];
    glm::vec3 vec2 = vertices[ indices[3*i + 1] ];
    glm::vec3 vec3 = vertices[ indices[3*i + 2] ];

    // face normal
    glm::vec3 normal = glm::cross(vec2 - vec1, vec3 - vec1  );
    // normal orientation
    if (glm::dot(mapSegCenter - vec1, normal) < 0.0f)
      normal = -normal;
    normal = glm::normalize(normal);

    // project tested point on face plane
    float lambda = glm::dot(v3Point - vec1, normal);
    glm::vec3 project = v3Point - lambda*normal;

    // check if orthogonal projection of the player is inside the face
    bool inside = true;

    // test all three edges, edge 1
    glm::vec3 edge = vec2 - vec1;
    glm::vec3 edge_n = glm::cross(edge, normal);
    // check if edge normal points towards third vertex
    if (glm::dot(vec3 - vec1, edge_n) < 0.0f)
      edge_n = -edge_n;
    // test if point is outside the triangle
    if (glm::dot(project - vec1, edge_n) < 0.0f)
      inside = false;

    // edge 2
    edge = vec3 - vec2;
    edge_n = glm::cross(edge, normal);
    // check if edge normal points towards third vertex
    if (glm::dot(vec1 - vec2, edge_n) < 0.0f)
      edge_n = -edge_n;
    // test if point is outside the triangle
    if (glm::dot(project - vec2, edge_n) < 0.0f)
      inside = false;

    // edge 3
    edge = vec1 - vec3;
    edge_n = glm::cross(edge, normal);
    // check if edge normal points towards third vertex
    if (glm::dot(vec2 - vec3, edge_n) < 0.0f)
      edge_n = -edge_n;
    // test if point is outside the triangle
    if (glm::dot(project - vec3, edge_n) < 0.0f)
      inside = false;


    // projection inside the triangle
    // and player is too close to the wall
    if (inside && lambda < distanceToKeep)
    {
      collision = true;

      // push away
      glm::vec3 push_curr = (distanceToKeep - lambda) * normal;

      // store maximum push of all triangles, only push on x/z-plane
      if (std::abs(push_vec[0]) < std::abs(push_curr[0]))
        push_vec[0] = push_curr[0];
      if (std::abs(push_vec[2]) < std::abs(push_curr[2]))
        push_vec[2] = push_curr[2];
    }

  } // next face

  // return pushing vector
  v3Push = push_vec;
  return collision;
}


void CollisionHandling::performCH_player_cloth()
{
  glm::vec3 playerPosition = m_player->getPlayerPosition();

  std::vector<ClothNode*> vecClothNodes;

  collectNodes<ClothNode>(m_pRootNode, vecClothNodes);

  for (ClothNode* pClothNode : vecClothNodes)
  {
    glm::vec3 clothPosition = pClothNode->GetPosition();
    glm::vec3 fromClothToPlayer = playerPosition - clothPosition;
    float distanceClothPlayer = glm::length(fromClothToPlayer);

    if (distanceClothPlayer > CLOTH_SIMULATION_DISTANCE)
    {
      pClothNode->SetSimulationEnabled(false);
    }
    else
    {
      pClothNode->SetSimulationEnabled(true);
      pClothNode->SetToPlayer(fromClothToPlayer);
    }

  }

}


void CollisionHandling::performCH_player_AI(std::vector<AI*>& vecAINodes, bool pushPlayer)
{
  for (size_t i = 0; i < vecAINodes.size(); ++i)
  {
    AI* pAi = vecAINodes[i];

    // positions
    glm::vec3 v3PlayerPos = m_player->getPlayerPosition();
    glm::vec3 v3AIPos = pAi->GetAiPosition();

    // same height
    v3PlayerPos.y = 0.0f;
    v3AIPos.y = 0.0f;

    // check distance
    float fDist = glm::distance(v3PlayerPos, v3AIPos);
    if (fDist < PLAYER_AI_DISTANCE)
    {
      // push apart
      glm::vec3 v3Diff = glm::normalize(v3AIPos - v3PlayerPos);
      // avoid nan caused by devision by 0
      if (fDist <= 0.0f)
      {
        // right on top of each other, push apart in x direction
        v3Diff = glm::vec3(1.0f, 0.0f, 0.0f);
      }

      glm::vec3 v3Push = (PLAYER_AI_DISTANCE - fDist) * v3Diff;

      // push AI
      if (!pushPlayer)
      {
        pAi->CollisionWithPlayer(v3Push);
      }

      // push player
      else
      {
        m_player->Translate(-v3Push);
        // alert AI (player may lose)
        pAi->CollisionWithPlayer( glm::vec3(0.0, 0.0, 0.0) );
      }
    }
  }
}


void CollisionHandling::performCH_AI_AI(std::vector<AI*>& vecAINodes)
{
  // check all pairs of AIs
  for (size_t i = 0; i < vecAINodes.size() - 1; ++i)
  {
    // position AI 1
    AI* pAi_1 = vecAINodes[i];
    glm::vec3 pos_1 = pAi_1->GetAiPosition();
    pos_1.y = 0.0f;

    for (size_t j = i+1; j < vecAINodes.size(); ++j)
    {
      // position AI 2
      AI* pAi_2 = vecAINodes[j];
      glm::vec3 pos_2 = pAi_2->GetAiPosition();
      pos_2.y = 0.0f;

      // check distance
      float fDist = glm::distance(pos_1, pos_2);
      if (fDist < AI_AI_DISTANCE)
      {
        // push apart
        glm::vec3 v3Diff = glm::normalize(pos_2 - pos_1);
        // avoid nan caused by devision by 0
        if (fDist <= 0.0f)
        {
          // right on top of each other, push apart in x direction
          v3Diff = glm::vec3(1.0f, 0.0f, 0.0f);
        }

        glm::vec3 v3Push = 0.5f * (AI_AI_DISTANCE - fDist) * v3Diff;
        // each is pushed half way
        pAi_1->CollisionWithAI(-v3Push, pos_2);
        pAi_2->CollisionWithAI(v3Push, pos_1);
      }
    }
  }
}


template<typename NodeType>
void CollisionHandling::collectNodes(Node * pNode, std::vector<NodeType*>& vecNodes)
{
  NodeType* pCastNode = dynamic_cast<NodeType*>(pNode);

  if (pCastNode)
  {
    vecNodes.push_back(pCastNode);
  }

  for (Node* pChild : pNode->GetChildren())
  {
    collectNodes<NodeType>(pChild, vecNodes);
  }
}


/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/