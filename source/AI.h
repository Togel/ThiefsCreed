#ifndef _AI_
#define _AI_

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include <vector>
#include "glm.hpp"
#include "Map/MapSegment.h"
#include "SceneGraph/TransformNode.h"

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/
class Node;
class ResourceCollection;
class Timer;
class GameManager;
class PlayerNode;
class LightManager;
class Map;
class MeshNode;
class LightNode;

/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/

class AI : public TransformNode
{
public:
  /*
  * Constructor.
  */
  AI(GameManager* pManager,  std::vector<MapSegment*> vecRoute, PlayerNode* pPlayer, LightManager* pLightManager, Map* pMap);

  /*
  * Destructor.
  */
  virtual ~AI();

  /*
  * Updates the Position of the AI.
  * @param Timer to determine time between frames
  */
  void Update(Timer* pTimer);

  /*
  * Returns a Pointer to the current Map Segment of the AI.
  * @return A Pointer to the current Map Segment of the AI.
  */
  MapSegment* GetSegment();

  /*
  * Returns the MeshNode of the Map Segment in which the drone is located.
  * @return The MeshNode of the Map Segment in which the drone is located.
  */
  MeshNode* GetCurrentMapMeshNode();

  /*
   * Returns the drone's position.
   * @return The drone's position.
   */
  glm::vec3 GetAiPosition();

  /*
   * Gets called by collision Detection, if
   * the drone collides with the player.
   * @param v3Translation The translation resulting from the collision.
   */
  void CollisionWithPlayer(glm::vec3 v3Translation);

  /*
   * Gets calledby collision Detection, if
   * the drone collides with another drone.
   * @param v3Translation The translation resulting from the collision.
   * @param v3PosOfOtherAI The position of the other AI, involved in the collision.
   */
  void CollisionWithAI(glm::vec3 v3Translation, glm::vec3 v3PosOfOtherAI);

  /*
   * Gets called by collision Detection, if
   * the drone collides with an object and needs a position update.
   * @param v3Translation The translation resulting from the collision.
   */
  void Collision(glm::vec3 v3Translation);

  /*
  * Returns whether the AI has caught to Player or not.
  * @return true iff the AI caught the Player.
  */
  bool CaughtPlayer();

private:
  MapSegment* m_pCurSegment = nullptr;
  Direction m_eDirection = Direction::South;

  glm::vec3 m_v3ViewDir = glm::vec3(0.0f, 0.0f, 1.0f);
  glm::vec3 m_v3Position = glm::vec3(0.0f, 0.0f, 0.0f);

  float m_fHeight = 0.0f;

  std::vector<MapSegment*> m_vecRoute;
  int m_iCurRoutePos = 0; // AI is placed on '0' Position, thus the next Patrol Point is on '1' Position.

  PlayerNode* m_pPlayer = nullptr;
  MapSegment* m_pLastKnownPos = nullptr;
  bool m_bChasing = false;  
  bool m_bMoveFreely = false;
  bool m_bCaught = false;

  Map* m_pMap = nullptr;

  LightNode* m_pDroneLight;

  // collision handling, pass another drone
  bool m_bCollisisonWithAI = false;
  float m_fLastDeltaT = 0.0f;
  glm::vec3 m_v3PosOfOtherAI;

  /*
  * Move in the Member Direction (not eDir).
  * @param fDeltaT Time elapsed since the last Call.
  */
  void Move(Direction eDir, float fDeltaT);

  /*
   * Lets the drone hover, i.e. move up and down. To be called in the move methods.
   */
  void Hover(float fDeltaT);

  /*
  * Returns a Pointer to the next Map Segment the AI is going to pass.
  * @return A Pointer to the next Map Segment the AI is going to pass.
  */
  MapSegment* GetNextSegment();

  /*
  * Construct the AI Geometry and places it in the SceneGraph.
  * @param 'pRoot' defines the Node at which the AI get rooted.
  * @param 'pCollection' Collection storing all Meshes and Textures.
  */
  void PlaceInSceneGraph(Node* pRoot, ResourceCollection* pCollection, LightManager* pLightManager);

  /*
  * Calculates the Direction the AI needs to move to get closer to a given Map Segment.
  * @param 'pTarget' defines the Segment the AI should move to.
  */
  Direction FindShortestPath(MapSegment* pTarget);

  /*
  * Creates a Vector storing the Paths from the current Segment to a Target Segment.
  * @param 'pCurPos' defines the current Map Segment of the AI.
  * @param 'pTarget' defines the Target Map Segment.
  * @param 'vecPaths' Vector storing all Paths.
  * @param 'vecPath' storing the Path which is checked in a Recursion Step.
  */
  void FindPaths(MapSegment* pCurPos, MapSegment* pTarget, std::vector<std::vector<MapSegment*>>& vecPaths, std::vector<MapSegment*>& vecPath);

  /*
  * Filter Paths which do not reach the Target.
  * @param 'vecPaths' defines the Path Vector which is filtered.
  * @param 'pTarget' defines the Target Map Segment.
  */
  void FilterPaths(std::vector<std::vector<MapSegment*>>& vecPaths, MapSegment* pTarget);

  /*
  * Checks whether there is the Player in the Viewing Direction.
  * @param 'eDir' defines the Direction which should be checked.
  * @return true iff the Player is in Viewing Direction.
  */
  bool IsPlayerInDir(Direction eDir);

  /*
  * Checks whether the View Direction equals the Target Direction.
  * @param fDeltaT Time elapsed since the last Call.
  * @return true iff the View Direction equals the Target Direction.
  */
  bool IsRotationNeeded(Direction eDir, float fDeltaT);

  /*
   * Tests whether the drone is at any segment's center.
   * @return True iff the drone is at some segment's center.
   */
  bool IsAtSegmentCenter();

  /*
   * Rotates the Drone towards the given target view direction,
   * according to the ellapsed time and the rotation speed.
   */
  void RotateTowardsDirection(glm::vec3 v3TargetDir, float fDeltaT);

  /*
   * Adjust the transformation matrix with respect to the current orientation.
   */
  void AdjustRotationMatrix();

  /*
  * Adjusts the View Direction Vector such that it points in one of the four basic Directions (N,S,W,E).
  */
  void RepairViewDir();

  /*
   * Test whether a target position is within a certain view cone of the Drone.
   * @param v3TargetPos The target position.
   * @param bMovement States if the cone test is used to check if a target is within
   * a certain small angled cone, i.e. orientation (true), or if it is tested, if the target is within
   * a finite, large angled view cone (false).
   * @return True iff the target position is within the cone.
   */
  bool IsTargetInViewCone(glm::vec3 v3TargetPos, bool bMovement);

  /*
   * Chase the player without sticking to the routs.
   */
  void MoveFreely(glm::vec3 v3TargetPos, float fDeltaT, bool bStopInFront = false);

  /*
   * Update the current segment needs a different procedure when
   * moving freely.
   */
  void UpdateCurrentSegment_FreeMove();

  /*
   * Update the stored direction in geographic directions (i.e. North, South, etc.)
   */
  void UpdateCurrentDirection_FreeMove();

  /*
   * Calculates the map indices from the drone's
   * current position.
   * @param xInd Output: The x-index.
   * @param yInd Output: The y-index.
   */
  void GetMapIndices(int& xInd, int& yInd);

  /*
   * Finds a point, to which the drone should return
   * after a free move. This is the center of either the
   * current segment or of the segment in front of the drone.
   * @param v3ReturnPoint Output paramter, will store he point,
   * to which the drone shall move next.
   * @return The MapSegment, to which the drone will shall return.
   */
   MapSegment* FreeMoveReturnPoint(glm::vec3& v3ReturnPoint);

   /*
    * Sets the drone's light's color.
    */
   void SetDroneLightColor(glm::vec3 v3LightCol);

   /*
    * Checks if the sight on the player is blocked by the geometry of the
    * current map segments of AI and player.
    * @return True iff the sight from AI to the player is blocked by the geometry
    * of either the player's or the AI's map segment.
    */
   bool IsSightBlockedBySegmentGeometry();

   /*
    * Checks if the drone views in the direction, in
    * which it is heading.
    * @return True iff the drones views in the direction,
    * in which it is heading.
    */
   bool IsViewingInTargetDirection();
};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
#endif