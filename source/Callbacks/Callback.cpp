/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "Callback.h"
#include "../Timer.h"

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

Callback::Callback()
  :m_pTimer(new Timer())
{

}

Callback::~Callback()
{

}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

void Callback::Execute()
{
  HandleEvents(m_pTimer);
  m_pTimer->Update();
}

void Callback::HandleEvents(Timer* pTimer)
{

}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/