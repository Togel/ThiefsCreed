#include "LightNode.h"
/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "TransformNode.h"
#include "../TC.h"

#include "../Settings.h"
#include "../Defines.h"

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/
LightNode::LightNode()
{
  glGenFramebuffers(1, &m_Framebuffer);
  glBindFramebuffer(GL_FRAMEBUFFER, m_Framebuffer);

  
  glGenTextures(1, &m_stupidTexture);
  glBindTexture(GL_TEXTURE_2D, m_stupidTexture);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, 1024, 1024, 0, GL_RGBA, GL_FLOAT, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, m_stupidTexture, 0);
  

  glGenTextures(1, &m_depthTexture);
  glBindTexture(GL_TEXTURE_2D, m_depthTexture);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, 1024, 1024, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_INT, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

  glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, m_depthTexture, 0);


  glDrawBuffer(GL_COLOR_ATTACHMENT0);
  //glDrawBuffer(GL_NONE);
  //glReadBuffer(GL_NONE);

  GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
  if (status != GL_FRAMEBUFFER_COMPLETE)
  {
    tc::err << "Light Framebuffer not complete : " << status << tc::endl;
  }

  glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

LightNode::~LightNode()
{
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/
glm::vec3 LightNode::GetLightPosition()
{
  Node* pParent = GetParent();
  
  glm::vec4 position(0.0, 0.0, 0.0, 1.0);

  while (pParent)
  {
    if (pParent->GetNodeType() == "TransformNode")
    {
      TransformNode* pTransNode = dynamic_cast<TransformNode*>(pParent);
      pTransNode->GetTransformation();

      position = pTransNode->GetTransformation() * position;
      
    }

    pParent = pParent->GetParent();
  }

  return position.swizzle(glm::comp::X, glm::comp::Y, glm::comp::Z);
}

glm::vec3 LightNode::GetLightDirection()
{
  Node* pParent = GetParent();

  glm::vec4 direction(0.0, 0.0, 1.0, 0.0);

  while (pParent)
  {
    if (pParent->GetNodeType() == "TransformNode")
    {
      TransformNode* pTransNode = dynamic_cast<TransformNode*>(pParent);
      pTransNode->GetTransformation();

      direction = pTransNode->GetTransformation() * direction;

    }

    pParent = pParent->GetParent();
  }

  return direction.swizzle(glm::comp::X, glm::comp::Y, glm::comp::Z);
}

void LightNode::SetLightIntensity(glm::vec3 v3Intensity)
{
  m_v3Intensity = v3Intensity;
}

glm::vec3 LightNode::GetLightIntensity()
{
  return m_v3Intensity;
}

void LightNode::SetLightAttenuation(float fAttenuation)
{
  m_fAttenuation = fAttenuation;
}

float LightNode::GetLightAttenuation()
{
  return m_fAttenuation;
}

void LightNode::SetLightAmbientCoefficient(float fAmbientCoefficient)
{
  m_fAmbientCoefficient = fAmbientCoefficient;
}

float LightNode::GetLightAmbientCoefficient()
{
  return m_fAmbientCoefficient;
}

void LightNode::SetLightConeAngle(float fAngle)
{
  m_fConeAngle = fAngle;
}

float LightNode::GetLightConeAngle()
{
  return m_fConeAngle;
}

void LightNode::SetLightType(LightType type)
{
  m_eLightType = type;
}

LightNode::LightType LightNode::GetLightType()
{
  return m_eLightType;
}

void LightNode::BindFramebuffer()
{
  glBindFramebuffer(GL_FRAMEBUFFER, m_Framebuffer);
}

void LightNode::BindTexture()
{
  glActiveTexture(GL_TEXTURE5);
  glBindTexture(GL_TEXTURE_2D, m_depthTexture);
}

void LightNode::SetMatProj(glm::mat4 matProj)
{
  m_matProj = matProj;
}

glm::mat4 LightNode::GetMatProj()
{
  return m_matProj;
}

void LightNode::SetMatView(glm::mat4 matView)
{
  m_matView = matView;
}

glm::mat4 LightNode::GetMatView()
{
  return m_matView;
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/


