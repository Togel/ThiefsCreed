/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "ClothNode.h"

#include "../SceneGraph/Node.h"
#include "../SceneGraph/TransformNode.h"

#include "../RessourceManagement/Shader.h"
#include "../RessourceManagement/Mesh.h"
#include "../RessourceManagement/Texture.h"
#include "../TC.h"

#include "../Timer.h"
#include "../Defines.h"

#include <glm.hpp>
#include <gtc/matrix_transform.hpp>
#include <vector>

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/
ClothNode::ClothNode(Shader* pShader, Shader* pIntegrationShader, Texture* pTexture)
  : DrawableNode(pShader)
{
  m_iteration_index = 0;
  m_pIntegrationShader = pIntegrationShader;

  //Init(uiCameraUniformBufferID, pTexture);
  Init(pTexture);
}

ClothNode::~ClothNode()
{
  m_pIntegrationShader->Destroy();
  glDeleteBuffers(7, m_vbo);
  glDeleteVertexArrays(2, m_vao);
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

void ClothNode::Init(Texture* pTexture)
{

  m_pTexture = pTexture;

  //tc::err << "glError() : " << glGetError() << tc::endl;


  glm::vec4 initPos[NUM_CLOTH_POINTS];
  glm::vec3 initVelocities[NUM_CLOTH_POINTS];
  glm::ivec4 connection_vectors[NUM_CLOTH_POINTS];
  glm::ivec4 connection_vectors2[NUM_CLOTH_POINTS];
  glm::vec3 initNormals[NUM_CLOTH_POINTS];
  glm::vec2 initUVs[NUM_CLOTH_POINTS];

  int n = 0;

  for (int j = 0; j < NUM_CLOTH_POINTS_Y; j++)
  {
    float fj = (float)j / (float)NUM_CLOTH_POINTS_Y;

    for (int i = 0; i < NUM_CLOTH_POINTS_X; i++)
    {
      float fi = (float)i / (float)NUM_CLOTH_POINTS_X;

      initPos[n] = glm::vec4(((fi - 0.5f) * (float)NUM_CLOTH_POINTS_X),
                             ((fj - 0.5f) * (float)NUM_CLOTH_POINTS_Y),
                             (0.6f * sinf(fi) * cosf(fj)),
                             (1.0f));

      initVelocities[n] = glm::vec3(0.0f);

      initNormals[n] = glm::vec3(0.5);

      initUVs[n] = glm::vec2(fi, fj);

      connection_vectors[n] = glm::ivec4(-1);
      connection_vectors2[n] = glm::ivec4(-1);
      
      if (j != (NUM_CLOTH_POINTS_Y - 1))
      {
        if (i != 0)
          connection_vectors[n][0] = n - 1;
        if (j != 0)
          connection_vectors[n][1] = n - NUM_CLOTH_POINTS_X;
        if (i != (NUM_CLOTH_POINTS_X - 1))
          connection_vectors[n][2] = n + 1;
        if (j != (NUM_CLOTH_POINTS_Y - 1))
          connection_vectors[n][3] = n + NUM_CLOTH_POINTS_X;

        if (i != 0 && j != 0)
          connection_vectors2[n][0] = n - NUM_CLOTH_POINTS_X - 1;
        if (j != 0 && i != (NUM_CLOTH_POINTS_X - 1))
          connection_vectors2[n][1] = n - NUM_CLOTH_POINTS_X + 1;
        if (i != 0 && j != (NUM_CLOTH_POINTS_Y - 1))
          connection_vectors2[n][2] = n + NUM_CLOTH_POINTS_X - 1;
        if (i != (NUM_CLOTH_POINTS_X - 1) && j != (NUM_CLOTH_POINTS_Y - 1))
          connection_vectors2[n][3] = n + NUM_CLOTH_POINTS_X + 1;
      }

      n++;
    }

  }


  glGenVertexArrays(2, m_vao);
  glGenBuffers(8, m_vbo);
  
  for (int i = 0; i < 2; i++)
  {
    glBindVertexArray(m_vao[i]);


    // Position
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo[POS_A + i]);
    glBufferData(GL_ARRAY_BUFFER, NUM_CLOTH_POINTS * sizeof(glm::vec4), initPos, GL_DYNAMIC_COPY);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(0);

    // Velocities
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo[VEL_A + i]);
    glBufferData(GL_ARRAY_BUFFER, NUM_CLOTH_POINTS * sizeof(glm::vec3), initVelocities, GL_DYNAMIC_COPY);
    glVertexAttribPointer(5, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(5);

    // Connection
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo[CON]);
    glBufferData(GL_ARRAY_BUFFER, NUM_CLOTH_POINTS * sizeof(glm::ivec4), connection_vectors, GL_STATIC_DRAW);
    glVertexAttribIPointer(6, 4, GL_INT, 0, NULL);
    glEnableVertexAttribArray(6);

    // normals
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo[NORMALS]);
    glBufferData(GL_ARRAY_BUFFER, NUM_CLOTH_POINTS * sizeof(glm::vec4), initNormals, GL_DYNAMIC_DRAW);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(1);

    // connection2
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo[CON2]);
    glBufferData(GL_ARRAY_BUFFER, NUM_CLOTH_POINTS * sizeof(glm::ivec4), connection_vectors2, GL_STATIC_DRAW);
    glVertexAttribIPointer(7, 4, GL_INT, 0, NULL);
    glEnableVertexAttribArray(7);

    // uvs
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo[UV]);
    glBufferData(GL_ARRAY_BUFFER, NUM_CLOTH_POINTS * sizeof(glm::vec2), initUVs, GL_STATIC_DRAW);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(2);
  }

  glGenTextures(2, m_pos_tbo);
  glBindTexture(GL_TEXTURE_BUFFER, m_pos_tbo[0]);
  glTexBuffer(GL_TEXTURE_BUFFER, GL_RGBA32F, m_vbo[POS_A]);
  glBindTexture(GL_TEXTURE_BUFFER, m_pos_tbo[1]);
  glTexBuffer(GL_TEXTURE_BUFFER, GL_RGBA32F, m_vbo[POS_B]);

  int triangles = (NUM_CLOTH_POINTS_X - 1) * 2 * (NUM_CLOTH_POINTS_Y - 1);
  int lines = (NUM_CLOTH_POINTS_X - 1) * NUM_CLOTH_POINTS_Y + (NUM_CLOTH_POINTS_Y - 1) * NUM_CLOTH_POINTS_X;
  m_iNumTriangles = lines;

  glGenBuffers(1, &m_index_buffer);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_index_buffer);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, triangles * 3 * sizeof(int), NULL, GL_STATIC_DRAW);

  int* e = (int *)glMapBufferRange(GL_ELEMENT_ARRAY_BUFFER, 0, triangles * 3 * sizeof(int), GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
  

  for (int j = 0; j < NUM_CLOTH_POINTS_Y - 1; j++)
  {
    for (int i = 0; i < NUM_CLOTH_POINTS_X - 1; i++)
    {
      int absolute = j * NUM_CLOTH_POINTS_X + i;
      
      *e++ = absolute;
      *e++ = absolute + NUM_CLOTH_POINTS_X;
      *e++ = absolute + NUM_CLOTH_POINTS_X + 1;
      
      *e++ = absolute;
      *e++ = absolute + NUM_CLOTH_POINTS_X + 1;
      *e++ = absolute + 1;
    }
  }

  glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);

  glBindVertexArray(0);

}

void ClothNode::Update(Timer * pTimer)
{
}

void ClothNode::Integration(Timer* pTimer, glm::mat4 matTransformation)
{
  if (m_bSimulationEnabled)
  {

    glm::mat4 matInverseModel = glm::inverse(matTransformation);
 
    glm::vec4 transformedToPlayer = matInverseModel * glm::vec4(m_v3ToPlayer, 0.0);

    // 'Draw' integration program
    m_pIntegrationShader->Bind();

    glEnable(GL_RASTERIZER_DISCARD);

    float rest_length_vertical = 0.4f;
    float rest_length_horizontal = 0.8f;
    float rest_lenght_shearing = std::sqrt(pow(rest_length_vertical, 2) + pow(rest_length_horizontal, 2)) + 0.1f;

    m_pIntegrationShader->SetUniform(m_pIntegrationShader->GetUniformLocation("vecToPlayer"), transformedToPlayer[0], transformedToPlayer[1], transformedToPlayer[2]);
    m_pIntegrationShader->SetUniform(m_pIntegrationShader->GetUniformLocation("t"), pTimer->GetElapsedTime_withoutUpdate() / ((float)1000.0f * (float)CLOTH_INTEGRATION_STEPS));
    m_pIntegrationShader->SetUniform(m_pIntegrationShader->GetUniformLocation("k_horizontal"), 120.0f);
    m_pIntegrationShader->SetUniform(m_pIntegrationShader->GetUniformLocation("k_vertical"), 300.0f);
    m_pIntegrationShader->SetUniform(m_pIntegrationShader->GetUniformLocation("k_shearing"), 30.0f);
    m_pIntegrationShader->SetUniform(m_pIntegrationShader->GetUniformLocation("c"), 1.8f);
    m_pIntegrationShader->SetUniform(m_pIntegrationShader->GetUniformLocation("rest_length_vertical"), rest_length_vertical);
    m_pIntegrationShader->SetUniform(m_pIntegrationShader->GetUniformLocation("rest_length_horizontal"), rest_length_horizontal);
    m_pIntegrationShader->SetUniform(m_pIntegrationShader->GetUniformLocation("rest_length_shearing"), rest_lenght_shearing);

    for (int i = CLOTH_INTEGRATION_STEPS; i != 0; --i)
    {

      glBindVertexArray(m_vao[m_iteration_index]);
      glBindTexture(GL_TEXTURE_BUFFER, m_pos_tbo[m_iteration_index]);
      m_iteration_index = m_iteration_index ^ 1;
      glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, m_vbo[POS_A + m_iteration_index]);
      glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 1, m_vbo[VEL_A + m_iteration_index]);
      glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 2, m_vbo[NORMALS]);
      glBeginTransformFeedback(GL_POINTS);
      glDrawArrays(GL_POINTS, 0, NUM_CLOTH_POINTS);
      glEndTransformFeedback();
    }

    glDisable(GL_RASTERIZER_DISCARD);
    m_pIntegrationShader->Release();
  }

}

void ClothNode::DrawOpaque(glm::mat4 matTransformation)
{

  glm::mat4 matInverseModel = glm::inverse(matTransformation);
  glm::mat4 matInvTransModel = glm::transpose(matInverseModel);

  // Draw the cloth
  m_pShader->Bind();

  if (m_pTexture)
  {
    m_pTexture->Bind(GL_TEXTURE0);
  }

  glBindVertexArray(m_vao[m_iteration_index]);

  m_pShader->SetUniform(m_pShader->GetUniformLocation("matModel"), &matTransformation[0][0]);
  m_pShader->SetUniform(m_pShader->GetUniformLocation("matInvTransModel"), &matInvTransModel[0][0]);
  m_pShader->SetUniform(m_pShader->GetUniformLocation("bOrientNormalToPlayer"), 1);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_index_buffer);
  glDrawElements(GL_TRIANGLES, m_iNumTriangles * 3, GL_UNSIGNED_INT, NULL);

  m_pShader->SetUniform(m_pShader->GetUniformLocation("bOrientNormalToPlayer"), 0);

  m_pShader->Release();
  glBindVertexArray(0);
}

glm::vec3 ClothNode::GetPosition()
{
  glm::vec4 position(0.0, 0.0, 0.0, 1.0);

  Node* pParent = GetParent();

  while (pParent)
  {
    if (pParent->GetNodeType() == "TransformNode")
    {
      TransformNode* pTransNode = dynamic_cast<TransformNode*>(pParent);
      pTransNode->GetTransformation();

      position = pTransNode->GetTransformation() * position;

    }

    pParent = pParent->GetParent();
  }

  return glm::vec3(position);
}

void ClothNode::SetSimulationEnabled(bool bValue)
{
  m_bSimulationEnabled = bValue;
}

void ClothNode::SetToPlayer(glm::vec3 v3ToPlayer)
{
  m_v3ToPlayer = v3ToPlayer;
}


/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
