#ifndef _SKY_BOX_NODE_
#define _SKY_BOX_NODE_

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MeshNode.h"

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/
class Mesh;
class Texture;
class Shader;

class Timer;

/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
 
class SkyBoxNode : public MeshNode
{
public:
  
  SkyBoxNode(Shader* pShader, Mesh* pMesh, Texture* pTexture, Texture* pNormalMap = nullptr, float fAlpha = 1.0f,
             DrawableMesh::DrawType pDrawType = DrawableMesh::DrawType::DT_OPAQUE);
  
  /*
  * Uploads updated Uniform Information to Graphics Card.
  * Overwrites virtual function of MeshNode.
  * @param 'bSet' defines whether the Uniforms are set or re-set.
  */
  void UpdateUniforms(bool bSet) override;
  
  
private:

  Timer* m_timer;
  
};








/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
#endif