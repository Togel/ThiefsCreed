#version 420 core

layout(binding =0) uniform sampler2D texture;

in vec2 textureCoords;

out vec4 color;


void main(){

  //if (textureCoords.x <= 0.01 || textureCoords.y <= 0.01 || textureCoords.x >= 0.99 || textureCoords.y >= 0.99)
  //{
  //  color = vec4(1.0, 0.0, 0.0, 1.0);
  //}
  //else
  //{
  //  color = vec4(texture2D(texture, textureCoords).rgb, 1.0);
  //}

  color = vec4(texture2D(texture, textureCoords).rgb, 1.0);
}
