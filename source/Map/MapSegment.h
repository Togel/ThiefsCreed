#ifndef _MAP_SEGMENT_
#define _MAP_SEGMENT_

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include <map>
#include <vector>
#include "glm.hpp"

#include "../Defines.h"

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/
class Geometry;

/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/

enum Direction
{
  North,
  West,
  East,
  South
};


class MapSegment
{
public:
  /*
  * Constructor.
  */
  MapSegment(int iID, glm::vec2 v2Pos);

  /*
  * Destructor.
  */
  virtual ~MapSegment();

  /*
  * Sets a Pointer to a neighboring Segment in a given Direction.
  * @param 'eDir' Direction from which you want to know the Neighbor.
  * @param 'pNeighbor' Pointer to a Neighboring Map Segment.
  */
  void SetNeighbor(Direction eDir, MapSegment* pNeighbor);

  /*
  * Returns a Pointer to the neighboring Segment in a given Direction.
  * Check for Null-Pointers on use!
  * @param 'eDir' Direction from which you want to know the Neighbor.
  * @return Pointer to a neighboring Map Segment.
  */
  MapSegment* GetNeighbor(Direction eDir);

  /*
  * Returns a Vector storing all neighboring Map Segments.
  * @return A Vector storing all neighboring Map Segments.
  */
  std::vector<MapSegment*> GetNeighbors();

  /*
  * Returns the Center Position of the Map Segment.
  */
  glm::vec2 GetPosition();

  /*
  * Redefines the state of a Segment (final or not final).
  * @param 'bFinal' true iff Segment should be final.
  */
  void SetFinal(bool bFinal);

  /*
  * Checks whether this Segment is a final Segment (Player wins in final Segments).
  * @return 'true' iff this Segment is final.
  */
  bool IsFinal();

private:
  std::map<Direction, MapSegment*> m_mNeighbors; // Stores up to 4 Neighbors and their relative Direction.

  bool m_bIsFinal = false; // Stores whether the Segment is final or not.

  int m_iID = 0;
  
  glm::vec2 m_v2Position = glm::vec2(0.0f, 0.0f);
};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
#endif