#version 420 core


layout (location = 0) in vec3 Position;
layout (location = 1) in vec3 Normal;
layout (location = 2) in vec2 TexCoord;
layout (location = 3) in vec3 Tangent;
layout (location = 4) in vec3 Bitangent;

uniform mat4 matModel;
uniform mat4 matInvTransModel;

uniform int bOrientNormalToPlayer = 0;

layout(std140) uniform Camera{
    mat4 matView;
    mat4 matProjection;
	mat4 matInvTransView;
};

out vec2 vTexCoord;
out vec3 vNormal;
out vec3 vPosCameraSpace;
out vec3 vPosWorld;
out vec3 vTangentCameraSpace;
out vec3 vBitangentCameraSpace;

void main()
{

  vec4 posWorldSpace = matModel * vec4(Position, 1.0);

  vPosWorld = posWorldSpace.xyz;


  vec4 posCameraSpace = matView * posWorldSpace;

	vPosCameraSpace = posCameraSpace.xyz;

  
  gl_Position = matProjection * posCameraSpace;
	vTexCoord = TexCoord;
	vNormal = (matInvTransView * matInvTransModel * vec4(Normal, 0.0)).xyz;

  // orient normal to player is needed
  if (bOrientNormalToPlayer != 0)
  {
    if (dot(vNormal, -vPosCameraSpace) < 0.0f)
    {
      vNormal = -vNormal;
    }
  }

  // Tangent Space coordinates
  vTangentCameraSpace = (matView * matModel * vec4(Tangent, 0.0)).xyz;
  vBitangentCameraSpace = (matView * matModel * vec4(Bitangent, 0.0)).xyz;
}
