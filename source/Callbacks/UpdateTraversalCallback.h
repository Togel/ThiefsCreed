#ifndef _UPDATE_TRAVERSAL_CALLBACK_
#define __

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "Callback.h"

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/
class Node;
class CollisionHandling;

/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class UpdateTraversalCallback : public Callback 
{
public:
  UpdateTraversalCallback(Node* pRoot, CollisionHandling* p_pCollisionHandler);
  ~UpdateTraversalCallback();

  virtual void HandleEvents(Timer* pTimer);

private:
  void RecUpdate(Node* pNode, Timer* pTimer);

  Node* m_pRoot;
  CollisionHandling* m_pCollisionHandler;

};


/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
#endif