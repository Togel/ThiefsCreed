#ifndef _SHADER_
#define _SHADER_

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "GL/glew.h"
#include <vector>
#include <map>

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/


/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class Shader
{
public:
  /*
  * Constructor.
  */
  Shader(GLuint glCameraUniformBufferID);

  /*
  * Destructor.
  */
  ~Shader();

  /*
  * Initializes a new Shader from a given File.
  * @param uiShaderTyp Defines the Shader Type.
  * @param sShader Defines the Path to the Shader.
  */
  bool InitShaderFromFile(const unsigned int uiShaderType, const std::string& sShader);

  /*
  * Initializes a new Shader from a given String.
  * @param uiShaderTyp Defines the Shader Type.
  * @param sShader Defines the Shader.
  */
  bool InitShaderFromString(const unsigned int uiShaderType, const std::string& sShader);

  /*
  * Initializes a new Shader Pair from given Files.
  * @param sVertexShader Defines the Path to the Vertex Shader.
  * @param sFragmentShader Defines the Path to the Fragment Shader.
  */
  bool InitFromFiles(const std::string& sVertexShader, const std::string& sFragmentShader);

  /*
  * Initializes a new Shader Pair from given Strings.
  * @param sVertexShader Defines the Vertex Shader.
  * @param sFragmentShader Defines the Fragment Shader.
  */
  bool InitFromStrings(const std::string& sVertexShader, const std::string& sFragmentShader);

  /*
  * Links the Shader to make them executable.
  */
  bool Link();

  /*
  * Deletes the Shader Object.
  */
  void Destroy();

  /*
  * Activate the Shader.
  */
  void Bind();

  /*
  * Deactivate the Shader.
  */
  void Release();

  /*
  * Returns the Shader Program.
  * @return The Shader Program.
  */
  GLuint GetProgram() const;

  /*
  * Returns the Location Index of the specific Uniform Variable.
  * @param sName The Name of the Uniform Variable.
  * @return The Location Index of the specific Uniform Variable.
  */
  GLuint GetUniformLocation(const std::string& sName) const;

  /*
  * Modifies the Value of a Uniform Variable.
  * @param iLoc Specifies the Location of the Uniform Variable to be modified.
  * @param pValue Specifies a Pointer to an Array that will be used to update the specified Uniform Variable.
  */
  void SetUniform(int iLoc, const GLfloat* pValue);

  /*
  * Modifies the Value of a Uniform Variable.
  * @param iLoc Specifies the Location of the Uniform Variable to be modified.
  * @param iUniform1 Specifies a new Value to be used for the specified Uniform Variable.
  */
  void SetUniform(int iLoc, int iUniform1);

  /*
  * Modifies the Value of a Uniform Variable.
  * @param iLoc Specifies the Location of the Uniform Variable to be modified.
  * @param fUniform1 Specifies a new Value to be used for the specified Uniform Variable.
  */
  void SetUniform(int iLoc, float fUniform1);

  /*
  * Modifies the Value of a Uniform Variable Array.
  * @param iLoc Specifies the Location of the Uniform Variable to be modified.
  * @param fUniform1 Specifies a new Value to be used for the specified Uniform Variable.
  * @param fUniform2 Specifies a new Value to be used for the specified Uniform Variable.
  */
  void SetUniform(int iLoc, float fUniform1, float fUniform2);

  /*
  * Modifies the Value of a Uniform Variable Array.
  * @param iLoc Specifies the Location of the Uniform Variable to be modified.
  * @param fUniform1 Specifies a new Value to be used for the specified Uniform Variable.
  * @param fUniform2 Specifies a new Value to be used for the specified Uniform Variable.
  * @param fUniform3 Specifies a new Value to be used for the specified Uniform Variable.
  */
  void SetUniform(int iLoc, float fUniform1, float fUniform2, float fUniform3);

  /*
  * Modifies the Value of a Uniform Variable Array.
  * @param iLoc Specifies the Location of the Uniform Variable to be modified.
  * @param fUniform1 Specifies a new Value to be used for the specified Uniform Variable.
  * @param fUniform2 Specifies a new Value to be used for the specified Uniform Variable.
  * @param fUniform3 Specifies a new Value to be used for the specified Uniform Variable.
  * @param fUniform4 Specifies a new Value to be used for the specified Uniform Variable.
  */
  void SetUniform(int iLoc, float fUniform1, float fUniform2, float fUniform3, float fUniform4);

  /*
  * Creates a Map with String-GLint-Pairs defining the Name and the Uniform.
  */
  void MapUniforms();

  /*
  * Returns whether the Shader is usable.
  * @return 'true' iff the Shader is usable.
  */
  bool IsUsable();

  /*
  * Returns the Uniform Buffer ID.
  * @return The Uniform Buffer ID.
  */
  GLuint GetCameraUniformBufferID();

private:
  GLuint m_uiProgram = 0;
  GLuint m_uiCameraUniformBufferID = 0;

  GLuint m_uiMatViewIndex;
  GLint m_uiMatViewOffset;
  GLuint m_uiMatProjectionIndex;
  GLint m_uiMatProjectionOffset;
  GLuint m_uiMatInvTransViewIndex;
  GLint m_uiMatIntTransViewOffset;

  bool m_bUsable = false;

  std::vector<GLuint> m_vecVertexShader;
  std::vector<GLuint> m_vecFragmentShader;
  std::vector<GLuint> m_vecTesselationControlShader;
  std::vector<GLuint> m_vecTesselationEvaluationShader;
  std::vector<GLuint> m_vecGeometryShader;
  std::vector<GLuint> m_vecComputeShader;

  std::map<std::string, GLint> m_vecUniforms;
};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
#endif
