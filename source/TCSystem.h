#ifndef _TC_SYSTEM_
#define _TC_SYSTEM_

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "GL/glew.h"
#include <vector>
#include <thread>

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/
class ThreadDraw;
class ThreadGeometry;
class ConfigLoader;
class Callback;
class GameManager;
class UpdateTraversalCallback;
class CameraBufferCallback;
class CollisionHandling;
class ShadowMappingCallback;
class ClothIntegrationCallback;

/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class TCSystem
{
public:
  /*
  * Constructor.
  */
  TCSystem();
  
  /*
  * Destructor.
  */
  ~TCSystem();

  /*
  * Initialize the System.
  */
  void Init();

  /*
  * Loads a test config so that the without need for other code clothing can be tested end developed
  */
  void LoadClothTestConfig();

  /*
  * Run the System.
  */
  void Run();
  
  /*
  * Stops the System.
  */
  void Quit();

  /*
  * Adds an additional Callback before the Draw Calls.
  * @param 'pCallback' defines the new Callback.
  */
  void AddPreDrawCallback(Callback* pCallback);

  /*
  * Adds an additional Callback after the Draw Calls.
  * @param 'pCallback' defines the new Callback.
  */
  void AddPostDrawCallback(Callback* pCallback);

  /*
  * Adds an additional Geometry Callback.
  * @param 'pCallback' defines the new Callback.
  */
  void AddGeometryCallback(Callback* pCallback);

  /*
  * Tries to delete a Callback.
  * @param 'pCallback' defines the Callback which should be deleted.
  * @return 'true' iff the Callback could be deleted.
  */
  bool DeleteCallback(Callback* pCallback);

private:
  ThreadDraw* m_pDrawThread = nullptr;
  UpdateTraversalCallback* m_pUpdateCallback = nullptr;
  CameraBufferCallback* m_pCameraCallback = nullptr;
  ShadowMappingCallback* m_pShadowMappingCallback = nullptr;
  ClothIntegrationCallback* m_pClothIntegrationCallback = nullptr;
  std::vector<ThreadGeometry*> m_vecGeoThreads;
  
  std::vector<std::thread> m_vecThreads;
  
  GameManager* m_pGameManager;
  //ConfigLoader* pConfLoader = nullptr;
  
  CollisionHandling* m_pCollisionHandler = nullptr;
  
  bool m_bRun = true; // Call Quit to stop the System.

  bool m_bEndScreen = false;
  
  int m_iNumOfThreads = 0;
  
  /*
  * Determines the Number of Cores available.
  * @return The Number of Cores available.
  */
  int GetNumOfCores();

};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
#endif