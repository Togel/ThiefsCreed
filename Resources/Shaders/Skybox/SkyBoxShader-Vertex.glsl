#version 420 core

layout (location = 0) in vec3 vertexPosition_modelspace;
layout (location = 1) in vec3 vertexNormal_modelspace;
layout (location = 2) in vec2 textureCoords_modelspace;

uniform mat4 matModel;
uniform mat4 matInvTransModel;
uniform mat4 matRotation;

layout(std140) uniform Camera{
  mat4 matView;
  mat4 matProjection;
  mat4 matInvTransView;
};



//out vec4 vertexNormal;
out vec2 textureCoords;
out vec3 vertexPos_modelspace;


void main(){
  
  //vertexNormal = matInvTransView * matInvTransModel * vec4(vertexNormal_modelspace, 0.0);
  textureCoords = textureCoords_modelspace;
  
  vertexPos_modelspace = (matRotation * vec4(vertexPosition_modelspace, 1.0)).xyz;
  
  
  // translate according to player position
  vec4 pos = vec4(vertexPosition_modelspace, 0.0);
  pos = pos + matModel[3];
  pos[3] = 1.0;
  
  // only use view and projection from here on
  gl_Position = matProjection * matView * pos;
  
  // project to far clipping plane
  //gl_Position =  gl_Position.xyww;
}