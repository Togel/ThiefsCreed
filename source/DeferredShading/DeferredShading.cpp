#include "DeferredShading.h"
/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "../RessourceManagement/Shader.h"
#include "GBuffer.h"
#include "../LightManager.h"
#include "../SceneGraph/LightNode.h"
#include "../SceneGraph/CameraNode.h"
#include "../TC.h"

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/
DeferredShading::DeferredShading(LightManager* pLightManager, GBuffer* pGBuffer, CameraNode* pCamNode)
  : m_pLightManager(pLightManager)
  , m_pCamNode(pCamNode)
  , m_pGBuffer(pGBuffer)
{
  Init(pCamNode->GetUniformBufferID());
}

DeferredShading::~DeferredShading()
{


}


/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/
void DeferredShading::DoShading()
{
  std::vector<LightNode*> vecLights = m_pLightManager->GetLights();
  glm::mat4 ViewMat = m_pCamNode->GetViewMatrix();


  m_pLightingShader->Bind();
  m_pGBuffer->BindForReading();

  glBindVertexArray(m_QuadVAO);


  if (vecLights.size() > 0)
  {
    LightNode* pLight = vecLights[0];
    UploadLightToShader(ViewMat, pLight);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
  }

  glEnable(GL_BLEND);
  glBlendEquation(GL_FUNC_ADD);
  glBlendFunc(GL_ONE, GL_ONE);

  glDepthFunc(GL_LEQUAL);

  for (unsigned int i = 1; i < vecLights.size(); i++)
  {
    LightNode* pLight = vecLights[i];

    UploadLightToShader(ViewMat, pLight);

    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

  }

  glDisable(GL_BLEND);
  glDepthFunc(GL_LESS);


  glBindVertexArray(0);

  m_pLightingShader->Release();
}

void DeferredShading::Init(GLuint CamBufferID)
{
  GLfloat quadVertices[] = 
  {
    -1.0f,  1.0f, 0.0f,    0.0f, 1.0f,
    -1.0f, -1.0f, 0.0f,    0.0f, 0.0f,
     1.0f,  1.0f, 0.0f,    1.0f, 1.0f,
     1.0f, -1.0f, 0.0f,    1.0f, 0.0f,
  };

  glGenVertexArrays(1, &m_QuadVAO);
  glGenBuffers(1, &m_QuadVBO);
  glBindVertexArray(m_QuadVAO);

  glBindBuffer(GL_ARRAY_BUFFER, m_QuadVBO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
  glEnableVertexAttribArray(1);
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));

  m_pLightingShader = new Shader(CamBufferID);
  m_pLightingShader->InitFromFiles("./Resources/Shaders/DeferredShading/LightingShader-vertex.glsl", "./Resources/Shaders/DeferredShading/LightingShader-fragment.glsl");
  m_pLightingShader->InitShaderFromFile(GL_FRAGMENT_SHADER, "./Resources/Shaders/BlinnPhong.glsl");
  m_pLightingShader->Link();

  m_uniformPosLightPosition = m_pLightingShader->GetUniformLocation("inLight.lightPosition");
  m_uniformPosLightDirection = m_pLightingShader->GetUniformLocation("inLight.lightDirection");
  m_uniformPosLightIntensity = m_pLightingShader->GetUniformLocation("inLight.lightIntensity");
  m_uniformPosLightConeAngle = m_pLightingShader->GetUniformLocation("inLight.lightConeAngle");
  m_uniformPosLightAmbientCoefficient = m_pLightingShader->GetUniformLocation("inLight.lightAmbientCoefficient");
  m_uniformPosLightAttenuation = m_pLightingShader->GetUniformLocation("inLight.lightAttenuation");
  m_uniformPosLightType = m_pLightingShader->GetUniformLocation("inLight.lightType");
  m_uniformPosMatView = m_pLightingShader->GetUniformLocation("matLightView");
  m_uniformPosMatProj = m_pLightingShader->GetUniformLocation("matLightProj");
}

void DeferredShading::UploadLightToShader(glm::mat4 ViewMat,LightNode* pLight)
{

  glm::vec3 pos = glm::vec3(ViewMat * glm::vec4(pLight->GetLightPosition(), 1.0));
  glm::vec3 dir = glm::vec3(ViewMat * glm::vec4(pLight->GetLightDirection(), 0.0));;
  glm::vec3 intens = pLight->GetLightIntensity();

  m_pLightingShader->SetUniform(m_uniformPosLightPosition, pos[0], pos[1], pos[2]);
  m_pLightingShader->SetUniform(m_uniformPosLightDirection, dir[0], dir[1], dir[2]);
  m_pLightingShader->SetUniform(m_uniformPosLightIntensity, intens[0], intens[1], intens[2]);
  m_pLightingShader->SetUniform(m_uniformPosLightConeAngle, pLight->GetLightConeAngle());
  m_pLightingShader->SetUniform(m_uniformPosLightAmbientCoefficient, pLight->GetLightAmbientCoefficient());
  m_pLightingShader->SetUniform(m_uniformPosLightAttenuation, pLight->GetLightAttenuation());
  m_pLightingShader->SetUniform(m_uniformPosLightType, (float)pLight->GetLightType());
  pLight->BindTexture();
  m_pLightingShader->SetUniform(m_uniformPosMatView, &pLight->GetMatView()[0][0]);
  m_pLightingShader->SetUniform(m_uniformPosMatProj, &pLight->GetMatProj()[0][0]);
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/


