#include "LightManager.h"
/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "SceneGraph/LightNode.h"
#include "SceneGraph/CameraNode.h"

#include <cstring>
/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/
LightManager::LightManager()
{

}

LightManager::~LightManager()
{
  
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/
LightNode * LightManager::CreateLight()
{
  LightNode* pNewLight = new LightNode();

  m_vecLights.push_back(pNewLight);

  return pNewLight;
}

std::vector<LightNode*>& LightManager::GetLights()
{
  return m_vecLights;
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
