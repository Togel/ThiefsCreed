#ifndef Node_h_
#define Node_h_

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include <vector>
#include<stdlib.h>
#include <string>

#include "glm.hpp"

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/

/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/
class Timer;

/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class Node
{
public:
	Node();
	virtual ~Node();

	/*
	* Returns a string used to determine the type of the node
	* To work properly this has to be implemented in all inheriting nodetypes
	* @return string that encodes type of the node
	*/
	virtual std::string GetNodeType();

	/*
	* Gets the pointer to the node's parent.
	* @return pointer to the node's parent.
	*/
	Node* GetParent();
	
	/*
	* Get the number of children this node has.
	* @return unsigned number of children.
	*/
	size_t GetNumChildren();

	/*
	* Grants access to the collection of nodes children
	* @return pointer to the vector of child-nodes
	*/
	std::vector<Node*>& GetChildren();

	/*
	* Adds a child to this node
	* @param the new child
	*/
	void AddChild(Node* pChild);

	/*
	* Removes a child form this node
	* @param the child to delete
	* @return 'true' iff the node was deleted
	*/
	bool RemoveChild(Node* pChild);

	/*
	* Gets called by the update threads
	* Override in inherting nodetypes to implement some
	* physical behaviour
	* @param Timer to determine time between frames
	*/
	virtual void Update(Timer* pTime);

  /*
   * Calculates the accumulated transformations of all
   * ancestor nodes.
   * @return The accumulated transormation of all
   * ancestor nodes.
   */
  glm::mat4 getAncestorTransformation();

  void SetActive(bool bActive);

  bool IsActive();

private:

	/*
	* Sets the parent of this node
	* This should only be done implicit by setting it as a child
	* @param pointer to this nodes new parent
	*/
	void SetParent(Node* pParent);


	// Graph structure
	Node* m_pParent = nullptr;
	std::vector<Node*> m_vecChildren;

  bool m_bActive = true;
};


/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
#endif
