/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "RotatingMeshNode.h"
#include "TransformNode.h"
#include "../Timer.h"
#include "../TC.h"

#include <gtx/rotate_vector.hpp>
#include <gtx/vector_angle.hpp>
#include "../RessourceManagement/Mesh.h"

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

RotatingMeshNode::RotatingMeshNode(glm::vec3 v3Rotation, float fMaxAngle, float fAnimationSpeed, Shader* pShader, Mesh* pMesh, Texture* pTexture, Texture* pNormalMap /*= nullptr*/, float fAlpha /*= 1.0f*/)
  : MeshNode(pShader, pMesh, pTexture, pNormalMap, fAlpha)
{
  m_v3RotAxis  = v3Rotation;
  m_fMaxAngle  = fMaxAngle;
  m_fAniSpeed  = fAnimationSpeed;
}

RotatingMeshNode::RotatingMeshNode(glm::vec3 v3Rotation, float fMaxAngle, float fAnimationSpeed, Shader* pShader, std::vector<DrawableMesh> vecMeshes)
  : MeshNode(pShader, vecMeshes)
{
  m_v3RotAxis  = v3Rotation;
  m_fMaxAngle  = fMaxAngle;
  m_fAniSpeed  = fAnimationSpeed;
}

RotatingMeshNode::~RotatingMeshNode()
{
  m_v3CompVec  = glm::vec3(0.0f, 0.0f, 0.0f);
  m_fMaxAngle  = 0.0f;
  m_fAniSpeed  = 0.0f;
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

void RotatingMeshNode::Update(Timer* pTimer)
{
  TransformNode* pTransform = dynamic_cast<TransformNode*>(this->GetParent());

  float fElapsedTime = pTimer->GetElapsedTime();

  // If the the maximal Angle has been overshot we need to rotate in the opposite Direction.
  if ((m_fMaxAngle != 360.0f) && (m_fCurAngle >= m_fMaxAngle))
    m_fDirection = -m_fDirection;

  // Update Rotations from the Transform Node and the Compare Vector: s = v*t
  pTransform->Rotate(m_fDirection * m_fAniSpeed * fElapsedTime, m_v3RotAxis);
  m_v3CompVec = glm::rotate(m_v3CompVec, m_fDirection * m_fAniSpeed * fElapsedTime, m_v3RotAxis);

  // Recalculate Rotation Angle.
  m_fCurAngle = glm::angle(m_v3CompVec, glm::vec3(0.0f, 1.0f, 0.0f));
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/