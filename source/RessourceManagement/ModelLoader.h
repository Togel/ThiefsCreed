#ifndef _MODEL_LOADER_
#define _MODEL_LOADER_

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include <vector>
#include <string>
#include "glm.hpp"

#include "Mesh.h"


/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/

/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class ModelLoader
{
public:

  /*
   * Loads a model. Delegates to the appropriate loader
   * (e.g. for .obj files).
   * @return Loading successfull.
   */
  bool loadModel(const std::string pFilename);

  /*
   * Copies the buffer content to the given mesh.
   * Note: The mesh needs to be initialized afterwards
   * -> call mesh.init()
   */
  void copyBufferToMesh(Mesh* pMesh);

  /*
   * Creates a Mesh from the current buffer.
   * @return A mesh with the stored geometry from the buffer or 0
   * if no object has been loaded.
   */
  Mesh* createMeshFromBuffer();

  /*
   * Combines loadModel() and createMesh() for easier handling.
   * @return A mesh loaded from the given file or 0
   * if no model could be loaded.
   */
  Mesh* loadMeshFromFile(const std::string pFilename);

  /*
   * Clears everything from the previous reading.
   */
  void clearModelBuffer();


private:

  /*
  * Loads an .obj file into the buffer (member functions).
  * @return Loading successfull.
  */
  bool loadObjFile(const std::string pFilename);

  /*
   * .obj file indices can be different for vertices, textures
   * and normals, but only one set of indices can be passed
   * to OpenGL, so additional vertices need to be created to
   * compensate this.
   */
  void adjustObjIndices();


private:
  std::vector<unsigned int> m_vecFaces_vertexIds;
  std::vector<unsigned int> m_vecFaces_normalIds;
  std::vector<unsigned int> m_vecFaces_UVIds;
  std::vector<glm::vec3> m_vecVertices;
  std::vector<glm::vec3> m_vecNormals;
  std::vector<glm::vec2> m_vecUVs;

  bool m_modelRead = false;

};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
#endif