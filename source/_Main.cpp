/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "TCSystem.h"
#include "TC.h"
#include "Map/Map.h"
#include <thread>

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

int main()
{
  TCSystem* pSystem = new TCSystem();
  pSystem->Run();

  return 0;
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
