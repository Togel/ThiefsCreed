#ifndef _TEXTURE_LOADER_
#define _TEXTURE_LOADER_

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include <vector>
#include <string>
//#include "glm.hpp"

#include "Texture.h"


/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/

/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class TextureLoader
{

public:

  /*
   * Loads a texture. Delegates to the appropriate loader
   * (e.g. for .dds images).
   * @return Loading successfull.
   */
  bool loadTexture(const std::string pImagePath);

  /*
   * Creates a Mesh from the current buffer.
   * @return A texture with the stored Texture Buffer index or 0
   * if no texture has been loaded.
   */
  Texture* createTextureFromBuffer();

  /*
   * Combines loadTexture() and createTextureFromBuffer() for easier handling.
   * @return A Texture loaded from the given image or 0
   * if no model texture be loaded.
   */
  Texture* loadTextureFromFile(const std::string pImagePath);


  /*
   * Load a Cube Map Texture from .dds files and creates a texture.
   * @return A Cube Map Texture of the given files.
   */
  Texture* loadCubeMapFromDDSFile(const std::string pTop, const std::string pBottom,
                              const std::string pFront, const std::string pBack,
                              const std::string pLeft, const std::string pRight);

  /*
   * Loads a .dds image as texture and stores the buffer index as member variable.
   * @return True iff loading was successfull.
   */
  bool loadDDS(const std::string pImagePath);

  /*
   * Load a Cube Map Texture from .dds files.
   * @return True iff loading was successfull.
   */
  bool loadCubeMapFromDDS(const std::string pTop, const std::string pBottom,
                              const std::string pFront, const std::string pBack,
                              const std::string pLeft, const std::string pRight);


private:
  GLuint m_iTexID = 0;
  std::string m_sPath = "";
  GLenum m_eTextureType = GL_TEXTURE_2D;

  bool m_textureRead = false;

};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
#endif