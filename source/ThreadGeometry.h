#ifndef _THREAD_GEOMETRY_
#define _THREAD_GEOMETRY_

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include <vector>

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/
class Callback;
class Timer;

/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class ThreadGeometry
{
public:
  /*
  * Constructor.
  */
  ThreadGeometry();
  
  /*
  * Destructor.
  */
  ~ThreadGeometry();
  
  /*
  * Executes all Geometry-Callbacks.
  */
  void Run();

  /*
  * Returns a Vector storing all Callbacks.
  * @return A Vector storing all Callbacks.
  */
  std::vector<Callback*>& GetCallbacks();

  /*
  * Adds an additional Callback.
  * @param pCallback Callback which should be added.
  */
  void AddCallback(Callback* pCallback);

  /*
  * Deletes a specific Callback.
  * @param pCallback Callback which should be removed.
  * @return 'true' iff the Callback could be removed.
  */
  bool DeleteCallback(Callback* pCallback);

  /*
  * Calculates the Time of the Execution of all Callbacks.
  * @return The Time of the Execution of all Callbacks.
  */
  float CalcDuration();

private:
  Timer* m_pTimer = nullptr;

  std::vector<Callback*> m_vecCallbacks;

  float m_fDuration = 0.0f;
};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
#endif