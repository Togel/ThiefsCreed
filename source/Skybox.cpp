#define SKYBOX_EFFECT_START_POS glm::vec3(10.0f, 0.0f, 0.0f)
#define SKYBOX_EFFECT_END_POS glm::vec3(-10.0f, 0.0f, 0.0f)
#define SKYBOX_EFFECT_SPEED 0.1f

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "Skybox.h"

#include "./SceneGraph/Node.h"
#include "./SceneGraph/TransformNode.h"
#include "./SceneGraph/PlayerNode.h"
#include "./SceneGraph/MeshNode.h"
#include "./SceneGraph/SkyBoxNode.h"
#include "./SceneGraph/TranslatingMeshNode.h"

#include "./RessourceManagement/ResourceCollection.h"
#include "./RessourceManagement/Mesh.h"
#include "./RessourceManagement/Shader.h"

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

Skybox::Skybox(PlayerNode* pPlayer, ResourceCollection* pCollection)
{
  PlaceInSceneGraph(pPlayer, pCollection);
}

Skybox::~Skybox()
{

}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

void Skybox::PlaceInSceneGraph(PlayerNode* pPlayer, ResourceCollection* pCollection)
{
  // Structure in SceneGraph -> R_N: Root Node, T: TransformNode, M_N: MeshNode, T_M_N: TranslatingMeshNode, P: PlayerNode
  //                            C_T_N: CameraTransformNode
  //    R_N
  //     |
  //   C_T_N
  //   __|__
  //  /  |  \
  // P  M_N  T
  //         |
  //       T_M_N



  // Load Meshes, Textures and Shader.
  Mesh* pSkybox = pCollection->GetMesh("Skybox");
  Texture* pSkyboxTex = pCollection->GetTexture("Skybox");
  Shader* pShader = pCollection->GetShader("SkyBoxShader");

  // Create Node.
  SkyBoxNode* pSkyboxMeshNode = new SkyBoxNode(pShader, pSkybox, pSkyboxTex, nullptr, 1.0f, DrawableMesh::DrawType::DT_NO_DEPTH_BUFFER);
  
  // for no rotation of the skybox replace by:
  //  MeshNode* pSkyboxMeshNode = new MeshNode(pShader, pSkybox, pSkyboxTex, nullptr, 1.0f, DrawableMesh::DrawType::DT_NO_DEPTH_BUFFER);

  // add to scene graph
  TransformNode* pPlayerParent = dynamic_cast<TransformNode*>(pPlayer->GetParent());
  pPlayerParent->AddChild(pSkyboxMeshNode);





  /*
   * Add moving effect (e.g. space ship), still to do.
   */

  //  Mesh* pEffect = pCollection->GetMesh("Skybox_Effect");
  //  Texture* pEffectTex = pCollection->GetTexture("Skybox_Effect");

  //  TransformNode* pEffectTrans  = new TransformNode();
  //  TranslatingMeshNode* pEffectMeshNode = new TranslatingMeshNode(SKYBOX_EFFECT_START_POS, SKYBOX_EFFECT_END_POS, SKYBOX_EFFECT_SPEED, pShader, pEffect, pEffectTex);

    // Connect Nodes.
  //  pEffectTrans->AddChild(pEffectMeshNode);
  //  pPlayerParent->AddChild(pEffectTrans);

}


/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/