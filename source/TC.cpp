/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "TC.h"
#include <algorithm>

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/


/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

namespace tc
{
  glm::mat3 TransIntoRotMatrix(glm::vec3 v3Rot)
  {
    float fSinX = glm::sin(v3Rot.x);
    float fSinY = glm::sin(v3Rot.y);
    float fSinZ = glm::sin(v3Rot.z);
    float fCosX = glm::cos(v3Rot.x);
    float fCosY = glm::cos(v3Rot.y);
    float fCosZ = glm::cos(v3Rot.z);

    glm::mat3 m3Rotation = glm::mat3(
      fCosY*fCosZ, fCosZ*fSinX*fSinY - fCosX*fSinZ, fCosX*fCosZ*fSinY + fSinX*fSinZ,
      fCosY*fSinZ, fCosX*fCosZ + fSinX*fSinY*fSinZ, -fCosZ*fSinX + fCosX*fSinY*fSinZ,
      -fSinY, fCosY*fSinX, fCosX*fCosY);

    return m3Rotation;
  }


  std::map<std::string, std::string> priv_mConfig;

  IOInfo priv_Inf;
  IOWarning priv_War;
  IOError priv_Err;
  IOCrash priv_Crs;

  volatile bool priv_bIsCritical = false;

  void SetConsoleColor(ConsoleColor eColor)
  {
    #ifdef WIN32
      // Windows
      HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
      SetConsoleTextAttribute(hConsole, eColor.m_iWinColor);
    #else
      // Linux
      std::cout << eColor.m_sLinColor;
    #endif
  }

  void ToggleCriticalSection(bool bIsCritical)
  {
    if (bIsCritical)
    {
      while (priv_bIsCritical);
    }

    priv_bIsCritical = bIsCritical;
  }

  IOInfo inf
  {
    return priv_Inf;
  }

  IOWarning war
  {
    return (priv_War << "Warning: ");
  }

  IOError err
  {
    return (priv_Err << "Error: ");
  };

  IOCrash crs
  {
    return (priv_Crs << "Crash: ");
  }

  void LoadConfigFile(std::string sName)
  {

  }

  std::string GetConfigStr(std::string sName)
  {
    return priv_mConfig[sName];
  }

  int GetConfigInt(std::string sName)
  {
    return std::stoi(priv_mConfig[sName]);
  }

  float GetConfigFlt(std::string sName)
  {
    return std::stof(priv_mConfig[sName]);
  }

  bool GetConfigBoo(std::string sName)
  {
    std::string sConf = priv_mConfig[sName];
    std::transform(sConf.begin(), sConf.end(), sConf.begin(), ::toupper);

    if (sConf == "TRUE")
      return true;
    else
      return false;
  }
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
