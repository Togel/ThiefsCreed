#version 420 core

in vec2 vTexCoord;
in vec3 vNormal;
in vec3 vPosCameraSpace;
in vec3 vPosWorld;
in vec3 vTangentCameraSpace;
in vec3 vBitangentCameraSpace;

layout(location = 0) out vec3 PosCamSpaceOut;
layout(location = 1) out vec3 DiffuseOut;
layout(location = 2) out vec3 NormalOut;
layout(location = 3) out vec3 TexCoordOut;
layout(location = 4) out vec3 PosWorldOut;

layout(binding = 0) uniform sampler2D texture;
layout(binding = 1) uniform sampler2D normalMap;

void main()
{
  PosCamSpaceOut = vPosCameraSpace;
	DiffuseOut = texture2D(texture, vTexCoord).xyz;
	TexCoordOut = vec3(vTexCoord, 0.0);
  NormalOut = normalize(vNormal);
  PosWorldOut = vPosWorld;

  // normal mapping
  if (vTangentCameraSpace != vec3(0.0) && vBitangentCameraSpace != vec3(0.0))
  {
    // normal coords from texture
    vec3 normalCoords = texture2D(normalMap, vTexCoord).xyz;
    normalCoords = normalCoords * 2.0 - 1.0;  // map to [-1, 1]

    // normalize tangent vectors
    vec3 vTangentCameraSpaceNorm = normalize(vTangentCameraSpace);
    vec3 vBitangentCameraSpaceNorm = normalize(vBitangentCameraSpace);

    // calculate new normal
    mat3 matTangentBasis = mat3(vTangentCameraSpaceNorm, vBitangentCameraSpaceNorm, NormalOut);
    NormalOut = matTangentBasis * normalCoords;
    NormalOut = normalize(NormalOut);
  }
}