#ifndef TransformNode_h_
#define TransformNode_h_

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "Node.h"
#include <glm.hpp>
/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/


/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class TransformNode : public Node
{
public:
	TransformNode();

	virtual ~TransformNode();

	/*
	* Returns a string used to determine the type of the node
	* To work properly this has to be implemented in all inheriting nodetypes
	* @return string that encodes type of the node
	*/
	std::string GetNodeType() override;

	/*
	* Sets the transformation of this node
	* @param the new transformation matrix
	*/
	void SetTransformation(glm::mat4 matTransformation);

	/*
	* Gets the transformation of this node
	* @return glm::mat4 of the current transformation
	*/
	glm::mat4 GetTransformation();

  /*
  * Gets the Translation of this node.
  * @return glm::vec3 of the current translation
  */
  glm::vec3 GetTranslation();

	/*
	* Adds a translation to the current translation
	* @param the translation vector
	*/
	void Translate(glm::vec3 v3Translation);

  /*
  * Reset to specific Translation.
  * @param 'v3Translation' defining the new Translation Vector.
  */
  void SetTranslation(glm::vec3 v3Translation);

  /*
  * Adds a Rotation to the current Rotation.
  * @param 'm3Rotation' defines the Rotation Vector.
  */
  void Rotate(glm::mat3 m3Rotation);

  /*
   * Adds a rotation to the current transformation
   * @param angle
   * @param axis around which to rotate
   */
  void Rotate(float angle, glm::vec3 axis);

  /*
   * Scales the Transformation.
   * The scaling will not affect the translation, thus it
   * will act as if the object is scaled as first step.
   */
  void Scale(float scaleFactor);

  /*
   * Scales the Transformation individually for each axis.
   * The scaling will not affect the translation, thus it
   * will act as if the object is scaled as first step.
   */
  void Scale(glm::vec3 vecScaleFactors);

	// TODO: Further access-methods needed?

private:

	glm::mat4 m_matTransformation;

};


/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
#endif
