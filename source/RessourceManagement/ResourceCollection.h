#ifndef _RESSOURCE_COLLECTION_
#define _RESSOURCE_COLLECTION_

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include <string>
#include <map>
#include <vector>

#include "GL/glew.h"

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/
class Mesh;
class Texture;
class Shader;

/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class ResourceCollection
{
public:
  /*
  * Constructor.
  */
  ResourceCollection(GLuint iCamUniformBufferID);

  /*
  * Destructor.
  */
  virtual ~ResourceCollection();

  /*
  * Returns a Mesh by its Name iff this Mesh has been loaded before.
  * @param 'sName' defines the Name of the Mesh.
  * @return A pointer to the Mesh.
  */
  Mesh* GetMesh(std::string sName);

  /*
  * Returns a Texture by its Name iff this Texture has been loaded before.
  * @param 'sName' defines the Name of the Texture.
  * @return A pointer to the Texture.
  */
  Texture* GetTexture(std::string sName);

  /*
  * Returns a Shader by its Name iff this Shader has been loaded before.
  * @param 'sName' defines the Name of the Shader.
  * @return A pointer to the Shader.
  */
  Shader* GetShader(std::string sName);

  /*
  * Loads all Meshes, Textures and Shaders from the Resource Directory.
  */
  void LoadResources();

  /*
  * Loads a Mesh.
  * @param 'sPath' defines the Path to the Mesh.
  */
  void LoadMesh(std::string sPath);

  /*
  * Loads a Texture.
  * @param 'sPath' defines the Path to the Texture.
  */
  void LoadTexture(std::string sPath);

  /*
  * Loads a Shader.
  * @param 'sPath' defines the Path to the Shader.
  */
  void LoadShader(std::string sPath);

private:
  std::map<std::string, Mesh*>    m_mapMeshes;
  std::map<std::string, Texture*> m_mapTextures;
  std::map<std::string, Shader*>  m_mapShaders;

  GLuint m_iCamUniformBufferID = 0;
  GLuint m_iLightUniformBufferID = 0;

  /*
  * Loads a list of all Files of a wished Type in a specific Directory.
  * @param 'vecFiles' Vector in which the Files get stored.
  * @param 'sDirectory' defines the Directory Path.
  * @param 'sFileType' defines the File Type (e.g. "obj")
  */
  void GetFilesInDirectory(std::vector<std::string>& vecFiles, const std::string& sDirectory, const std::string sFileType);

  /*
  * Returns the File Name defined by a specific Path.
  * @param 'sPath' defines the Path to a File.
  * @param 'sType' defines the Type of the File.
  * @return The File Name.
  */
  std::string GetNameFromPath(const std::string sPath, const std::string sType);

  /*
  * Create some Shader manually.
  */
  void InitCustomShader();

  /*
   * Create Cube Map Textures manually.
   */
  void InitCubeMapTextures();

};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
#endif