#ifndef _DEFINES_
#define _DEFINES_

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "Settings.h"

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/

// Map
#define SEGMENT_TEX_COUNT 1
#define MAP_OFFSET 1.5f

// MapSegment
#define SEGMENT_WIDTH 4.0f


// Camera
#define CAMERA_OPENING_ANGLE 50.0f
#define NEAR_PLANE 0.01f
#define FAR_PLANE 100.0f

// Camera Transformation
#define CRITICAL_DOT 0.9f


// Cloth
#define CLOTH_SIMULATION_DISTANCE 4.0f
#define CLOTH_INTEGRATION_STEPS 16
#define NUM_CLOTH_POINTS_X 32
#define NUM_CLOTH_POINTS_Y 32
#define NUM_CLOTH_POINTS (NUM_CLOTH_POINTS_X * NUM_CLOTH_POINTS_Y)


// Player
#define PLAYER_ABOVE_FLOOR 1.3f

// Player Movement Speed
#define CAMERA_MOVEMENT_SPEED 0.002


// AI
#define AI_SCALING_FACTOR 0.35f
#define AI_CANNOT_VIEW_THROUGH_EMPTY_SPACE_IN_CORNERS true


// Collision Detection
#define HEIGHT_FOR_COL_DET_WALLS 0.45f
#define KEEP_DISTANCE_TO_WALLS_PLAYER 0.25f
#define KEEP_DISTANCE_TO_WALLS_AI (1.2 * AI_SCALING_FACTOR)
#define PLAYER_AI_DISTANCE (0.9f + AI_SCALING_FACTOR)
#define AI_AI_DISTANCE (2.25f * AI_SCALING_FACTOR)

#define SET_AI_CANNOT_COLLIDE false


// Skybox
#define SKYBOX_MILLISECS_PER_ROUND 600000.0f // one round every 10 minutes




/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
#endif