/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MeshNode.h"
#include "../TC.h"
#include "../RessourceManagement/Mesh.h"
#include "../RessourceManagement/Texture.h"
#include "../RessourceManagement/Shader.h"

#include <gtc/matrix_transform.hpp>

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

// DO NOT USE THE DEFAULT CONSTRUCTOR!
MeshNode::MeshNode()
  : DrawableNode(nullptr)
{
}

MeshNode::MeshNode(Shader* pShader, Mesh* pMesh, Texture* pTexture, Texture* pNormalMap, float fAlpha /*= 1.0f*/,
                   DrawableMesh::DrawType pDrawType)
  : DrawableNode(pShader)
{
  // Insert the Mesh in the Vector.
  DrawableMesh oMesh = DrawableMesh(pMesh, fAlpha, pTexture, pNormalMap, pDrawType);
  m_vecMeshes.push_back(oMesh);

  // compute tangent basis if normal map is given
  if (pNormalMap != nullptr)
  {
    pMesh->ComputeTangentBasis();
  }
}

MeshNode::MeshNode(Shader* pShader, std::vector<DrawableMesh> vecMeshes)
  : DrawableNode(pShader)
{
  // Store the Meshes.
  std::vector<DrawableMesh>::iterator it;

  for (it = vecMeshes.begin(); it != vecMeshes.end(); ++it)
  {
    m_vecMeshes.push_back(*it);
    (*it).m_pMesh->ComputeTangentBasis();
  }
}

MeshNode::~MeshNode()
{
  m_vecMeshes.clear();
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

void MeshNode::DrawOpaque(glm::mat4 matTransformation)
{
  std::vector<DrawableMesh>::iterator it;

  for (it = m_vecMeshes.begin(); it != m_vecMeshes.end(); ++it)
  {
    DrawableMesh oMesh = *it;
    if (oMesh.m_eDrawType == DrawableMesh::DT_OPAQUE)
      Draw(matTransformation, oMesh);
  }
}

void MeshNode::DrawTransparent(glm::mat4 matTransformation)
{
  std::vector<DrawableMesh>::iterator it;

  for (it = m_vecMeshes.begin(); it != m_vecMeshes.end(); ++it)
  {
    DrawableMesh oMesh = *it;

    if (oMesh.m_eDrawType == DrawableMesh::DT_TRANSPARENT)
      Draw(matTransformation, oMesh);
  }
}

void MeshNode::DrawWithoutDepthBuffer(glm::mat4 matTransformation)
{
  std::vector<DrawableMesh>::iterator it;

  for (it = m_vecMeshes.begin(); it != m_vecMeshes.end(); ++it)
  {
    DrawableMesh oMesh = *it;

    if (oMesh.m_eDrawType == DrawableMesh::DT_NO_DEPTH_BUFFER)
      Draw(matTransformation, oMesh);
  }
}

Mesh* MeshNode::getFirstStoredMesh()
{
  // no Mesh stored
  if (m_vecMeshes.size() == 0)
    return nullptr;

  DrawableMesh dm = m_vecMeshes.front();
  return dm.m_pMesh;
}

void MeshNode::Draw(glm::mat4 matTransformation, DrawableMesh oMesh)
{
  GLuint iModelMatrix;
  GLuint iInvTransModel;
  GLuint iAlpha;

  // Update Uniforms.
  if (m_pShader != nullptr)
  {
    m_pShader->Bind();
    
    UpdateUniforms(true);
    
    // Update Position.
    iModelMatrix = m_pShader->GetUniformLocation("matModel");
    m_pShader->SetUniform(iModelMatrix, &matTransformation[0][0]);
    
    glm::mat4 matInvTransModel = glm::transpose(glm::inverse(matTransformation));
    
    iInvTransModel = m_pShader->GetUniformLocation("matInvTransModel");
    m_pShader->SetUniform(iInvTransModel, &matInvTransModel[0][0]);
    
    // Update Alpha.
    iAlpha = m_pShader->GetUniformLocation("Alpha");
    m_pShader->SetUniform(iAlpha, oMesh.m_fAlpha);
    
    // Bind Texture.
    if (oMesh.m_pTexture != nullptr)
      oMesh.m_pTexture->Bind(GL_TEXTURE0);
    
    // Bind Normal Map.
    Texture* pNormalMap = oMesh.m_pNormalMap;
    
    if (pNormalMap != nullptr)
      pNormalMap->Bind(GL_TEXTURE1);
  }
  
  // Draw Mesh.
  oMesh.m_pMesh->Draw();

  if (m_pShader != nullptr)
  {
    //// Unbind Textures.
    //oMesh.m_pTexture->Unbind();
    //
    //if (oMesh.m_pNormalMap != nullptr)
    //  oMesh.m_pNormalMap->Unbind();
    
    // Reset Uniforms.
    m_pShader->SetUniform(iAlpha, 1.0f);
    m_pShader->SetUniform(iModelMatrix, &mat4NullMatrix[0][0]);
    
    UpdateUniforms(false);
    m_pShader->Release();
  }
}

void MeshNode::UpdateUniforms(bool bSet)
{
  // Update additional Uniforms in Sub-Classes.
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/