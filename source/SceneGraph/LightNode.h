#ifndef _LIGHT_NODE_
#define _LIGHT_NODE_

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "Node.h"

#include <glm.hpp>
#include <GL/glew.h>
/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/
class LightManager;

/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class LightNode : public Node
{
  friend LightManager;

public:
  enum LightType
  {
    DIRECTIONAL = 1,
    SPOTLIGHT = 2,
    POINTLIGHT = 3,
    HEADLIGHT = 4
  };

  virtual ~LightNode();

  glm::vec3 GetLightPosition();
  glm::vec3 GetLightDirection();

  void SetLightIntensity(glm::vec3 v3Intensity);
  glm::vec3 GetLightIntensity();

  void SetLightAttenuation(float fAttenuation);
  float GetLightAttenuation();

  void SetLightAmbientCoefficient(float fAmbientCoefficient);
  float GetLightAmbientCoefficient();

  void SetLightConeAngle(float fAngle);
  float GetLightConeAngle();

  void SetLightType(LightType type);
  LightType GetLightType();

  void BindFramebuffer();
  void BindTexture();

  void SetMatProj(glm::mat4 matProj);
  glm::mat4 GetMatProj();

  void SetMatView(glm::mat4 matView);
  glm::mat4 GetMatView();

private:
  LightNode(); // Only the LightManager can create new Lights!

  glm::mat4 m_matProj;
  glm::mat4 m_matView;

  glm::vec3 m_v3Intensity;
  float m_fAttenuation;
  float m_fAmbientCoefficient;
  float m_fConeAngle;
  LightType m_eLightType;

  GLuint m_Framebuffer;
  GLuint m_stupidTexture;
  GLuint m_depthTexture;
};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
#endif