/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "Navigation.h"
#include "../TC.h"
#include "glfw3.h"

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

NavigationKey::NavigationKey(GLuint iKey)
{
  m_iKey = iKey;
}

NavigationKey::~NavigationKey()
{
  m_iKey = -1;
}

Navigation::Navigation(GLFWwindow* pWindow)
{
  m_pWindow = pWindow;
}

Navigation::~Navigation()
{
  for (NavigationKey* pKey : m_vecKeys)
  {
    delete pKey;
  }

  m_vecKeys.clear();
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

void NavigationKey::ProcessKey(bool bPressed)
{
  if (bPressed)
    Pressed();
  else
    Released();

  m_bPressed = bPressed;
}

void NavigationKey::Pressed()
{
}

void NavigationKey::Released()
{
}

GLuint NavigationKey::GetKey()
{
  return m_iKey;
}

bool NavigationKey::WasPressed()
{
  return m_bPressed;
}


void Navigation::HandleEvents(Timer* pTimer)
{
  for (NavigationKey* pKey : m_vecKeys)
  {
    if (glfwGetKey(m_pWindow, pKey->GetKey()) == GLFW_PRESS)
      pKey->ProcessKey(true);
    else
      pKey->ProcessKey(false);
  }
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/