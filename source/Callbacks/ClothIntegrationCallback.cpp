#include "ClothIntegrationCallback.h"
/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "../SceneGraph/Node.h"
#include "../SceneGraph/TransformNode.h"
#include "../SceneGraph/ClothNode.h"

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/
ClothIntegrationCallback::ClothIntegrationCallback(Node * pGraphRoot)
{
  m_pGraphRoot = pGraphRoot;
}

ClothIntegrationCallback::~ClothIntegrationCallback()
{
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/
void ClothIntegrationCallback::HandleEvents(Timer * pTimer)
{
  m_pTimer = pTimer;
  RecIntegrate(m_pGraphRoot, glm::mat4());
}

void ClothIntegrationCallback::RecIntegrate(Node * pNode, glm::mat4 matTransform)
{
  if (pNode->GetNodeType() == "TransformNode")
  {
    TransformNode* pTransfNode = dynamic_cast<TransformNode*>(pNode);
    matTransform = matTransform * pTransfNode->GetTransformation();
  }
  else if (pNode->GetNodeType() == "DrawableNode")
  {
    ClothNode* pClothNode = dynamic_cast<ClothNode*>(pNode);

    if(pClothNode)
      pClothNode->Integration(m_pTimer ,matTransform);
  }

  for (Node* child : pNode->GetChildren())
  {
    RecIntegrate(child, matTransform);
  }
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
