#ifndef _Player_
#define _Player_

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "glm.hpp"
#include "Map/MapSegment.h"

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/


/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class Player
{
public:
  /*
  * Constructor.
  */
  Player(MapSegment* pStartSegment);

  /*
  * Destructor.
  */
  virtual ~Player();

  /*
  * Returns the Map Segment the Player is positioned at.
  * @return The Map Segment the Player is positioned at.
  */
  MapSegment* GetSegment();

private:
  MapSegment* m_pCurSegment = nullptr;
  Direction m_eDirection = Direction::South;
  glm::vec3 m_v3Position = glm::vec3(0.0f, 0.0f, 0.0f);
};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
#endif