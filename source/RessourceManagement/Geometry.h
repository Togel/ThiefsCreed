#ifndef _GEOMETRY_
#define _GEOMETRY_

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "glm.hpp"
#include <vector>
#include "GL/glew.h"
#include <string>

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/
class Shader;

/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class Geometry
{
public:
  /*
  * Constructor.
  */
  Geometry(std::vector<glm::vec3>& vecVertices, std::vector<glm::vec3>& vecNormals, std::vector<glm::vec2>& vecUvs);

  /*
  * Destructor.
  */
  ~Geometry();

  /*
  * Uploads the Data to the Graphics Card.
  */
  void Upload();

  /*
  * Uploads updated Information to Graphics Card.
  * @param 'pShader' defining Shader Program which should be used.
  */
  void Update(Shader* pShader);

  /*
  * Draw the Geometry.
  * @param 'pShader' defining Shader Program which should be used.
  */
  void Draw(Shader* pShader);

  /*
  * Translate by Value.
  * @param 'v3Trans' Value by which the Geometry should get translated.
  */
  void Translate(glm::vec3 v3Trans);

  /*
  * Translate to Position.
  * @param 'v3Trans' Value to which the Geometry should get translated.
  */
  void SetTranslation(glm::vec3 v3Trans);

  /*
  * Returns the current Translation of the Geometry.
  * @return The current Translation of the Geometry.
  */
  glm::vec3 GetTranslation();

  /*
  * Rotate by Value.
  * @param 'v3Rot' Value by which the Geometry should get rotated.
  */
  void Rotate(glm::vec3 v3Rot);

  /*
  * Rotate to Angles.
  * @param 'v3Rot' Angle Values to which the Geometry should get rotated.
  */
  void SetRotation(glm::vec3 v3Rot);

  /*
  * Returns the current Rotation of the Geometry.
  * @return The current Rotation of the Geometry.
  */
  glm::vec3 GetRotation();

  /*
  * Load a new Texture for the Geometry.
  * @param 'sTexture' specifies the Path to the Texture.
  */
  void SetTexture(std::string sTexture);

private:
  glm::vec3 m_v3Translation = glm::vec3(0.0f, 0.0f, 0.0f);
  glm::vec3 m_v3Rotation    = glm::vec3(0.0f, 0.0f, 0.0f);

  std::vector<glm::vec3> m_vecVertices;
  std::vector<glm::vec3> m_vecNormals;
  std::vector<glm::vec2> m_vecUVs;

  GLuint m_iVertexBuffer = 0;
  GLuint m_iNormalBuffer = 0;
  GLuint m_iUVBuffer = 0;
  GLuint m_iTexture = 0;

  GLsizei m_iSize = 0;
};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
#endif
