/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "SkyBoxNode.h"

#include "../Defines.h"
#include "../Timer.h"
#include "../RessourceManagement/Shader.h"

#include <glm.hpp>
#include "gtc/matrix_transform.hpp"

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/


SkyBoxNode::SkyBoxNode(Shader* pShader, Mesh* pMesh, Texture* pTexture, Texture* pNormalMap, float fAlpha /*= 1.0f*/,
                       DrawableMesh::DrawType pDrawType)
  : MeshNode(pShader, pMesh, pTexture, pNormalMap, fAlpha, pDrawType)
{
  m_timer = new Timer();
  m_timer->Update();
}


/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

void SkyBoxNode::UpdateUniforms(bool bSet)
{
  
  if (bSet)
  {
    float ellapsedTime = m_timer->GetElapsedTime_withoutUpdate();
    
    // calculate current rotatin matrix
    glm::vec3 rotAxis(1.0f, 0.0f, 0.5f);
    rotAxis = glm::normalize(rotAxis);
    
    float angle = ellapsedTime * 360.0f / SKYBOX_MILLISECS_PER_ROUND; // one round per minute 
    
    glm::mat4 matRot = glm::mat4();
    matRot = glm::rotate(matRot, angle, rotAxis);
    
    // update uniform
    GLuint iRotMatrix = m_pShader->GetUniformLocation("matRotation");
    m_pShader->SetUniform(iRotMatrix, &matRot[0][0]);
  }

}


/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/