#ifndef _THREAD_DRAW_
#define _THREAD_DRAW_

#ifdef _WIN32
  #include "windows.h"
#endif

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include <vector>
#include "glfw3.h"
#include "glm.hpp"

#include "Defines.h"
#include <chrono>

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/
class Callback;
class GameManager;
class Node;
class GBuffer;
class DeferredShading;
class MeshNode;
class ResourceCollection;

/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class ThreadDraw
{
public:
  /*
  * Constructor.
  */
  ThreadDraw(GameManager* pGameManager);
  
  /*
  * Destructor.
  */
  ~ThreadDraw();
  
  /*
  * Initializes the Draw Thread.
  */
  void Init();

  /*
  * Executes all Geometry-Callbacks.
  */
  void Run();

  /*
  * Initializes all needed for the DeferredShading
  */
  void SetupDeferredShading();

  /*
  * Deferred Shading Geometry Pass.
  */
  void DSGeometryPass();

  /*
  * Deferred Shading Light Pass.
  */
  void DSLightPass();

  /*
  * Returns a Vector storing all Pre-Draw Callbacks.
  * @return A Vector storing all Pre-Draw Callbacks.
  */
  std::vector<Callback*>& GetPreDrawCallbacks();

  /*
  * Returns a Vector storing all Post-Draw Callbacks.
  * @return A Vector storing all Post-Draw Callbacks.
  */
  std::vector<Callback*>& GetPostDrawCallbacks();

  /*
  * Adds an additional Pre-Draw-Callback.
  * @param pCallback Callback which should be added.
  */
  void AddPreDrawCallback(Callback* pCallback);

  /*
  * Adds an additional Post-Draw-Callback.
  * @param pCallback Callback which should be added.
  */
  void AddPostDrawCallback(Callback* pCallback);

  /*
  * Deletes a specific Callback.
  * @param pCallback Callback which should be removed.
  * @return 'true' iff the Callback could be removed.
  */
  bool DeleteCallback(Callback* pCallback);

  /*
  * Set the GLFW Window Hints.
  * @param iVersion Defines GLFW_CONTEXT_VERSION_MINOR and GLFW_CONTEXT_VERSION_MAJOR
  */
  void SetGLFWHints(unsigned int iVersion);

  /*
  * Creates a new GLFW Window.
  */
  bool CreateGLFWWindow();

  /*
   * States if the window should be closed.
   * @return True iff the window should be closed.
   */
  bool WindowShouldClose();

  /*
   * Initializes the End Screen.
   * @param pResourceCollection The Resource Collection.
   * @param bWon States if the player has won.
   */
  void InitEndScreen(ResourceCollection* pResourceCollection, bool bWon);

  /*
   * Draws the end screen
   */
  void DrawEndScreen();

private:
  /*
  * Recursive traversal of the scene Graph that calls draw on all nodes
  * @param 'pNode' current node of the graph
  * @param 'matTransformation' currently collected transformation
  */
  void recDraw(Node * pNode, glm::mat4 matTransformation);

  GameManager* m_pGameManager = nullptr;
  GBuffer* m_pGBuffer = nullptr;
  DeferredShading* m_pDeferredShading = nullptr;

  std::vector<Callback*> m_vecPreDrawCallbacks;
  std::vector<Callback*> m_vecPostDrawCallbacks;

  GLFWwindow* m_pWindow = nullptr;
  glm::uvec2 m_v2WindowSize = glm::uvec2(WINDOW_WIDTH, WINDOW_HEIGHT);

  std::chrono::high_resolution_clock::time_point m_tLastTime;
  unsigned int m_iFrames = 0;

  MeshNode* m_pEndScreenNode;
};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
#endif