#ifndef _TEXTURE_
#define _TEXTURE_

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "GL/glew.h"
#include <string>

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/
class Shader;

/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class Texture
{
public:
  /*
  * Constructor.
  */
  Texture(GLuint iTexID, std::string sPath, GLenum pTextureType = GL_TEXTURE_2D);

  /*
  * Destructor.
  */
  ~Texture();

  /*
  * Returns the ID of the Texture.
  * @return The ID of the Texture.
  */
  GLuint GetID();

  /*
  * Returns the Path of the Texture.
  * @return The Path of the Texture.
  */
  std::string GetPath();

  /*
  * Binds the Texture using a specific Shader.
  * @param 'pShader' defines a specific Shader.
  * @param 'iName' defines the Name in the Shader ("Texture" + "iName").
  */
  void Bind(GLenum textureUnit);

  /*
  * Unbinds the Texture.
  */
  void Unbind();

private:
  GLuint m_iTexID = 0;
  std::string m_sPath = "";
  GLenum m_eTextureType;
};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
#endif