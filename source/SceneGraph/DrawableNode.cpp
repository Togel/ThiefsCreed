#include "DrawableNode.h"
/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/


/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/
DrawableNode::DrawableNode(Shader* pShader)
  : m_bTransparent(false)
{
  m_pShader = pShader;
}

DrawableNode::~DrawableNode()
{
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/
std::string DrawableNode::GetNodeType()
{
  return "DrawableNode";
}

bool DrawableNode::GetIsTransparent()
{
  return m_bTransparent;
}

void DrawableNode::SetIsTransparent(bool bTransparent)
{
  m_bTransparent = bTransparent;
}

void DrawableNode::DrawOpaque(glm::mat4 matTransformation)
{
  // A empty drawable draws nothing
}

void DrawableNode::DrawTransparent(glm::mat4 matTransformation)
{
  // Empty drawable draws nothing
}

void DrawableNode::DrawWithoutDepthBuffer(glm::mat4 matTransformation)
{
  // Empty drawable draws nothing
}

void DrawableNode::SetShader(Shader * pShader)
{
  m_pShader = pShader;
}

Shader * DrawableNode::GetShader()
{
  return m_pShader;
}


/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
