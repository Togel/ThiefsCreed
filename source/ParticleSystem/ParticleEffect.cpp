#define PARTICLE_MIN_AGE 0
#define PARTICLE_MAX_AGE 0

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "ParticleEffect.h"
#include "ParticleEmitter.h"
#include "Particle.h"
#include "../Timer.h"
#include "../TC.h"
#include "gtc/quaternion.hpp"
#include "../SceneGraph/CameraTransformNode.h"
#include "../RessourceManagement/Texture.h"
#include "../RessourceManagement/Shader.h"

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

ParticleEffect::ParticleEffect(Shader* pShader, TransformNode* pEffectTrans, CameraTransformNode* pTransNode, ParticleEmitter* pEmitter, const unsigned int iNumOfParticles, Texture* pTexture, float fInitSize, glm::vec3 v3Force)
  :m_pShader(pShader)
  ,m_pEffectTransNode(pEffectTrans)
  ,m_pCamTransNode(pTransNode)
  ,m_pParticleEmitter(pEmitter)
  ,m_pTexture(pTexture)
  ,m_fInitSize(fInitSize)
  ,m_v3Force(v3Force)
  ,m_iNumOfParticles(iNumOfParticles)
{
  for (unsigned int iParticle = 0; iParticle < iNumOfParticles; ++iParticle)
  {
    m_vecDeadParticles.emplace_back(new Particle());
  }

  Init();
}

ParticleEffect::~ParticleEffect()
{
  m_fInitSize = 0.0f;
  m_v3Force = glm::vec3(0.0f);

  for (Particle* pParticle : m_vecDeadParticles)
  {
    delete pParticle;
  }
  m_vecDeadParticles.clear();

  for (Particle* pParticle : m_vecAliveParticles)
  {
    delete pParticle;
  }
  m_vecAliveParticles.clear();

  m_vecParticleVertexBuffer.clear();
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

void ParticleEffect::HandleEvents(Timer* pTimer)
{
  float fTime = pTimer->GetElapsedTime();

  // Update Emitter Position.
  m_pParticleEmitter->SetPosition(m_pEffectTransNode->GetTranslation() - glm::vec3(0.0f, 0.28, 0.0f));

  // Age all alive Particles.
  AgeParticles(fTime);

  // Emit a few Particles.
  EmitParticles(fTime);

  // Collect Dead Particles.
  CollectDeadParticles();

  // Prepare Data for Rendering.
  UpdateVertexBuffer();

  // Upload updated Data.
  Upload();

  // Render alive Particles.
  Render();
}


void ParticleEffect::UpdateVertexBuffer()
{
  const glm::vec3 v3X = glm::vec3(0.5f, 0.0f, 0.0f);
  const glm::vec3 v3Y = glm::vec3(0.0f, 0.5f, 0.0f);
  const glm::vec3 v3Z = glm::vec3(0.0f, 0.0f, 1.0f);

  glm::vec3 v3ViewDir = m_pCamTransNode->GetViewDir();

  glm::vec3 v3Right = glm::cross(v3ViewDir, glm::vec3(0.0f, 1.0f, 0.0f));
  v3Right = glm::normalize(v3Right);
  glm::vec3 v3Up = glm::cross(v3Right, v3ViewDir);
  v3Up = glm::normalize(v3Up);

  ResizePVB(static_cast<unsigned int>(m_vecAliveParticles.size()));

  for (unsigned int iParticle = 0; iParticle < m_vecAliveParticles.size(); ++iParticle)
  {
    Particle* pParticle = m_vecAliveParticles[iParticle];

    glm::quat qParticleRotation = glm::angleAxis(pParticle->m_fRotation, v3Z);
    
    // Get the Particle Vertices for the current Particle.
    unsigned int iVertexIndex = iParticle * 6;

    // Triangle 1
    ParticleVertex& pT1TL = m_vecParticleVertexBuffer[iVertexIndex + 0]; // Top-Left
    ParticleVertex& pT1BR = m_vecParticleVertexBuffer[iVertexIndex + 1]; // Bottom-Right
    ParticleVertex& pT1BL = m_vecParticleVertexBuffer[iVertexIndex + 2]; // Bottom-Left
    
    // Triangle 2
    ParticleVertex& pT2TL = m_vecParticleVertexBuffer[iVertexIndex + 3]; // Top-Left
    ParticleVertex& pT2BR = m_vecParticleVertexBuffer[iVertexIndex + 4]; // Bottom-Right
    ParticleVertex& pT2TR = m_vecParticleVertexBuffer[iVertexIndex + 5]; // Top-Right

    // Top-Left
    glm::vec3 v3PosOffset = (qParticleRotation * (-v3X + v3Y) * pParticle->m_fSize);
    pT1TL.m_v3Position = pParticle->m_v3Position + v3PosOffset.x * v3Right + v3PosOffset.y * v3Up;
    pT1TL.m_v2TexCoord = glm::vec2(0.0f, 0.0f);
    pT2TL.m_v3Position = pT1TL.m_v3Position;
    pT2TL.m_v2TexCoord = pT1TL.m_v2TexCoord;

    // Bottom-Right
    v3PosOffset = (qParticleRotation * ( v3X - v3Y) * pParticle->m_fSize);
    pT1BR.m_v3Position = pParticle->m_v3Position + v3PosOffset.x * v3Right + v3PosOffset.y * v3Up;
    pT1BR.m_v2TexCoord = glm::vec2(1.0f, 1.0f);
    pT2BR.m_v3Position = pT1BR.m_v3Position;
    pT2BR.m_v2TexCoord = pT1BR.m_v2TexCoord;

    // Bottom-Left
    v3PosOffset = (qParticleRotation * (-v3X - v3Y) * pParticle->m_fSize);
    pT1BL.m_v3Position = pParticle->m_v3Position + v3PosOffset.x * v3Right + v3PosOffset.y * v3Up;
    pT1BL.m_v2TexCoord = glm::vec2(0.0f, 1.0f);

    // Top-Right
    v3PosOffset = (qParticleRotation * ( v3X + v3Y) * pParticle->m_fSize);
    pT2TR.m_v3Position = pParticle->m_v3Position + v3PosOffset.x * v3Right + v3PosOffset.y * v3Up;
    pT2TR.m_v2TexCoord = glm::vec2(1.0f, 0.0f);

    // Test DELETE ME!!!
    //pT1TL.m_v3Position = glm::vec3(0.0f, 0.0f, -1.0f);
    //pT1BR.m_v3Position = glm::vec3(1.0f, 1.0f, -1.0f);
    //pT1BL.m_v3Position = glm::vec3(0.0f, 1.0f, -1.0f);
    //
    //pT2TL.m_v3Position = glm::vec3(0.0f, 0.0f, -1.0f);
    //pT2BR.m_v3Position = glm::vec3(1.0f, 1.0f, -1.0f);
    //pT2TR.m_v3Position = glm::vec3(1.0f, 0.0f, -1.0f);

    //pT1TL.m_v3Position.z = -1.0f;
    //pT1BR.m_v3Position.z = -1.0f;
    //pT1BL.m_v3Position.z = -1.0f;
    //pT2TL.m_v3Position.z = -1.0f;
    //pT2BR.m_v3Position.z = -1.0f;
    //pT2TR.m_v3Position.z = -1.0f;
  }
}


void ParticleEffect::Render()
{ 
  if (m_pShader != nullptr)
  {
    m_pShader->Bind();

    // Define Drawing Properties.
    glDepthMask(GL_FALSE);
    glEnable(GL_BLEND);
    //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glBlendFunc(GL_ONE, GL_ONE);

    // Bind the Texture.
    m_pTexture->Bind(GL_TEXTURE0);

    // Bind VAO.
    glBindVertexArray(m_iVAO);

    // Draw the Particles
    glDrawArrays(GL_TRIANGLES, 0, static_cast<GLsizei>(m_vecParticleVertexBuffer.size()));
    
    // Unbind Buffer.
    glBindVertexArray(0);

    // Unbind Texture.
    m_pTexture->Unbind();

    glDisable(GL_BLEND);
    glDepthMask(GL_TRUE);

    m_pShader->Release();
  }
}


void ParticleEffect::EmitParticles(float fTime)
{
  fTime /= 1000.0f; // Convert to Seconds.
  float fAvgLifeTime = m_pParticleEmitter->GetAverageLifeTime();

  // Security Definition to prevent all Particles being emitted at once.
  if (fTime == 0.0f)
  {
//    tc::err << "ParticleEffect::EmitParticles -> fTime: " << fTime << tc::endl;
    fTime = fAvgLifeTime;
  }

  // Calculate how many Particles should be emitted.
  unsigned int iDeadParticles = static_cast<unsigned int>(m_vecDeadParticles.size());
  //unsigned int iAvgParticles  = static_cast<int>(fAvgLifeTime / fTime); // Average Amount of Particles which should be emitted so that always about the same Amount is emitted.
  unsigned int iAvgParticles = static_cast<unsigned int>(fAvgLifeTime / fTime);
  unsigned int iNumOfParticles = glm::max( glm::min(iDeadParticles, iAvgParticles), glm::min(static_cast<unsigned int>(1), iDeadParticles) );

  // Emit at most ... Particles per Frame.
  for (unsigned int iParticle = 0; iParticle < iNumOfParticles; ++iParticle)
  {
    Particle* pParticle = m_vecDeadParticles.at(0);

    // Switch the Particle from the Dead Particles to the alive Particles.
    m_vecDeadParticles.erase(m_vecDeadParticles.begin());
    m_vecAliveParticles.push_back(pParticle);

    // Emit the Particle.
    EmitParticle(pParticle);
  }
}


void ParticleEffect::CollectDeadParticles()
{
  // Switch dead Particles to the corresponding Vector.
  unsigned int iAliveParticles = static_cast<unsigned int>(m_vecAliveParticles.size());

  for (int iIndex = iAliveParticles - 1; iIndex >= 0; --iIndex)
  {
    Particle* pParticle = m_vecAliveParticles[iIndex];

    if (pParticle->m_fAge > pParticle->m_fLifeTime)
    {
      m_vecDeadParticles.push_back(pParticle);
      m_vecAliveParticles.erase(m_vecAliveParticles.begin() + iIndex);
    }
  }
}


void ParticleEffect::SetInitalSize(float fSize)
{
  m_fInitSize = fSize;
}


void ParticleEffect::EmitParticle(Particle* pParticle)
{
  assert(m_pParticleEmitter != nullptr);

  pParticle->m_fSize = m_fInitSize;
  m_pParticleEmitter->EmitParticle(pParticle);
}


void ParticleEffect::ResizePVB(unsigned int iParticles)
{
  unsigned int iVerticesInBuffer = static_cast<unsigned int>(m_vecParticleVertexBuffer.size());
  int iDifference = 6*iParticles - iVerticesInBuffer;

  // Add a few Particle Vertices.
  if (iDifference >= 0)
  {
    for (int iNewParticle = 0; iNewParticle < iDifference; ++iNewParticle)
    {
      m_vecParticleVertexBuffer.push_back(ParticleVertex());
    }
  }
  // Delete a few Particle Vertices.
  else
  {
    for (int iDeleteCount = 0; iDeleteCount < (-iDifference); ++iDeleteCount)
    {
      m_vecParticleVertexBuffer.erase(m_vecParticleVertexBuffer.begin());
    }
  }
}


void ParticleEffect::AgeParticles(float fTime)
{
  fTime /= 1000.0f; // Convert to Seconds.

  for (Particle* pParticle : m_vecAliveParticles)
  {
    // Update Age.
    pParticle->m_fAge += fTime;

    // Only update other Attributes iff the Particle is not dead.
    if (pParticle->m_fAge <= pParticle->m_fLifeTime)
    {
      // Update Velocity.
      pParticle->m_v3Veclocity += m_v3Force * fTime;

      // Update Position.
      pParticle->m_v3Position += pParticle->m_v3Veclocity * fTime;

      // Update Rotation?

      // Update Size?
    }
  }
}


void ParticleEffect::Init()
{
  // Create VAO and VBO
  glGenVertexArrays(1, &m_iVAO);
  glBindVertexArray(m_iVAO);

  glGenBuffers(1, &m_iVBO);

  glBindBuffer(GL_ARRAY_BUFFER, m_iVBO);

  // Enable Vertex Attribute Pointer.
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);

  // Enable Normal Attribute Pointer.
  glEnableVertexAttribArray(1);
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*) (3 * sizeof(float)));

  // Enable UV Attribute Pointer.
  glEnableVertexAttribArray(2);
  glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*) (6 * sizeof(float)));

  // Unbind Buffers.
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);  
}


void ParticleEffect::Upload()
{
  // Bind Buffers.
  glBindVertexArray(m_iVAO);
  glBindBuffer(GL_ARRAY_BUFFER, m_iVBO);

  // Upload Data.
  if (m_vecParticleVertexBuffer.size() > 0)
    glBufferData(GL_ARRAY_BUFFER, m_vecParticleVertexBuffer.size()*sizeof(ParticleVertex), &m_vecParticleVertexBuffer[0], GL_DYNAMIC_DRAW);

  // Unbind Buffer.
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0); // Unbind VAO
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/