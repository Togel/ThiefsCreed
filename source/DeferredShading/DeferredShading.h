#ifndef _DEFERRED_SHADING_
#define _DEFFERED_SHADING_

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include <GL/glew.h>
#include <glm.hpp>

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/
class LightManager;
class Shader;
class GBuffer;
class LightNode;
class CameraNode;

/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class DeferredShading
{
public:
  DeferredShading(LightManager* pLightManager, GBuffer* pGBuffer, CameraNode* pCamNode);
  virtual ~DeferredShading();

  void DoShading();

private:
  LightManager* m_pLightManager;
  CameraNode* m_pCamNode;
  GBuffer* m_pGBuffer;

  GLuint m_QuadVAO;
  GLuint m_QuadVBO;

  Shader* m_pLightingShader;

  GLuint m_uniformPosLightPosition;
  GLuint m_uniformPosLightDirection;
  GLuint m_uniformPosLightIntensity;
  GLuint m_uniformPosLightConeAngle;
  GLuint m_uniformPosLightAmbientCoefficient;
  GLuint m_uniformPosLightAttenuation;
  GLuint m_uniformPosLightType;
  GLuint m_uniformPosMatView;
  GLuint m_uniformPosMatProj;

  void Init(GLuint CamBufferID);

  void UploadLightToShader(glm::mat4 ViewMat, LightNode* pLight);

};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
#endif