#ifndef _CIRCLE_PARTICLE_EMITTER_
#define _CIRCLE_PARTICLE_EMITTER_

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "ParticleEmitter.h"
#include "glm.hpp"

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/


/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/

class CircleParticleEmitter : public ParticleEmitter
{
public:
  /*
  * Constructor.
  */
  CircleParticleEmitter(float fRadius, float fAngle);

  /*
  * Destructor.
  */
  virtual ~CircleParticleEmitter();

  /*
  * Defines the Radius in which the Particles are emitted.
  */
  void SetRadius(float fRadius);

  /*
  * Defines the maximum Angle in which the Particles are emitted.
  */
  void SetAngle(float fAngle);

  /*
  * Emits a Particle in the Scene.
  * @param 'pParticle' Pointer on the Particle which should be emitted.
  */
  virtual void EmitParticle(Particle* pParticle);

protected:
  /*
  * Returns a random Position inside the Circle.
  * @return A random Position inside the Circle.
  */
  glm::vec3 GetRandomPos();

  /*
  * Returns a random Angle in which the Particle is emitted.
  * @return A random Angle in which the Particle is emitted.
  */
  float GetRandomAngle();

  /*
  * Returns a random Velocity Vector for the particle.
  * @return A random Velocity Vector for the particle.
  */
  glm::vec3 GetRandomVelocity();

private:
  float m_fRadius = 0.0f;
  float m_fAngle = 0.0f;
};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
#endif