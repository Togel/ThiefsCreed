#include "GBuffer.h"
/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "../TC.h"

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/
GBuffer::GBuffer()
{
}

GBuffer::~GBuffer()
{
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/
bool GBuffer::Init(unsigned int WindowWidth, unsigned int WindowHeight)
{
  m_uiWindowWidth = WindowWidth;
  m_uiWindowHeight = WindowHeight;

  glGenFramebuffers(1, &m_fbo);
  glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);

  glGenTextures(GBUFFER_NUM_TEXTURES, m_textures);

  // bind textures to framebuffer
  for (unsigned int i = 0; i < GBUFFER_NUM_TEXTURES; i++)
  {
    glBindTexture(GL_TEXTURE_2D, m_textures[i]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, WindowWidth, WindowHeight, 0, GL_RGB, GL_FLOAT, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, m_textures[i], 0);
  }

  glGenRenderbuffers(1, &m_depthStencilRenderbuffer);
  glBindRenderbuffer(GL_RENDERBUFFER, m_depthStencilRenderbuffer);
  glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, WindowWidth, WindowHeight);
  glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_depthStencilRenderbuffer);

  GLenum DrawBuffers[GBUFFER_NUM_TEXTURES];
  for (unsigned int i = 0; i < GBUFFER_NUM_TEXTURES; i++)
  {
    DrawBuffers[i] = GL_COLOR_ATTACHMENT0 + i;
  }
 
  glDrawBuffers(GBUFFER_NUM_TEXTURES, DrawBuffers);

  GLenum Status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
  if (Status != GL_FRAMEBUFFER_COMPLETE)
  {
    tc::crs << "Framebuffer error! status: " << Status << tc::endl;
    return false;
  }

  glBindFramebuffer(GL_FRAMEBUFFER, 0);

  return true;


}

void GBuffer::BindForWriting()
{
  glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
}

void GBuffer::CopyFromStandardBuffer()
{
  glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
  glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_fbo);
  glBlitFramebuffer(0, 0, m_uiWindowWidth, m_uiWindowHeight, 0, 0, m_uiWindowWidth, m_uiWindowHeight, GL_COLOR_BUFFER_BIT, GL_NEAREST);
}

void GBuffer::CopyDepthToStandardBuffer()
{
  glBindFramebuffer(GL_READ_FRAMEBUFFER, m_fbo);
  glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
  glBlitFramebuffer(0, 0, m_uiWindowWidth, m_uiWindowHeight, 0, 0, m_uiWindowWidth, m_uiWindowHeight, GL_DEPTH_BUFFER_BIT, GL_NEAREST);

  GLenum status = glGetError();
  if (status != 0)
  {
    tc::err << "Copy depth to standard Buffer : " << status << tc::endl;
  }
}

void GBuffer::ClearAllBuffers()
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void GBuffer::BindForReading()
{
  for (unsigned int i = 0; i < GBUFFER_NUM_TEXTURES; i++)
  {
    glActiveTexture(GL_TEXTURE0 + i);
    glBindTexture(GL_TEXTURE_2D, m_textures[i]);
  }
}

void GBuffer::SetReadBuffer(GBUFFER_TEXTURE_TYPE TextureType)
{
  glBindFramebuffer(GL_READ_FRAMEBUFFER, m_fbo);
  glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
  glReadBuffer(GL_COLOR_ATTACHMENT0 + TextureType);
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/


