set( RelativeDir "source/SceneGraph" )
set( RelativeSourceGroup "SourceFiles\\SceneGraph" )

set(Directories
  
)

set(DirFiles
  CameraNode.cpp
  CameraNode.h
  ClothNode.cpp
  ClothNode.h
  CameraTransformNode.cpp
  CameraTransformNode.h
  DrawableNode.cpp
  DrawableNode.h
  LightNode.cpp
  LightNode.h
  MeshNode.h
  MeshNode.cpp
  Node.cpp
  Node.h
  PlayerNode.cpp
  PlayerNode.h
  RotatingMeshNode.cpp
  RotatingMeshNode.h
  SkyBoxNode.cpp
  SkyBoxNode.h
  TransformNode.cpp
  TransformNode.h
  TranslatingMeshNode.cpp
  TranslatingMeshNode.h
  _SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${RelativeSourceGroup} FILES ${LocalSourceGroupFiles} )

foreach( subdir ${Directories})
	include("${RelativeDir}/${subdir}/_SourceFiles.cmake")
endforeach()

set( RelativeDir "source")