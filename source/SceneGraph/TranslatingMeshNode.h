#ifndef _TRANSLATING_MESH_NODE_
#define _TRANSLATING_MESH_NODE_

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MeshNode.h"

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/


/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/

class TranslatingMeshNode : public MeshNode
{
public:
  /*
  * Constructor.
  */
  TranslatingMeshNode(glm::vec3 v3Start, glm::vec3 v3End, float fSpeed, Shader* pShader, Mesh* pMesh, Texture* pTexture, Texture* pNormalMap = nullptr, float fAlpha = 1.0f);
  TranslatingMeshNode(glm::vec3 v3Start, glm::vec3 v3End, float fSpeed, Shader* pShader, std::vector<DrawableMesh> vecMeshes);

  /*
  * Destructor.
  */
  virtual ~TranslatingMeshNode();

  /*
  * Update the Rotation of the Node.
  * @param Timer to determine time between frames
  */
  void Update(Timer* pTimer) override;

private:
  glm::vec3 m_v3Start  = glm::vec3(0.0f, 0.0f, 0.0f); // Start Point.
  glm::vec3 m_v3End    = glm::vec3(0.0f, 0.0f, 0.0f); // End Point.
  glm::vec3 m_v3Dir    = glm::vec3(0.0f, 0.0f, 0.0f); // Animation Direction.
  float m_fSpeed       = 0.0f; // Animation Speed.
  float m_fDistance    = 0.0f; // Distance between Start and End Point.
  float m_fTraveled    = 0.0f; // Distance traveled between Start and End Point.
};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
#endif