#ifndef _SCENE_GRAPH_
#define _SCENE_GRAPH_

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include <vector>
#include "glm.hpp"
#include "GL/glew.h"
#include "glfw3.h"

#include "AI.h"

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/
class Geometry;
class Map;
class MapSegment;
class PlayerNode;
class Shader;
class Node;
class CameraNode;
class CameraTransformNode;
class ResourceCollection;
class CollisionHandling;
class LightManager;
class Skybox;
class ParticleEffect;

/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class GameManager
{
public:
  /*
  * Constructor.
  */
  GameManager();

  /*
  * Destructor.
  */
  ~GameManager();

  /*
  * Initializes the Game Manager.
  */
  void Init();

  /*
  * Returns a Pointer to the Resource Collection.
  * @return A Pointer to the Resource Collection.
  */
  ResourceCollection* GetResourceCollection();

  /*
  * Generates a new Map.
  * @param 'iMapSize' defines the Number of Map Segments.
  */
  void GenerateMap(int iMapSize);

  /*
  * Inserts a fixed amount of AIs in the Map.
  * @param 'iNumOfAI' defines the amount of AIs.
  */
  void InsertAI(unsigned int iNumOfAI);

  /*
  * Adds a Particle Effect for each AI.
  */
  void AddAIParticleEffects();

  /*
  * Returns a Vector storing the Particle Effects.
  * @return A Vector storing the Particle Effects.
  */
  std::vector<ParticleEffect*> GetParticleEffects();

  /*
   * Adds the Player, as a Node, to the SceneGraph.
   */
  void AddPlayer();

  /*
   * Adds a SkyBox.
   */
  void AddSkyBox();

  /*
   * Initializes the Collision Handling object,
   * by passing all necessary references.
   */
  void InitCollisionHandler(CollisionHandling* p_colHandler);

  /*
  * Returns a Pointer to the default Shader.
  * @return A Pointer to the default Shader.
  */
  Shader* GetDefaultShader();

  void LoadTestScene();

  /*
  * Returns a Pointer to the SG root node
  * @return A Pointer to the root node
  */
  Node* GetSceneGraphRoot();

  /*
  * Returns a pointer to the camera node
  * @return a pointer to the cam node
  */
  CameraNode * GetCameraNode();

  /*
  * Returns a pointer to the light manager
  * @return a pointer to the light manager
  */
  LightManager* GetLightManager();

  /*
  * Returns a pointer to the Player node
  * @return a pointer to the Player node
  */
  PlayerNode * GetPlayerNode();

  /*
   * Sets the GLFW Window.
   */
  void SetGlfwWindow(GLFWwindow* p_pWindow);

  /*
  * Returns a Pointer to the Camera Transform Node.
  * @return A Pointer to the Camera Transform Node.
  */
  CameraTransformNode* GetCameraTransformNode();

  /*
  * Returns a Vector storing the AIs.
  * @return A Vector storing the AIs.
  */
  std::vector<AI*> GetAIs();

  /*
  * Function ends the Game iff the Player has won or lost.
  * @return True if the End Screen should be displayed.
  */
  bool WonOrLost();

  /*
   * Returns if the player has won or lost.
   * @return True if the player has won, false otherwise.
   */
  bool PlayerHasWon();

private:
  Map* m_pMap = nullptr;
  PlayerNode* m_pPlayer = nullptr;
  std::vector<AI*> m_vecAIs;
  std::vector<ParticleEffect*> m_vecParticleEffects;
  Skybox* m_pSkybox;

  Shader* m_pDefaultShader = nullptr;

  Node* m_pSceneGraphRoot = nullptr;
  CameraNode* m_pCameraNode = nullptr;
  CameraTransformNode* m_pCamTransformNode = nullptr;

  ResourceCollection* m_pResourceCollection = nullptr;
  LightManager* m_pLightManager = nullptr;

  GLFWwindow* m_pWindow = nullptr;

  bool m_bEndOfGame = false;
  bool m_bWon = false;
};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
#endif
