#ifndef _GBUFFER_
#define _GBUFFER_

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include <GL/glew.h>

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/


/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class GBuffer
{
public:
  /*
  * The possible types of textures within the GBuffer
  *
  */
  enum GBUFFER_TEXTURE_TYPE
  {
    GBUFFER_TEXTURE_TYPE_POSITION,
    GBUFFER_TEXTURE_TYPE_DIFFUSE,
    GBUFFER_TEXTURE_TYPE_NORMAL,
    GBUFFER_TEXTURE_TYPE_TEXCOORD,
    GBUFFER_TEXTURE_TYPE_POSNDC,
    GBUFFER_NUM_TEXTURES
  };

  /*
  * Constructor
  */
  GBuffer();
  virtual ~GBuffer();

  /*
  * Initializes the GBuffer
  * @param WindowWidth the width of the render target window in pixel
  * @param WindowHeight the height of the render target window in pixel
  * @return 'true' iff success
  */
  bool Init(unsigned int WindowWidth, unsigned int WindowHeight);

  /*
   * Binds the GBuffer so that it can be written
   */
  void BindForWriting();

  /*
  * Binds the GBuffer so that it can be read
  */
  void BindForReading();

  void CopyFromStandardBuffer();

  void CopyDepthToStandardBuffer();

  void ClearAllBuffers();

  void SetReadBuffer(GBUFFER_TEXTURE_TYPE TextureType);

private:

  GLuint m_fbo;
  GLuint m_textures[GBUFFER_NUM_TEXTURES];
  //GLuint m_textures;
  GLuint m_depthTexture;
  GLuint m_depthStencilRenderbuffer;

  unsigned int m_uiWindowWidth = 0;
  unsigned int m_uiWindowHeight = 0;

};


/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
#endif
