#ifndef _NO_DEPTH_DRAW_Callback_
#define _NO_DEPTH_DRAW_Callback_

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "Callback.h"

#include <glm.hpp>

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/
class Node;

/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class NoDepthBufferDrawCallback : public Callback
{
public:
  NoDepthBufferDrawCallback(Node* pRoot);

  virtual ~NoDepthBufferDrawCallback();

  virtual void HandleEvents(Timer* pTimer);

private:

  void recDraw(Node* pNode, glm::mat4 matTransformation);

  Node* m_pRoot;

};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
#endif