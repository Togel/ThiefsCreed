set( RelativeDir "source/Callbacks" )
set( RelativeSourceGroup "SourceFiles\\Callbacks" )

set(Directories
  
)

set(DirFiles
  Callback.h
  Callback.cpp
  CameraBufferCallback.cpp
  CameraBufferCallback.h
  ClothIntegrationCallback.cpp
  ClothIntegrationCallback.h
  Navigation.h
  Navigation.cpp
  NoDepthBufferDrawCallback.cpp
  NoDepthBufferDrawCallback.h
  ShadowMappingCallback.cpp
  ShadowMappingCallback.h
  UpdateTraversalCallback.cpp
  UpdateTraversalCallback.h  
  _SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${RelativeSourceGroup} FILES ${LocalSourceGroupFiles} )

foreach( subdir ${Directories})
	include("${RelativeDir}/${subdir}/_SourceFiles.cmake")
endforeach()

set( RelativeDir "source")