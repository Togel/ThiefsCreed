#version 420 core

uniform float materialShininess = 16.0;
uniform vec3 materialSpecularColor = vec3(1.0, 1.0, 1.0);

layout(binding = 5) uniform sampler2D texture_ShadowMap;

struct LightParams {
  vec3 lightPosition;
  vec3 lightDirection;
  vec3 lightIntensity;
  float lightConeAngle;
  float lightAmbientCoefficient;
  float lightAttenuation;
  float lightType;
};

uniform mat4 matLightView;
uniform mat4 matLightProj;

vec3 ComputeLight(LightParams light, vec3 surfaceColor, vec3 normal, vec3 surfacePosCam, vec3 surfacePosWorld, vec3 surfaceToCamera)
{
  
  mat4 biasMatrix = mat4(0.5, 0.0, 0.0, 0.0,
    0.0, 0.5, 0.0, 0.0,
    0.0, 0.0, 0.5, 0.0,
    0.5, 0.5, 0.5, 1.0);

  vec4 shadowCoord = biasMatrix * matLightProj * matLightView * vec4(surfacePosWorld, 1.0);
  shadowCoord.x = shadowCoord.x / shadowCoord.w;
  shadowCoord.y = shadowCoord.y / shadowCoord.w;
  shadowCoord.z = shadowCoord.z / shadowCoord.w;

  float visibility = 1.0;
  float bias = 0.005;// * tan(acos(dot(normal, -1.0 * light.lightDirection)));
  //bias = clamp(bias, 0.0, 0.01);
  float textureDepth = texture2D(texture_ShadowMap, shadowCoord.xy).x;

  if (textureDepth < (shadowCoord.z - bias) )
  {
    visibility = 0.0;
  }

  if (shadowCoord.x > 1.0 || shadowCoord.x < -1.0 // Outside light clipping space, so we assume no shadow
    || shadowCoord.y > 1.0 || shadowCoord.y < -1.0)
  {
    visibility = 1.0;
  }
  
	vec3 surfaceToLight;
	float lightToSurfaceAngle;
	float distanceToLight;
	float attenuation = 1.0;
	if(light.lightType == 1.0) // Directional
	{
		surfaceToLight = normalize(light.lightDirection);
		attenuation = 1.0;
	}
	else if(light.lightType == 2.0) // Spotlight
	{
		surfaceToLight = normalize(light.lightPosition - surfacePosCam);
		float distanceToLight = length(light.lightPosition - surfacePosCam);
		attenuation = 1.0 / (1.0 + light.lightAttenuation * pow(distanceToLight, 2));

		float lightToSurfaceAngle = degrees(acos(dot(-surfaceToLight, normalize(light.lightDirection))));
		if(lightToSurfaceAngle > light.lightConeAngle)
		{
			attenuation = 0.0;
		}
	}
	else if(light.lightType == 3.0) // PointLight
	{
		surfaceToLight = normalize(light.lightPosition - surfacePosCam);
		float distanceToLight = length(light.lightPosition - surfacePosCam);
		attenuation = 1.0 / (1.0 + light.lightAttenuation * pow(distanceToLight, 2));
	}
	else if(light.lightType == 4.0) // HeadLight
	{
		surfaceToLight = surfaceToCamera;
		distanceToLight = length(surfacePosCam);
		attenuation = 1.0 / (1.0 + light.lightAttenuation * pow(distanceToLight, 2));

		float angle = acos(dot(-surfaceToCamera, vec3(0.0, 0.0, -1.0)));

    visibility = 1.0;

		lightToSurfaceAngle = degrees(angle);
		if(lightToSurfaceAngle > (light.lightConeAngle * 0.8))
		{
			attenuation = mix(0.0, attenuation, max(0.0, (light.lightConeAngle - lightToSurfaceAngle) / (light.lightConeAngle * 0.2) ));
		}
		
		//attenuation = attenuation * abs(sin(angle));

	}


  if (distanceToLight > 99.0)
  {
    attenuation = 0.0;
  }
	// ambient
	vec3 ambient = light.lightAmbientCoefficient * surfaceColor * light.lightIntensity;

	// diffuse
	float diffuseCoefficient = max(0.0, dot(normal, surfaceToLight));
	vec3 diffuse = diffuseCoefficient * surfaceColor * light.lightIntensity;

	// specular
	float specularCoefficient = 0.0;
	if(diffuseCoefficient > 0.0)
	{
		specularCoefficient = pow(max(0.0, dot(surfaceToCamera, reflect(-surfaceToLight, normal))), materialShininess);
	}
	vec3 specular = specularCoefficient * materialSpecularColor * light.lightIntensity;

	return ambient + visibility * attenuation * (diffuse + specular);
  //return ambient + attenuation * (diffuse + specular);

	//return vec3(attenuation);
}

