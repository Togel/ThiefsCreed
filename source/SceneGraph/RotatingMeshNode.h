#ifndef _ROTATING_MESH_NODE_
#define _ROTATING_MESH_NODE_

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MeshNode.h"

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/


/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/

class RotatingMeshNode : public MeshNode
{
public:
  /*
  * Constructor.
  * @param 'v3Rotation' defines the Rotation Speed of the x-,y- and z-Coordinate in Degree per Second.
  */
  RotatingMeshNode(glm::vec3 v3Rotation, float fMaxAngle, float fAnimationSpeed, Shader* pShader, Mesh* pMesh, Texture* pTexture, Texture* pNormalMap = nullptr, float fAlpha = 1.0f);
  RotatingMeshNode(glm::vec3 v3Rotation, float fMaxAngle, float fAnimationSpeed, Shader* pShader, std::vector<DrawableMesh> vecMeshes);

  /*
  * Destructor.
  */
  virtual ~RotatingMeshNode();

  /*
	* Update the Rotation of the Node.
	* @param Timer to determine time between frames
	*/
	void Update(Timer* pTimer) override;

private:
  glm::vec3 m_v3RotAxis  = glm::vec3(0.0f);
  glm::vec3 m_v3CompVec  = glm::vec3(0.0f, 1.0f, 0.0f); // The Rotation is applied not only to the Transform Node but to this Vector, too.
  
  float m_fCurAngle = 0.0f; // Current Rotation Angle.
  float m_fMaxAngle = 360.0f; // Only rotate up to this angle value.
  float m_fAniSpeed = 0.0f;   // Animation Speed.

  float m_fDirection = 1.0f; // 1.0 if rotating forward, -1.0 if rotating backwards.
};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
#endif