/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "Geometry.h"
#include "Shader.h"
#include "../TC.h"

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

Geometry::Geometry(std::vector<glm::vec3>& vecVertices, std::vector<glm::vec3>& vecNormals, std::vector<glm::vec2>& vecUvs)
{
  // Store Vertex, Normal and UV Information.
  for (glm::vec3 v3Vertex : vecVertices)
  {
    m_vecVertices.push_back(v3Vertex);
  }

  for (glm::vec3 v3Normal : vecNormals)
  {
    m_vecNormals.push_back(v3Normal);
  }

  for (glm::vec2 v2UV : vecUvs)
  {
    m_vecUVs.push_back(v2UV);
  }

  // Generate Buffers.
  glGenBuffers(1, &m_iVertexBuffer);
  glGenBuffers(1, &m_iNormalBuffer);
  glGenBuffers(1, &m_iUVBuffer);
  
  m_iSize = static_cast<GLsizei>(vecVertices.size());
}

Geometry::~Geometry()
{
  // Tidy up.
  m_v3Translation = glm::vec3(0.0f, 0.0f, 0.0f);
  m_v3Rotation    = glm::vec3(0.0f, 0.0f, 0.0f);

  m_vecVertices.clear();
  m_vecNormals.clear();
  m_vecUVs.clear();

  m_iVertexBuffer = 0;
  m_iNormalBuffer = 0;
  m_iUVBuffer     = 0;
  m_iTexture      = 0;

  m_iSize = 0;
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

void Geometry::Upload()
{
  if (m_iTexture == 0)
    tc::war << "Geometry::Upload -> Trying to Upload Geometry without Texture." << tc::endl;

  // Bind Vertex Buffer.
  glBindBuffer(GL_ARRAY_BUFFER, m_iVertexBuffer);
  glBufferData(GL_ARRAY_BUFFER, m_vecVertices.size()*sizeof(glm::vec3), &m_vecVertices[0], GL_STATIC_DRAW);

  // Bind Normal Buffer.
  glBindBuffer(GL_ARRAY_BUFFER, m_iNormalBuffer);
  glBufferData(GL_ARRAY_BUFFER, m_vecNormals.size()*sizeof(glm::vec3), &m_vecNormals[0], GL_STATIC_DRAW);

  // Bind UV Buffer.
  glBindBuffer(GL_ARRAY_BUFFER, m_iUVBuffer);
  glBufferData(GL_ARRAY_BUFFER, m_vecUVs.size()*sizeof(glm::vec2), &m_vecUVs[0], GL_STATIC_DRAW);

  // Unbind Buffer.
  glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Geometry::Update(Shader* pShader)
{
  // Update Translation and Rotation Uniforms.
  GLuint iTrans = pShader->GetUniformLocation("v3Trans");
  GLuint iRot   = pShader->GetUniformLocation("v3Rot");

  pShader->SetUniform(iTrans, m_v3Translation.x, m_v3Translation.y, m_v3Translation.z);
  pShader->SetUniform(iRot, m_v3Rotation.x, m_v3Rotation.y, m_v3Rotation.z);
}

void Geometry::Draw(Shader* pShader)
{
  // Bind Texture.
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, m_iTexture);
  GLuint iTexID = pShader->GetUniformLocation("sTexture");
  pShader->SetUniform(iTexID, 0);

  // Set Vertex Attribute Pointer.
  glEnableVertexAttribArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, m_iVertexBuffer);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

  // Set Normal Attribute Pointer.
  glEnableVertexAttribArray(1);
  glBindBuffer(GL_ARRAY_BUFFER, m_iNormalBuffer);
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

  // Set UV Attribute Pointer.
  glEnableVertexAttribArray(2);
  glBindBuffer(GL_ARRAY_BUFFER, m_iUVBuffer);
  glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);

  // Draw Triangles.
  glDrawArrays(GL_TRIANGLES, 0, m_iSize);

  // Disable Attribute Pointers.
  glDisableVertexAttribArray(0);
  glDisableVertexAttribArray(1);
  glDisableVertexAttribArray(2);
}

void Geometry::Translate(glm::vec3 v3Trans)
{
  m_v3Translation += v3Trans;
}

void Geometry::SetTranslation(glm::vec3 v3Trans)
{
  m_v3Translation = v3Trans;
}

glm::vec3 Geometry::GetTranslation()
{
  return m_v3Translation;
}

void Geometry::Rotate(glm::vec3 v3Rot)
{
  m_v3Rotation += v3Rot;
}

void Geometry::SetRotation(glm::vec3 v3Rot)
{
  m_v3Rotation = v3Rot;
}

glm::vec3 Geometry::GetRotation()
{
  return m_v3Rotation;
}

void Geometry::SetTexture(std::string sTexture)
{
  tc::war << "Geometry::SetTexture -> Missing Code" << tc::endl;
  // Modify m_iTexture!!!
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/