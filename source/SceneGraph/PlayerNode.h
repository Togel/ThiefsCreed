#ifndef _PLAYER_NODE_
#define _PLAYER_NODE_

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "Node.h"

#include "glm.hpp"

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/
class Map;
class MapSegment;
class MeshNode;

/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/

class PlayerNode : public Node 
{
public:
  PlayerNode(Map* p_map);

  virtual ~PlayerNode();

  /*
  * Returns the Map Segment in which the Player stands.
  * @return The Map Segment in which the Player stands.
  */
  MapSegment* GetCurrentMapSegment();

  /*
  * Returns the MeshNode of the Map Segment in which the Player stands.
  * @return The MeshNode of the Map Segment in which the Player stands.
  */
  MeshNode* GetCurrentMapMeshNode();

  /*
   * Translates the player as well as the camera.
   */
  void Translate(glm::vec3 p_translation);

  /*
   * Returns the player's position.
   * @return The player's position.
   */
  glm::vec3 getPlayerPosition();

  /*
   * Returns the player's position in 2D.
   * @return The player's position in 2D.
   */
  glm::vec2 getPlayerPosition_2D();

  /*
  * Gets called by the update threads
  * Overrides the virtual method of Node.
  * @param Timer to determine time between frames
  */
  virtual void Update(Timer* pTime) override;

private:
  Map* m_pMap = nullptr;
  MapSegment* m_pCurSegment = nullptr;

  glm::vec3 m_playerPos;
  glm::vec2 m_playerPos_2D;


  /*
   * Calculates the map indices from the player's
   * current position.
   */
  void getMapIndices(int& xInd, int& yInd);

};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
#endif