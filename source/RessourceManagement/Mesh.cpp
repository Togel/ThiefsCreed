/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "Mesh.h"
#include "../TC.h"

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

Mesh::Mesh()
{}

Mesh::Mesh(std::vector<glm::vec3> vecVertices, std::vector<glm::vec3> vecNormals, std::vector<glm::vec2> vecUVs, Texture* pTexture, Texture* pNormalMap)
{
  m_pTexture = pTexture;
  m_pNormalMap = pNormalMap;

  m_vecVertices = vecVertices;
  m_vecNormals = vecNormals;
  m_vecUVs = vecUVs;

  init();
}

Mesh::Mesh(std::vector<glm::vec3> vecVertices, std::vector<glm::vec3> vecNormals, std::vector<glm::vec2> vecUVs, std::vector<unsigned int> vecFaces_indices,
  Texture* pTexture, Texture* pNormalMap)
{
  m_pTexture = pTexture;
  m_pNormalMap = pNormalMap;

  m_vecVertices = vecVertices;
  m_vecNormals = vecNormals;
  m_vecUVs = vecUVs;

  m_vecFaces_indices = vecFaces_indices;

  init();
}

Mesh::~Mesh()
{
  m_vecFaces_indices.clear();
  m_vecVertices.clear();
  m_vecNormals.clear();
  m_vecUVs.clear();

  glDeleteBuffers(1, &m_iVerBuff);
  glDeleteBuffers(1, &m_iNorBuff);
  glDeleteBuffers(1, &m_iUVBuff);
  glDeleteBuffers(1, &m_iEAB);
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

void Mesh::Rotate(glm::vec3 v3Rot)
{
  // Create the Rotation Matrix.
  float fSinX = glm::sin(v3Rot.x);
  float fSinY = glm::sin(v3Rot.y);
  float fSinZ = glm::sin(v3Rot.z);
  float fCosX = glm::cos(v3Rot.x);
  float fCosY = glm::cos(v3Rot.y);
  float fCosZ = glm::cos(v3Rot.z);

  glm::mat3x3 m3Rotation = glm::mat3x3(
    fCosY*fCosZ, fCosZ*fSinX*fSinY - fCosX*fSinZ, fCosX*fCosZ*fSinY + fSinX*fSinZ,
    fCosY*fSinZ, fCosX*fCosZ + fSinX*fSinY*fSinZ, -fCosZ*fSinX + fCosX*fSinY*fSinZ,
    -fSinY, fCosY*fSinX, fCosX*fCosY);

  // Apply the Matrix to all Vertices and Normals.
  for (unsigned int i = 0; i < m_vecVertices.size(); ++i)
  {
    m_vecVertices.at(i) = m3Rotation * m_vecVertices.at(i);
    m_vecNormals.at(i) = m3Rotation * m_vecNormals.at(i);
  }
}


void Mesh::init()
{
  // use indexing if the index array is filled
  if (m_vecFaces_indices.size() > 0) m_useIndexBuffer = true;
  else m_useIndexBuffer = false;

  // number of elements to be drawn
  if (m_useIndexBuffer)
  {
    m_iSize = static_cast<GLsizei>(m_vecFaces_indices.size());
  }
  else{
    m_iSize = static_cast<GLsizei>(m_vecVertices.size());
  }

  // Create VAO
  glGenVertexArrays(1, &m_iVAO);
  glBindVertexArray(m_iVAO);

  glGenBuffers(1, &m_iVerBuff);
  glGenBuffers(1, &m_iNorBuff);
  glGenBuffers(1, &m_iUVBuff);
  glGenBuffers(1, &m_iEAB);
  glGenBuffers(1, &m_iTangBuff);
  glGenBuffers(1, &m_iBitangBuff);

  // Upload Data
  Upload();

  // Enable Vertex Attribute Pointer.
  if (m_vecVertices.size() > 0)
  {
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, m_iVerBuff);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
  }

  // Enable Normal Attribute Pointer.
  if (m_vecNormals.size() > 0)
  {
    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, m_iNorBuff);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
  }

  // Enable UV Attribute Pointer.
  if (m_vecUVs.size() > 0)
  {
    glEnableVertexAttribArray(2);
    glBindBuffer(GL_ARRAY_BUFFER, m_iUVBuff);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);
  }

  // Unbind VAO
  glBindVertexArray(0);
}

void Mesh::Upload()
{
  // Bind Vertex Buffer.
  if (m_vecVertices.size() > 0)
  {
    glBindBuffer(GL_ARRAY_BUFFER, m_iVerBuff);
    glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3)*m_vecVertices.size(), &m_vecVertices[0], GL_STATIC_DRAW);
  }

  // Bind Normal Buffer.
  if (m_vecNormals.size() > 0)
  {
    glBindBuffer(GL_ARRAY_BUFFER, m_iNorBuff);
    glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3)*m_vecNormals.size(), &m_vecNormals[0], GL_STATIC_DRAW);
  }

  // Bind UV Buffer.
  if (m_vecUVs.size() > 0)
  {
    glBindBuffer(GL_ARRAY_BUFFER, m_iUVBuff);
    glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2)*m_vecUVs.size(), &m_vecUVs[0], GL_STATIC_DRAW);
  }

  // Bind Element Array Buffer
  if (m_vecFaces_indices.size() > 0)
  {
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_iEAB);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int)*m_vecFaces_indices.size() , &m_vecFaces_indices[0], GL_STATIC_DRAW);
  }

  // Unbind Buffer.
  glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Mesh::Draw()
{
  glBindVertexArray(m_iVAO); // Bind VAO

  if (!m_useIndexBuffer)
  {
    glDrawArrays(GL_TRIANGLES, 0, m_iSize);
  }
  else {
    glDrawElements(GL_TRIANGLES, m_iSize, GL_UNSIGNED_INT, 0);
  }

  glBindVertexArray(0); // Unbind VAO
}

void Mesh::ComputeTangentBasis()
{
  if (m_vecFaces_indices.size() == 0 ||
      m_vecVertices.size() == 0 ||
      m_vecVertices.size() != m_vecUVs.size())
  {
    tc::err << "Mesh: Cannot compute Tangent Basis, some arrays are not appropriate." << tc::endl;
    return;
  }

  m_vecTangents.clear();
  m_vecBitangents.clear();
  m_vecTangents.resize(m_vecVertices.size(), glm::vec3(0.0, 0.0, 0.0));
  m_vecBitangents.resize(m_vecVertices.size(), glm::vec3(0.0, 0.0, 0.0));


  // compute tangents for each face
  for (size_t i = 0; i < m_vecFaces_indices.size(); i+= 3)
  {
    // vertices
    glm::vec3 vertex_1 = m_vecVertices[ m_vecFaces_indices[i] ];
    glm::vec3 vertex_2 = m_vecVertices[ m_vecFaces_indices[i+1] ];
    glm::vec3 vertex_3 = m_vecVertices[ m_vecFaces_indices[i+2] ];

    // UVs
    glm::vec2 uv_1 = m_vecUVs[ m_vecFaces_indices[i] ];
    glm::vec2 uv_2 = m_vecUVs[ m_vecFaces_indices[i+1] ];
    glm::vec2 uv_3 = m_vecUVs[ m_vecFaces_indices[i+2] ];


    // Edges of the triangle : postion delta
    glm::vec3 deltaPos1 = vertex_2-vertex_1;
    glm::vec3 deltaPos2 = vertex_3-vertex_1;

    // UV delta
    glm::vec2 deltaUV1 = uv_2-uv_1;
    glm::vec2 deltaUV2 = uv_3-uv_1;


    // formula for tangent and bitangent computation from
    // http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-13-normal-mapping/
    float r = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x);
    glm::vec3 tangent = (deltaPos1 * deltaUV2.y   - deltaPos2 * deltaUV1.y)*r;
    glm::vec3 bitangent = (deltaPos2 * deltaUV1.x   - deltaPos1 * deltaUV2.x)*r;


    // add tangents to the respective vertices
    m_vecTangents[ m_vecFaces_indices[i] ] += tangent;
    m_vecTangents[ m_vecFaces_indices[i+1] ] += tangent;
    m_vecTangents[ m_vecFaces_indices[i+2] ] += tangent;

    m_vecBitangents[ m_vecFaces_indices[i] ] += bitangent;
    m_vecBitangents[ m_vecFaces_indices[i+1] ] += bitangent;
    m_vecBitangents[ m_vecFaces_indices[i+2] ] += bitangent;
  }

  // normalize
  for (size_t i = 0; i < m_vecTangents.size(); ++i)
  {
    glm::vec3 tangent = m_vecTangents[i];
    glm::vec3 bitangent = m_vecBitangents[i];

    // make tangents orthogonal to normal
    if (i < m_vecNormals.size())
    {
      glm::vec3 normal = m_vecNormals[i];

      glm::vec3 cross = glm::cross(tangent, normal);
      tangent = glm::cross(normal, cross);

      cross = glm::cross(bitangent, normal);
      bitangent = glm::cross(normal, cross);
    }

    m_vecTangents[i] = glm::normalize(tangent);
    m_vecBitangents[i] = glm::normalize(bitangent);
  }


  // OpenGL Buffers
  glBindVertexArray(m_iVAO);

  // upload and enable attributes
  glBindBuffer(GL_ARRAY_BUFFER, m_iTangBuff);
  glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3)*m_vecTangents.size(), &m_vecTangents[0], GL_STATIC_DRAW);
  glEnableVertexAttribArray(3);
  glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

  glBindBuffer(GL_ARRAY_BUFFER, m_iBitangBuff);
  glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3)*m_vecBitangents.size(), &m_vecBitangents[0], GL_STATIC_DRAW);
  glEnableVertexAttribArray(4);
  glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

  // Unbind Buffer.
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  // Unbind VAO
  glBindVertexArray(0);
}


std::vector<glm::vec3> Mesh::GetVertices()
{
  return m_vecVertices;
}

std::vector<glm::vec3> Mesh::GetNormals()
{
  return m_vecNormals;
}

std::vector<glm::vec2> Mesh::GetUVs()
{
  return m_vecUVs;
}

std::vector<unsigned int> Mesh::getFaces_indices()
{
  return m_vecFaces_indices;
}

Texture* Mesh::GetTexture()
{
  return m_pTexture;
}

Texture* Mesh::GetNormalMap()
{
  return m_pNormalMap;
}

GLuint& Mesh::GetVertexBuffer()
{
  return m_iVerBuff;
}

GLuint& Mesh::GetNormalBuffer()
{
  return m_iNorBuff;
}

GLuint& Mesh::GetUVBuffer()
{
  return m_iUVBuff;
}

GLuint& Mesh::GetElementArrayBuffer()
{
  return m_iEAB;
}


void Mesh::setVertices(std::vector<glm::vec3> vecVertices)
{
  m_vecVertices = vecVertices;
}

void Mesh::setNormals(std::vector<glm::vec3> vecNormals)
{
  m_vecNormals = vecNormals;
}

void Mesh::setUVs(std::vector<glm::vec2> vecUVs)
{
  m_vecUVs = vecUVs;
}

void Mesh::setFaces_indices(std::vector<unsigned int> vecFaces_indices)
{
  m_vecFaces_indices = vecFaces_indices;
}

void Mesh::setTexture(Texture* pTexture)
{
  m_pTexture = pTexture;
}

void Mesh::setNormalMap(Texture* pNormalMap)
{
  m_pNormalMap = pNormalMap;
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/