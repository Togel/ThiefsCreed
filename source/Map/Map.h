#ifndef _TC_MAP_
#define _TC_MAP_

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include <vector>
#include "glm.hpp"

#include "../Defines.h"

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/
class MapSegment;
class Node;
class Mesh;
class MeshNode;
class ResourceCollection;
class TransformNode;
class Shader;

/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class Map
{
public:
  /*
  * Constructor.
  */
  Map(Node* pRoot, ResourceCollection* pResourceCollection, int iMapSize);

  /*
  * Destructor.
  */
  virtual ~Map();

  /*
  * Returns Vectors storing a Vector of the Control Points for each AI.
  * @param 'iNumOfAI' defines the Number of AIs.
  * @return Vectors storing a Vector of the Control Points for each AI.
  */
  std::vector<std::vector<MapSegment*>> GetAISegments(int iNumOfAI);

  /*
   * Returns the MapSegment at position iX, iY.
   * @return The MapSegment at position iX, iY.
   */
  MapSegment* getMapSegment(int iX, int iY);

  /*
   * Returns the MeshNode at position iX, iY.
   * @return The MeshNode at position iX, iY.
   */
  MeshNode* getMeshNode(int iX, int iY);

  /*
   * Returns the initial position of the Player.
   * @return The initial position of the Player.
   */
  glm::vec3 getInitialPlayerPosition();

  /*
   * Returns the initial orientation of the Player in degrees.
   * @return The initial orientation of the Player in degrees.
   */
  float getInitialPlayerOrientation();

private:
  MapSegment* m_pStartSegment = nullptr; // Reference to the starting Point. (Defines whole map)

  std::vector<MapSegment*> m_vecSegments; // Vector storing all Map Segments.

  MapSegment* m_aMap[MapWidth][MapHeight]; // Array storing whole Map.
  MeshNode* m_aMap_MeshNodes[MapWidth][MapHeight]; // Array storing all MeshNodes of the Map.

  Node* m_pMapNode = nullptr; // To this Node all TransformNodes + MeshNodes get rooted.
  ResourceCollection* m_pResCol = nullptr; // Collection storing all Meshes and Textures.

  /*
  / Generates a random Map.
  */
  bool GenerateRandomMap(int iMapSize);

  /*
  / Generates a random Number between 0 and 99.
  */
  int GetRandomPosition(int iMaxValue);

  /*
  * Checks whether a MapSegment can be placed at Position (iX, iY).
  * @param 'iX' X-Position.
  * @param 'iY' Y-Position.
  * @return true iff the MapSegment can be placed.
  */
  bool IsValidPosition(int iX, int iY);

  /*
  * Connects neighboring MapSegments.
  */
  void ConnectSegments();

  /*
  * Prints the Map to the Console.
  */
  void PrintMap();

  /*
  * Construct the Map Geometry and places it in the SceneGraph.
  */
  void PlaceInSceneGraph();

  /*
  * Inserts a Mesh Node which is connected to a given Transform Node.
  * The Type of the MapSegment is detected automatically and a random Texture is selected.
  * @param 'iX' X-Position.
  * @param 'iY' Y-Position.
  * @param 'pTrans' TransformNode.
  * @param 'pShader' Shader.
  */
  void InsertMeshNodeAtPos(int iX, int iY, TransformNode* pTrans, Shader* pShader);
};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
#endif