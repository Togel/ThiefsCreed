#ifndef _ClothNode_
#define _ClothNode_

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "DrawableNode.h"

#include <GL/glew.h>

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/
class Timer;
class Mesh;
class Shader;
class Texture;

/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class ClothNode : public DrawableNode
{
public:
  ClothNode(Shader* pShader, Shader* pIntegrationShader, Texture* pTexture);
  virtual ~ClothNode();

  

  void Update(Timer* pTimer) override;
 
  void Integration(Timer* pTimer, glm::mat4 matTransformation);

  void DrawOpaque(glm::mat4 matTransformation) override;

  glm::vec3 GetPosition();

  void SetSimulationEnabled(bool bValue);

  void SetToPlayer(glm::vec3 v3ToPlayer);
private:
  void Init(Texture* pTexture = nullptr);

  enum BUFFER_TYPE
  {
    POS_A,
    POS_B,
    VEL_A,
    VEL_B,
    CON,
    CON2,
    NORMALS,
    UV
  };

  Texture* m_pTexture;

  Shader* m_pIntegrationShader;

  int IndexCount;
  int m_iNumTriangles;

  GLuint m_vao[2];
  GLuint m_vbo[8];
  GLuint m_index_buffer;
  GLuint m_pos_tbo[2];
  GLuint m_C_loc;
  GLuint m_iteration_index;

  bool m_bSimulationEnabled;

  glm::vec3 m_v3ToPlayer;




};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
#endif
