#version 420 core

uniform samplerCube cubeMap;
//uniform sampler2D texture;

//in vec4 vertexNormal;
in vec2 textureCoords;
in vec3 vertexPos_modelspace;

out vec4 color;

void main(){

  vec3 texel = texture(cubeMap, vertexPos_modelspace).rgb;

  color = vec4(texel, 1.0);

}