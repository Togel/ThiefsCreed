/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "Map.h"
#include "MapSegment.h"

#include "../RessourceManagement/Mesh.h"
#include "../RessourceManagement/ResourceCollection.h"

#include "../SceneGraph/TransformNode.h"
#include "../SceneGraph/MeshNode.h"
#include "../SceneGraph/ClothNode.h"

#include "../TC.h"

#include <stdlib.h>
#include <chrono>

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

Map::Map(Node* pRoot, ResourceCollection* pResourceCollection, int iMapSize)
{
  // Init Random Number Generator.
  std::chrono::milliseconds ms = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch());
  unsigned int iTime = static_cast<unsigned int>(ms.count());
  srand(iTime);

  // Clear the Map.
  for (int iRow = 0; iRow < MapHeight; ++iRow)
  {
    for (int iCol = 0; iCol < MapWidth; ++iCol)
    {
      m_aMap[iCol][iRow] = nullptr;
    }
  }

  for (int iRow = 0; iRow < MapHeight; ++iRow)
  {
    for (int iCol = 0; iCol < MapWidth; ++iCol)
    {
      m_aMap_MeshNodes[iCol][iRow] = nullptr;
    }
  }

  // Create Map Node.
  m_pMapNode = new Node();

  m_pResCol = pResourceCollection;

  // Generate the Map.
  if (!GenerateRandomMap(iMapSize))
    tc::err << "Map::Map -> Could not generate a Map." << tc::endl;

  pRoot->AddChild(m_pMapNode);
}

Map::~Map()
{
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

std::vector<std::vector<MapSegment*>> Map::GetAISegments(int iNumOfAI)
{
  std::vector <std::vector<MapSegment*>> vecResult;

  // Filter m_vecSegments (no dead-ends)
  std::vector<MapSegment*> vecNotDeadEnd;

  for (MapSegment* pSegment : m_vecSegments)
  {
    // Count Neighbors
    if (pSegment->GetNeighbors().size() > 1)
      vecNotDeadEnd.push_back(pSegment);
  }

  // Divide into 'iNumOfAI' parts
  int iSegmentsPerAI = static_cast<int>(vecNotDeadEnd.size() / iNumOfAI);

  for (int iAI = 0; iAI < iNumOfAI; ++iAI)
  {
    std::vector<MapSegment*> vecRoute;

    for (int iSegID = iAI*iSegmentsPerAI; iSegID < (iAI + 1)*iSegmentsPerAI; ++iSegID)
    {
      if (static_cast<unsigned int>(iSegID) < vecNotDeadEnd.size())
        vecRoute.push_back(vecNotDeadEnd.at(iSegID));
    }

    if (vecRoute.size() == 0)
      tc::err << "Map::GetAISegments -> AI has a Route consisting of 0 Segments." << tc::endl;

    vecResult.push_back(vecRoute);
  }

  return vecResult;
}


MapSegment* Map::getMapSegment(int iX, int iY)
{
  return m_aMap[iX][iY];
}


MeshNode* Map::getMeshNode(int iX, int iY)
{
    return m_aMap_MeshNodes[iX][iY];
}


glm::vec3 Map::getInitialPlayerPosition()
{
  // no start segment defined
  if (m_pStartSegment == nullptr)
    return glm::vec3(0.0, 0.0, 0.0);

  glm::vec2 segPos = m_pStartSegment->GetPosition();
  return glm::vec3(segPos[0], MAP_OFFSET, segPos[1]);
}


float Map::getInitialPlayerOrientation()
{
  // no start segment defined
  if (m_pStartSegment == nullptr)
    return 0.0f;

  if (m_pStartSegment->GetNeighbor(Direction::North) != nullptr)
  {
    return 0.0f;
  }
  if (m_pStartSegment->GetNeighbor(Direction::East) != nullptr)
  {
    return 270.0f;
  }
  if (m_pStartSegment->GetNeighbor(Direction::West) != nullptr)
  {
    return 90.0f;
  }
  if (m_pStartSegment->GetNeighbor(Direction::South) != nullptr)
  {
    return 180.0f;
  }

  // should not get here
  return 0.0f;
}


bool Map::GenerateRandomMap(int iMapSize)
{
  // We need to store the Number of already placed Map Segments as well as the current randomized Position.
  int iCurNumOfSegments = 0;
  int iPosX = 0;
  int iPosY = 0;

  // Check whether 'iMapSize' Segments can be created.
  if (iMapSize > MapWidth*MapHeight || iMapSize == 0)
  {
    tc::err << "Could not create a new Map. Chosen Number of Segments is either too large or equals 0" << tc::endl;
    return false;
  }

  // Determine a random starting Point.
  iPosX = GetRandomPosition(MapWidth);
  iPosY = GetRandomPosition(MapHeight);


  // Create a Root Map Segment.
  int iID = 0;
  glm::vec2 v2Pos = glm::vec2(iPosX*SEGMENT_WIDTH, iPosY*SEGMENT_WIDTH);
  m_pStartSegment      = new MapSegment(iID, v2Pos);
  m_aMap[iPosX][iPosY] = m_pStartSegment;
  iCurNumOfSegments++;
  iID++;

  // As long as there are Segments missing insert further ones
  while (iCurNumOfSegments < iMapSize)
  {
    iPosX = GetRandomPosition(MapWidth);
    iPosY = GetRandomPosition(MapHeight);

    // Check whether a new Segment can be placed at the Position (iPosX, iPosY).
    // A Segment can be placed if no other Segment is placed there yet and it would have at least one neighboring Segment.
    if ( IsValidPosition(iPosX, iPosY) )
    {
      glm::vec2 v2Pos = glm::vec2(iPosX*SEGMENT_WIDTH, iPosY*SEGMENT_WIDTH);
      m_aMap[iPosX][iPosY] = new MapSegment(iID, v2Pos);
      iID++;
      iCurNumOfSegments++;
    }
  }

  // Connect the Segments.
  ConnectSegments();

  // Randomly choose one of the Segments as start Segment.
  while (true){
    int iRanPos = GetRandomPosition(iMapSize);
    MapSegment* pStart = m_vecSegments.at(iRanPos);

    if (pStart->GetNeighbors().size() <= 1){
      m_pStartSegment = pStart;
      break;
    }
  }

  // Randomly choose one of the Segments as final Segment.
  while (true){
    int iRanPos = GetRandomPosition(iMapSize);
    MapSegment* pFinal = m_vecSegments.at(iRanPos);

    if (pFinal != m_pStartSegment && pFinal->GetNeighbors().size() <= 1){
      pFinal->SetFinal(true);
      break;
    }
  }

  // Place Map in SceneGraph.
  PlaceInSceneGraph();

  // Output the created Map
  #ifdef _DEBUG
  PrintMap();
  #endif

  tc::inf << "Map created successfully!" << tc::endl;
  return true;
}


int Map::GetRandomPosition(int iMaxValue)
{
  return rand() % iMaxValue;
}


bool Map::IsValidPosition(int iX, int iY)
{
  // Check whether a new Segment can be placed at the Position (iPosX, iPosY).
  // A Segment can be placed if no other Segment is placed there yet and it would have at least one neighboring Segment.
  if (m_aMap[iX][iY] == nullptr)
  {
    // Count the number of adjacent Segments.
    int iAdjacentSegments = 0;

    if (iX > 0 && m_aMap[iX - 1][iY] != nullptr)
      iAdjacentSegments++;

    if (iX < MapWidth - 1 && m_aMap[iX + 1][iY] != nullptr)
      iAdjacentSegments++;

    if (iY > 0 && m_aMap[iX][iY - 1] != nullptr)
      iAdjacentSegments++;

    if (iY < MapHeight - 1 && m_aMap[iX][iY + 1] != nullptr)
      iAdjacentSegments++;

    if (iAdjacentSegments > 0)
    {
      // We additionally prefer MapSegments with less Neighbors.
      if (iAdjacentSegments == 1)
      {
        return true;
      }
      // The more Neighbors a new Segment would have the lower the possibility gets that we use that Segment.
      else
      {
        int iDiscard = static_cast<int>(100 / iAdjacentSegments);
        iDiscard     = GetRandomPosition(iDiscard);

        if (iDiscard > 35)
          return true;
      }
    }
  }

  return false;
}


void Map::ConnectSegments()
{
  for (int iRow = 0; iRow < MapHeight; ++iRow)
  {
    for (int iCol = 0; iCol < MapWidth; ++iCol)
    {
      MapSegment* pCurSegment = m_aMap[iCol][iRow];

      // Connect with neighboring Segments.
      if (pCurSegment != nullptr)
      {
        // Store Element.
        m_vecSegments.push_back(pCurSegment);

        // Connect West Segment if it exists.
        if (iCol > 0)
          pCurSegment->SetNeighbor(Direction::West, m_aMap[iCol - 1][iRow]);

        // Connect East Segment if it exists.
        if (iCol < MapWidth - 1)
          pCurSegment->SetNeighbor(Direction::East, m_aMap[iCol + 1][iRow]);

        // Connect North Segment if it exists.
        if (iRow > 0)
          pCurSegment->SetNeighbor(Direction::North, m_aMap[iCol][iRow - 1]);

        // Connect North Segment if it exists.
        if (iRow < MapHeight - 1)
          pCurSegment->SetNeighbor(Direction::South, m_aMap[iCol][iRow + 1]);

      }
    }
  }
}


void Map::PrintMap()
{
  tc::inf << "Map:" << tc::endl;

  for (int iRow = 0; iRow < MapHeight; ++iRow)
  {
    for (int iCol = 0; iCol < MapWidth; ++iCol)
    {
      if (m_aMap[iCol][iRow] != nullptr)
      {
        if (m_aMap[iCol][iRow]->IsFinal())
        {
          tc::inf << "F";
        }
        else if (m_aMap[iCol][iRow] == m_pStartSegment)
        {
          tc::inf << "S";
        }
        else
        {
          tc::inf << "+";
        }
      }
      else {
        tc::inf << " ";
      }
    }

    tc::inf << tc::endl;
  }
}


void Map::PlaceInSceneGraph()
{
  // Structure in SceneGraph -> R_N: Root Node, N: (Map) Node, T: TransformNode, M_N: MeshNode, R_M_N: RotatingMeshNode
  //              R_N
  //               |
  //        _______N_______
  //       /       |       \
  //    . . .      T      . . .
  //               |     
  //    . . .     M_N     . . .

  // Error Handling
  if (m_pResCol == nullptr)
  {
    tc::err << "Map::PlaceInSceneGraph -> Resource Collection is Null-Pointer!" << tc::endl;
    return;
  }

  // Load Shader.
  Shader* pShader = m_pResCol->GetShader("DeferredGeometryShader");

  for (int iRow = 0; iRow < MapHeight; ++iRow)
  {
    for (int iCol = 0; iCol < MapWidth; ++iCol)
    {
      if (m_aMap[iCol][iRow] != nullptr)
      {
        glm::vec3 v3Trans = glm::vec3(iCol*SEGMENT_WIDTH, MAP_OFFSET, iRow*SEGMENT_WIDTH);

        // Create Translation Node.
        TransformNode* pTransNode = new TransformNode();
        pTransNode->Translate(v3Trans);

        // Create Mesh Node.
        InsertMeshNodeAtPos(iCol, iRow, pTransNode, pShader);

        // Connect Node.
        m_pMapNode->AddChild(pTransNode);
      }
    }
  }
}


void Map::InsertMeshNodeAtPos(int iX, int iY, TransformNode* pTrans, Shader* pShader)
{
  // Error Handling.
  if (m_pResCol == nullptr)
  {
    tc::err << "Map::InsertMeshNodeAtPos -> Resource Collection is Null-Pointer!" << tc::endl;
    return;
  }

  bool bInsertCurtain = false;

  // Store Mesh, Texture and Rotation Information.
  Mesh* pMesh = nullptr;
  Mesh* pOutside = nullptr;
  Texture* pTexture = nullptr;
  Texture* pNormalMap = nullptr;

  // Store the Name of the Segment in the String Stream.
  std::stringstream sMesh;
  std::stringstream sTexture;
  sTexture << "MapSegment";

  // North Neighbor?
  if (iY > 0 && m_aMap[iX][iY - 1] != nullptr)
    sMesh << "N";

  // South Neighbor?
  if (iY < MapHeight - 1 && m_aMap[iX][iY + 1] != nullptr)
    sMesh << "S";

  // West Neighbor?
  if (iX > 0 && m_aMap[iX - 1][iY] != nullptr)
    sMesh << "W";

  // East Neighbor?
  if (iX < MapWidth - 1 && m_aMap[iX + 1][iY] != nullptr)
    sMesh << "E";


  // Load Mesh and Texture.
  // 1 neighboring Segment.
  if (sMesh.str() == "N" || sMesh.str() == "S" || sMesh.str() == "W" || sMesh.str() == "E")
  {
    pMesh = m_pResCol->GetMesh("MapSegmentW");
    pOutside = m_pResCol->GetMesh("MapSegmentWOut");
    sTexture << "W" << GetRandomPosition(SEGMENT_TEX_COUNT) + 1;
    bInsertCurtain = true;
  }
  // 2 neighboring Segments (straight).
  else if (sMesh.str() == "NS" || sMesh.str() == "WE")
  {
    pMesh = m_pResCol->GetMesh("MapSegmentWE");
    pOutside = m_pResCol->GetMesh("MapSegmentWEOut");
    sTexture << "WE" << GetRandomPosition(SEGMENT_TEX_COUNT) + 1;
  }
  // 2 neighboring Segments (corner).
  else if (sMesh.str() == "NW" || sMesh.str() == "NE" || sMesh.str() == "SW" || sMesh.str() == "SE")
  {
    pMesh = m_pResCol->GetMesh("MapSegmentNW");
    pOutside = m_pResCol->GetMesh("MapSegmentNWOut");
    sTexture << "NW" << GetRandomPosition(SEGMENT_TEX_COUNT) + 1;
  }
  // 3 neighboring Segments.
  else if (sMesh.str() == "NSW" || sMesh.str() == "NSE" || sMesh.str() == "NWE" || sMesh.str() == "SWE")
  {
    pMesh = m_pResCol->GetMesh("MapSegmentNSW");
    pOutside = m_pResCol->GetMesh("MapSegmentNSWOut");
    sTexture << "NSW" << GetRandomPosition(SEGMENT_TEX_COUNT) + 1;
  }
  // 4 neighboring Segments.
  else if (sMesh.str() == "NSWE")
  {
    pMesh = m_pResCol->GetMesh("MapSegmentNSWE");
    pOutside = m_pResCol->GetMesh("MapSegmentNSWEOut");
    sTexture << "NSWE" << GetRandomPosition(SEGMENT_TEX_COUNT) + 1;
  }
  else
  {
    tc::err << "Map::InsertMeshNodeAtPos -> Mesh MapSegment" << sMesh.str() << " not found." << tc::endl;
    return;
  }
  
  // Get Texture and Normal Map.
  pTexture = m_pResCol->GetTexture(sTexture.str());
  sTexture << "NormalMap";
  pNormalMap = m_pResCol->GetTexture(sTexture.str());

  // Get Rotation for the Mesh.
  float fRotAngle = 0.0f;

  if (sMesh.str() == "S" || sMesh.str() == "NS" || sMesh.str() == "SW" || sMesh.str() == "SWE")
    fRotAngle = 90.0f;
  else if (sMesh.str() == "E" || sMesh.str() == "SE" || sMesh.str() == "NSE")
    fRotAngle = 180.0f;
  else if (sMesh.str() == "N" || sMesh.str() == "NE" || sMesh.str() == "NWE")
    fRotAngle = -90.0f;

  pTrans->Rotate(fRotAngle, glm::vec3(0.0f, 1.0f, 0.0f));

  // Create Node.
  MeshNode* pMeshNode = new MeshNode(pShader, pMesh, pTexture, pNormalMap);
  MeshNode* pOutsideMeshNode = new MeshNode(pShader, pOutside, nullptr, nullptr);

  // Connect Nodes
  pTrans->AddChild(pMeshNode);
  pTrans->AddChild(pOutsideMeshNode);

  // store MeshNode in Map array
  m_aMap_MeshNodes[iX][iY] = pMeshNode;


  // Insert a Curtain if the Segment is an End Segment.
  if (bInsertCurtain)
  {
    int iNum = 1 + rand() % 2;
    std::stringstream sStream;
    sStream << "Curtain" << iNum;

    Shader* pIntegrationShader = m_pResCol->GetShader("ClothIntegrationShader");

    Texture* pCurtainTexture = m_pResCol->GetTexture(sStream.str());

    TransformNode* pCurtainTrans = new TransformNode();
    pCurtainTrans->Translate(glm::vec3(-2.0f, 1.1f, 0.0f));
    pCurtainTrans->Rotate(270.0f, glm::vec3(0.0f, 1.0f, 0.0f));
    pCurtainTrans->Scale(0.06f);
    
    ClothNode* pCurtain = new ClothNode(pShader, pIntegrationShader, pCurtainTexture);

    pCurtainTrans->AddChild(pCurtain);
    pTrans->AddChild(pCurtainTrans);
  }
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/