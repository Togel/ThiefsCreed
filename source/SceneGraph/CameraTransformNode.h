#ifndef CameraTransformNode_h_
#define CameraTransformNode_h_

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "Node.h"
#include "TransformNode.h"
#include "../Timer.h"

#include <glm.hpp>
#include "glfw3.h"


/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/


/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class CameraTransformNode : public TransformNode
{
public:
  CameraTransformNode(GLFWwindow* p_glfwWindow, double p_movementSpeed, double p_mouseSpeed);

  virtual ~CameraTransformNode();

  /*
  * Returns a string used to determine the type of the node
  * To work properly this has to be implemented in all inheriting nodetypes
  * @return string that encodes type of the node
  */
  std::string GetNodeType() override;


  /*
  * Gets called by the update threads
  * Overrides the virtual method of Node.
  * @param Timer to determine time between frames
  */
  virtual void Update(Timer* pTime) override;


  /*
  * Returns the View Direction.
  * @return The viewing Direction.
  */
  glm::vec3 GetViewDir();

  /*
   * Returns the movement speed.
   * @return the movement speed.
   */
  double getMovementSpeed();

  /*
   * Returns the mouse speed.
   * @return the mouse speed.
   */
  double getMouseSpeed();

  /*
   * Sets the movement speed.
   */
  void setMovementSpeed(double p_movementSpeed);

  /*
   * Sets the mouse speed.
   */
  void setMouseSpeed(double p_mouseSpeed);


  /*
   * Returns if we have control over the mouse already,
   * i.e. can do glfwSetCursorPos(...)
   * @return True iff we have control over the mouse.
   */
  bool isMouseControlGained();

  /*
   * Set if we have control over the mouse already,
   * i.e. can do glfwSetCursorPos(...)
   */
  void setMouseControlGained(bool p_mouseControlGained);


private:

  GLFWwindow* m_glfwWindow;

  // sensitivity
  double m_movementSpeed;
  double m_mouseSpeed;

  // need to wait until we have control of the mouse
  bool m_mouseControlGained;

  // cheater
  bool setCanFly;
};


/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
#endif
