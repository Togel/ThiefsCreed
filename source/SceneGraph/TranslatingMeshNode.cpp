/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "TranslatingMeshNode.h"
#include "TransformNode.h"
#include "../Timer.h"

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

TranslatingMeshNode::TranslatingMeshNode(glm::vec3 v3Start, glm::vec3 v3End, float fSpeed, Shader* pShader, Mesh* pMesh, Texture* pTexture, Texture* pNormalMap /*= nullptr*/, float fAlpha /*= 1.0f*/)
  : MeshNode(pShader, pMesh, pTexture, pNormalMap, fAlpha)
{
  
  m_v3Start   = v3Start;
  m_v3End     = v3End;
  m_v3Dir     = glm::normalize(v3End - v3Start);
  m_fSpeed    = fSpeed;
  m_fDistance = glm::length(v3End - v3Start);

  TransformNode* pTransform = dynamic_cast<TransformNode*>(this->GetParent());
  pTransform->SetTranslation(v3Start);
}

TranslatingMeshNode::TranslatingMeshNode(glm::vec3 v3Start, glm::vec3 v3End, float fSpeed, Shader* pShader, std::vector<DrawableMesh> vecMeshes)
  : MeshNode(pShader, vecMeshes)
{
  
  m_v3Start   = v3Start;
  m_v3End     = v3End;
  m_v3Dir     = glm::normalize(v3End - v3Start);
  m_fSpeed    = fSpeed;
  m_fDistance = glm::length(v3End - v3Start);

  TransformNode* pTransform = dynamic_cast<TransformNode*>(this->GetParent());
  pTransform->SetTranslation(v3Start);
}

TranslatingMeshNode::~TranslatingMeshNode()
{
  m_v3Start   = glm::vec3(0.0f, 0.0f, 0.0f);
  m_v3End     = glm::vec3(0.0f, 0.0f, 0.0f);
  m_v3Dir     = glm::vec3(0.0f, 0.0f, 0.0f);
  m_fSpeed    = 0.0f;
  m_fDistance = 0.0f;
  m_fTraveled = 0.0f;
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

void TranslatingMeshNode::Update(Timer* pTimer)
{
  TransformNode* pTransform = dynamic_cast<TransformNode*>(this->GetParent());

  float fElapsedTime = pTimer->GetElapsedTime();

  // s = v*t
  glm::vec3 v3TravelBy = m_v3Dir * fElapsedTime;

  // Calculate Distance traveled.
  m_fTraveled += glm::length(v3TravelBy);

  // Apply Transformation.
  pTransform->Translate(v3TravelBy);

  // If we would overshoot the Target Location we go back to the Starting Position.
  if (m_fTraveled >= m_fDistance)
  {
    pTransform->SetTranslation(m_v3Start);
    m_fTraveled = 0.0f;
  }
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/