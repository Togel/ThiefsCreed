#include "ShadowMappingCallback.h"
/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "../LightManager.h"
#include "../RessourceManagement/Shader.h"
#include "../RessourceManagement/ResourceCollection.h"
#include "../SceneGraph/Node.h"
#include "../SceneGraph/DrawableNode.h"
#include "../SceneGraph/TransformNode.h"
#include "../SceneGraph/CameraNode.h"
#include "../SceneGraph/LightNode.h"
#include "../SceneGraph/ClothNode.h"
#include "../Settings.h"
#include "../Defines.h"
#include "../TC.h"

#include <gtc/matrix_transform.hpp>

#include <cstring>

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/
ShadowMappingCallback::ShadowMappingCallback(LightManager* pLightManager, Node* pSceneGraphNode, ResourceCollection* pResCollection, CameraNode* pCamNode)
  : m_pLightManager(pLightManager)
  , m_pSceneGraphRoot(pSceneGraphNode)
  , m_pResourceCollection(pResCollection)
  , m_pCameraNode(pCamNode)
{

  m_pShader = m_pResourceCollection->GetShader("ShadowMappingShader");

  depthProjectionMatrix = glm::ortho<float>(-10, 10, -10, 10);
}

ShadowMappingCallback::~ShadowMappingCallback()
{

}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/
void ShadowMappingCallback::HandleEvents(Timer * pTimer)
{

  static bool once = false;
  glCullFace(GL_FRONT);
  

  for (LightNode* pLight : m_pLightManager->GetLights())
  {
    pLight->BindFramebuffer();

    if (pLight->GetLightType() != pLight->HEADLIGHT)
    {
      glEnable(GL_DEPTH_TEST);
      glDepthFunc(GL_LESS);
      glDepthMask(GL_TRUE);
      glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
      glViewport(0, 0, 1024, 1024);

      glm::vec3 lightDir = glm::normalize(pLight->GetLightDirection());
      glm::vec3 lightPos = pLight->GetLightPosition();

      glm::vec3 lightRight = glm::cross(lightDir, glm::vec3(0.0, 1.0, 0.0));
      glm::vec3 lightUp = glm::cross(lightRight, lightDir);

      depthViewMatrix = glm::lookAt(lightPos, lightPos + lightDir, lightUp);

      if (pLight->GetLightType() == pLight->HEADLIGHT || pLight->GetLightType() == pLight->SPOTLIGHT)
      {
        depthProjectionMatrix = glm::perspective(pLight->GetLightConeAngle() * 2, 1.0f, 0.1f, 100.0f);
      }

      pLight->SetMatProj(depthProjectionMatrix);
      pLight->SetMatView(depthViewMatrix);

      FillCameraUniformBuffer();

      RecDraw(m_pSceneGraphRoot, glm::mat4());
    }
  }


  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
  m_pCameraNode->FillBuffer();
  glCullFace(GL_BACK);
}

void ShadowMappingCallback::RecDraw(Node * pNode, glm::mat4 transformMat)
{

  if (pNode->GetNodeType() == "DrawableNode")
  {
    DrawableNode* pDrawableNode = dynamic_cast<DrawableNode*>(pNode);

    ClothNode* pClothNode = dynamic_cast<ClothNode*>(pNode);
    if (pClothNode)
    {
      glCullFace(GL_BACK);
    }

    Shader* pShaderStorage = pDrawableNode->GetShader();
    pDrawableNode->SetShader(m_pShader);
    pDrawableNode->DrawOpaque(transformMat);
    pDrawableNode->SetShader(pShaderStorage);

    if (pClothNode)
    {
      glCullFace(GL_FRONT);
    }
  }
  else if (pNode->GetNodeType() == "TransformNode")
  {
    TransformNode* pTransformNode = dynamic_cast<TransformNode*>(pNode);
    transformMat = transformMat * pTransformNode->GetTransformation();
  }

  for (Node* pChild : pNode->GetChildren())
  {
    RecDraw(pChild, transformMat);
  }
}

void ShadowMappingCallback::FillCameraUniformBuffer()
{
  GLuint camBuffer = m_pCameraNode->GetUniformBufferID();

  glm::mat4 inverseTransposedView = glm::transpose(glm::inverse(depthViewMatrix));

  GLfloat buffer[48];

  memcpy(buffer +  0, &depthViewMatrix[0][0]      , 16 * sizeof(float));
  memcpy(buffer + 16, &depthProjectionMatrix[0][0], 16 * sizeof(float));
  memcpy(buffer + 32, &inverseTransposedView[0][0], 16 * sizeof(float));

  glBindBuffer(GL_UNIFORM_BUFFER, camBuffer);
  glBufferData(GL_UNIFORM_BUFFER, 48 * sizeof(float), buffer, GL_DYNAMIC_DRAW);
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/


