#version 420 core

layout(binding =0) uniform sampler2D texture;

in vec2 textureCoords;

out vec4 color;


void main(){

  color = vec4(texture2D(texture, textureCoords).rgb, 1.0);

}
