#ifndef _SETTINGS_
#define _SETTINGS_

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/


/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/

// Controls, Mouse Sensivity
#define CAMERA_MOUSE_SPEED 0.2


// Resolution, Window Size
#define WINDOW_WIDTH 1024
#define WINDOW_HEIGHT 860


// Map
#define NUMBER_OF_MAP_SEGMENTS 50
// Maimum Width and Height of the Map
#define MapWidth 30
#define MapHeight 30


// Difficulty, Number of AIs
#define NUMBER_OF_AI 4


// Cheating
#define SET_CAN_FLY false
#define SET_CAN_MOVE_THROUGH_WALLS false
#define SET_CAN_MOVE_THROUTH_AI false


// Endless Game
#define INFINITE_GAME false

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
#endif