/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "GameManager.h"
#include "Defines.h"
#include "TC.h"
#include "CollisionHandling.h"
#include "LightManager.h"
#include "Skybox.h"
#include "Map/Map.h"
#include "SceneGraph/Node.h"
#include "SceneGraph/MeshNode.h"
#include "SceneGraph/TransformNode.h"
#include "SceneGraph/CameraNode.h"
#include "SceneGraph/CameraTransformNode.h"
#include "SceneGraph/MeshNode.h"
#include "SceneGraph/LightNode.h"
#include "SceneGraph/PlayerNode.h"

#include "SceneGraph/ClothNode.h"
#include "RessourceManagement/Shader.h"
#include "RessourceManagement/GeometryLoader.h"
#include "RessourceManagement/Mesh.h"
#include "RessourceManagement/ModelLoader.h"
#include "RessourceManagement/ResourceCollection.h"
#include "RessourceManagement/Texture.h"
#include "RessourceManagement/TextureLoader.h"
#include "ParticleSystem/CircleParticleEmitter.h"
#include "ParticleSystem/ParticleEffect.h"

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

GameManager::GameManager()
{
	m_pSceneGraphRoot = new TransformNode();  
}

GameManager::~GameManager()
{

}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

void GameManager::Init()
{
  // create camera
  m_pCameraNode = new CameraNode();
  m_pCameraNode->SetView(glm::vec3(0.0f, 0.0f, 1.0f), glm::vec3(0.0f, 0.0f, 1.0f), glm::vec3(0.0f, 1.0f, 0.0f));
  m_pCameraNode->SetProjective(50.0f, (float)WINDOW_WIDTH / (float)WINDOW_HEIGHT, 0.01f, 100.0f);

  // camera input sensitivities
  double cam_mouseSpeed = CAMERA_MOUSE_SPEED;
  double cam_movementSpeed = CAMERA_MOVEMENT_SPEED;

  // create camera transform node
  m_pCamTransformNode = new CameraTransformNode(m_pWindow, cam_movementSpeed, cam_mouseSpeed);
  // initial camera position and orientation
  //pCamTransformNode->Rotate(0.0, glm::vec3(0.0, 1.0, 0.0));
  //pCamTransformNode->Translate(glm::vec3(1.0,1.0,4.0));

  // add camera
  m_pCamTransformNode->AddChild(m_pCameraNode);
  // add to scene graph
  m_pSceneGraphRoot->AddChild(m_pCamTransformNode);

  m_pLightManager = new LightManager();

  
  LightNode* pHeadLight = m_pLightManager->CreateLight();
  pHeadLight->SetLightAmbientCoefficient(0.001f);
  pHeadLight->SetLightAttenuation(1.0f);
  pHeadLight->SetLightConeAngle(15.0f);
  pHeadLight->SetLightIntensity(glm::vec3(0.6, 0.6, 0.3));
  pHeadLight->SetLightType(LightNode::HEADLIGHT);
  m_pCamTransformNode->AddChild(pHeadLight);

  m_pResourceCollection = new ResourceCollection(m_pCameraNode->GetUniformBufferID());
}

ResourceCollection* GameManager::GetResourceCollection()
{
  return m_pResourceCollection;
}

void GameManager::GenerateMap(int iMapSize)
{
  // Reset old Map.
  delete m_pMap;
  m_pMap = nullptr;

  // Generate new Map.
  m_pMap = new Map(m_pSceneGraphRoot, m_pResourceCollection, iMapSize);
}

void GameManager::InsertAI(unsigned int iNumOfAI)
{
  std::vector<std::vector<MapSegment*>> vecRoutes = m_pMap->GetAISegments(iNumOfAI);

  // Error Handling
  if (iNumOfAI > vecRoutes.size())
    tc::err << "GameManager::InsertAI -> Could not insert as many AIs, since the Height of the Map is too small." << tc::endl;

  // Compute Parts of Map which are patrolled by the AIs.
  for (std::vector<MapSegment*> vecControlPoints : vecRoutes)
  {
    // Create AIs
    AI* pAI = new AI(this, vecControlPoints, m_pPlayer, m_pLightManager, m_pMap);
    m_vecAIs.push_back(pAI);
  }
}

void GameManager::AddAIParticleEffects()
{
  Shader* pEffectShader = m_pResourceCollection->GetShader("ParticleShader");

  for (AI* pAI : m_vecAIs)
  {
    TransformNode* pEffectTrans = dynamic_cast<TransformNode*>(pAI);
    CircleParticleEmitter* pEmitter = new CircleParticleEmitter(0.08f, 5.0f);
    pEmitter->SetLifeTime(0.4f, 0.6f);
    Texture* pTexure = m_pResourceCollection->GetTexture("Particle");
    ParticleEffect* pEffect = new ParticleEffect(pEffectShader, pEffectTrans, m_pCamTransformNode, pEmitter, 200, pTexure, 0.2f, glm::vec3(0.0f, -0.001f, 0.0f));
    m_vecParticleEffects.push_back(pEffect);
  }
}

std::vector<ParticleEffect*> GameManager::GetParticleEffects()
{
  return m_vecParticleEffects;
}

void GameManager::AddPlayer()
{
  // initial camera/player orientation
  float orientation = m_pMap->getInitialPlayerOrientation();
  m_pCamTransformNode->Rotate(orientation, glm::vec3(0.0f, 1.0f, 0.0f));

  // initial camera/player position
  glm::vec3 position = m_pMap->getInitialPlayerPosition();
  float playerAboveFloor = PLAYER_ABOVE_FLOOR;
  position[1] += playerAboveFloor;
  m_pCamTransformNode->Translate(position);


  // create player
  m_pPlayer = new PlayerNode(m_pMap);
  m_pCamTransformNode->AddChild(m_pPlayer);
}

void GameManager::AddSkyBox()
{
  if (m_pPlayer != nullptr && m_pResourceCollection != nullptr)
  {
    m_pSkybox = new Skybox(m_pPlayer, m_pResourceCollection);
  }
  else {
    tc::err << "Game Manager: Cannot create Skybox, need to create necessary objects first." << tc::endl;
  }
}

void GameManager::InitCollisionHandler(CollisionHandling* p_colHandler)
{
  p_colHandler->setPlayerNode(m_pPlayer);

  /*
   * Add more parameters to the collision handler, if they are needed.
   */

  p_colHandler->setRootNode(m_pSceneGraphRoot);
}

Shader* GameManager::GetDefaultShader()
{
  return m_pDefaultShader;
}

void GameManager::LoadTestScene()
{

  TransformNode* pTransformNode = new TransformNode();
  pTransformNode->Translate(glm::vec3(0.0, 0.0, -1.0));

  Shader* pShader = new Shader(m_pCameraNode->GetUniformBufferID());
  
  tc::inf << "VertexShader" << tc::endl;
  pShader->InitShaderFromFile(GL_VERTEX_SHADER, "./Resources/Shaders/DeferredShading/geometry_pass-vertex.glsl");
  
  tc::inf << "FragmentShader" << tc::endl;
  pShader->InitShaderFromFile(GL_FRAGMENT_SHADER, "./Resources/Shaders/DeferredShading/geometry_pass-fragment.glsl");

  pShader->Link();

  TextureLoader* pTexLoader = new TextureLoader();
  Texture* pTexture = pTexLoader->loadTextureFromFile("./Resources/Textures/AI/AI_Corpus.dds");

  ModelLoader* pLoader = new ModelLoader();
  
  Mesh* pMesh = pLoader->loadMeshFromFile("./Resources/Meshes/AI/AI_Corpus_Smooth.obj");

  MeshNode* pMeshNode = new MeshNode(pShader, pMesh, pTexture);
  pTransformNode->AddChild(pMeshNode);
  m_pSceneGraphRoot->AddChild(pTransformNode);

  m_pCamTransformNode->SetTransformation(glm::mat4());
  m_pCamTransformNode->SetTranslation(glm::vec3(0.0, 0.0, 1.0));
  m_pCamTransformNode->Rotate(0.0f, glm::vec3(0.0, 1.0, 0.0));

  LightNode* pSpotlight = m_pLightManager->CreateLight();
  pSpotlight->SetLightAmbientCoefficient(0.001f);
  pSpotlight->SetLightAttenuation(1.0f);
  pSpotlight->SetLightConeAngle(15.0f);
  pSpotlight->SetLightIntensity(glm::vec3(0.6, 0.6, 0.3));
  pSpotlight->SetLightType(LightNode::SPOTLIGHT);

  TransformNode* pLightTransform = new TransformNode();
  pLightTransform->SetTranslation(glm::vec3(0.0, 0.0, 1.0));
  pLightTransform->Rotate(180.0, glm::vec3(0.0, 1.0, 0.0));
  pLightTransform->AddChild(pSpotlight);

  TransformNode* pSecondMesh = new TransformNode();
  pSecondMesh->SetTranslation(glm::vec3(0.0, 0.1, -3.0));

  pSecondMesh->AddChild(pMeshNode);
  m_pSceneGraphRoot->AddChild(pLightTransform);
  m_pSceneGraphRoot->AddChild(pSecondMesh);
}

Node * GameManager::GetSceneGraphRoot()
{
	return m_pSceneGraphRoot;
}

CameraNode* GameManager::GetCameraNode()
{
  return m_pCameraNode;
}

LightManager * GameManager::GetLightManager()
{
  return m_pLightManager;
}

PlayerNode* GameManager::GetPlayerNode()
{
  return m_pPlayer;
}


void GameManager::SetGlfwWindow(GLFWwindow* p_pWindow)
{
  m_pWindow = p_pWindow;
}

CameraTransformNode* GameManager::GetCameraTransformNode()
{
  return m_pCamTransformNode;
}

std::vector<AI*> GameManager::GetAIs()
{
  return m_vecAIs;
}

bool GameManager::WonOrLost()
{
  if (m_pPlayer == nullptr)
    return false;

  bool bWon = false;
  bool bLost = false;

  // Player Won?
  MapSegment* pPlayerSegment = m_pPlayer->GetCurrentMapSegment();

  if (pPlayerSegment != nullptr)
    bWon = pPlayerSegment->IsFinal();
  
  // Player lost?
  for (AI* pAI : m_vecAIs)
  {
    if (pAI->CaughtPlayer())
      bLost = true;
  }

  // Place Screen in Scene Graph.
  if (bWon || bLost)
  {    
    m_bEndOfGame = true;
    m_bWon = m_bWon || bWon;

    if (INFINITE_GAME && bWon)
    {
      tc::inf << "You reached the final room!" << tc::endl;
    }
  }

  return m_bEndOfGame;
}


bool GameManager::PlayerHasWon()
{
  return m_bWon;
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
