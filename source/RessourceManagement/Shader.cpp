/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "Shader.h"

#include "../TC.h"

#include <fstream>

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

Shader::Shader(GLuint glCameraUniformBufferID)
  : m_uiCameraUniformBufferID(glCameraUniformBufferID)
{
  
}

Shader::~Shader()
{
  // Tidy up.
  m_uiProgram = 0;

  m_bUsable = false;

  m_vecVertexShader.clear();
  m_vecFragmentShader.clear();

  m_vecUniforms.clear();
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

bool Shader::InitShaderFromFile(const unsigned int uiShaderType, const std::string& sShader)
{
  // Open the File
  std::fstream file(sShader.c_str(), std::ios_base::in | std::ios_base::binary);

  if (file.is_open())
  {
    // Determine File size.
    file.seekg(0, file.end);
    std::size_t stProgramLength = static_cast<std::size_t>(file.tellg());
    file.seekg(0, file.beg);

    // Store File Context.
    char* sProgramStr = new char[stProgramLength + 1];
    file.read(sProgramStr, stProgramLength);
    file.close();
    sProgramStr[stProgramLength] = '\0';

    // Create the Shader
    if (InitShaderFromString(uiShaderType, sProgramStr))
    {
      delete[] sProgramStr;
      return true;
    }
    else
    {
      delete[] sProgramStr;
      return false;
    }

  }
  else
  {
    tc::err << "CageShader::InitShaderFromFile -> Could not open Shader File." << tc::endl;
    tc::err << sShader << tc::endl;
    return false;
  }

  return true;
}

bool Shader::InitShaderFromString(const unsigned int uiShaderType, const std::string& sShader)
{
  // Create a Shader Program iff non exist.
  if (!m_uiProgram)
    m_uiProgram = glCreateProgram();

  // Create and store the Shader
  GLuint uiShader = glCreateShader(uiShaderType);

  switch (uiShaderType)
  {
  case GL_VERTEX_SHADER:
    m_vecVertexShader.push_back(uiShader);
    break;
  case GL_FRAGMENT_SHADER:
    m_vecFragmentShader.push_back(uiShader);
    break;
  case GL_TESS_CONTROL_SHADER:
    m_vecTesselationControlShader.push_back(uiShader);
    break;
  case GL_TESS_EVALUATION_SHADER:
    m_vecTesselationEvaluationShader.push_back(uiShader);
    break;
  case GL_GEOMETRY_SHADER:
    m_vecGeometryShader.push_back(uiShader);
    break;
  case GL_COMPUTE_SHADER:
    m_vecComputeShader.push_back(uiShader);
  default:
    tc::err << "CageShader::InitShaderFromString -> Unknown Shader Type." << tc::endl;
    return false;
  }

  // Compile and check the Shader.
  const char* pSrc = sShader.c_str();
  glShaderSource(uiShader, 1, &pSrc, NULL);
  glCompileShader(uiShader);

  GLint iCompileStatus = GL_FALSE;
  glGetShaderiv(uiShader, GL_COMPILE_STATUS, &iCompileStatus);
  if (iCompileStatus != GL_TRUE)
  {
    tc::err << "CageShader::InitShaderFromString -> Failed to compile Shader Program." << tc::endl;

    int iLogLength = 0;
    glGetShaderiv(uiShader, GL_INFO_LOG_LENGTH, &iLogLength);

    if (iLogLength > 0)
    {
      char* sLog = new char[iLogLength];
      glGetShaderInfoLog(uiShader, iLogLength, NULL, sLog);

      tc::err << "CageShader::InitShaderFromString:" << tc::endl;
      tc::err << sLog << tc::endl;

      delete[] sLog;
    }
  }

  // Attach the Shader
  glAttachShader(m_uiProgram, uiShader);

  return true;
}

bool Shader::InitFromFiles(const std::string& sVertexShader, const std::string& sFragmentShader)
{
  if (!InitShaderFromFile(GL_VERTEX_SHADER, sVertexShader) || !InitShaderFromFile(GL_FRAGMENT_SHADER, sFragmentShader))
  {
    return false;
  }

  return true;
}

bool Shader::InitFromStrings(const std::string& sVertexShader, const std::string& sFragmentShader)
{
  if (!InitShaderFromString(GL_VERTEX_SHADER, sVertexShader) || !InitShaderFromString(GL_FRAGMENT_SHADER, sFragmentShader))
  {
    return false;
  }

  return true;
}

bool Shader::Link()
{
  m_bUsable = false;

  if (!m_uiProgram)
  {
    tc::err << "CageShader::Link() -> Shader Program not created yet." << tc::endl;
    return false;
  }

  // Link Program.
  glLinkProgram(m_uiProgram);

  GLint iLinkStatus = GL_FALSE;
  glGetProgramiv(m_uiProgram, GL_LINK_STATUS, &iLinkStatus);

  // Error Checking.
  if (iLinkStatus == GL_FALSE)
  {

    GLint maxLength = 0;
    glGetProgramiv(m_uiProgram, GL_INFO_LOG_LENGTH, &maxLength);

    //The maxLength includes the NULL character
    std::vector<GLchar> infoLog(maxLength);
    glGetProgramInfoLog(m_uiProgram, maxLength, &maxLength, &infoLog[0]);


    std::string str;
    for (GLchar c : infoLog)
    {
      str.push_back(c);
    }

    tc::err << "CageShader::Link() -> " << str <<  tc::endl;
    return false;
  }
  else
  {
    m_bUsable = true;

    MapUniforms();

    return true;
  }
}

void Shader::Destroy()
{
  for (unsigned int i = 0; i < m_vecVertexShader.size(); ++i)
    glDeleteShader(m_vecVertexShader[i]);
  for (unsigned int i = 0; i < m_vecFragmentShader.size(); ++i)
    glDeleteShader(m_vecFragmentShader[i]);
  for (unsigned int i = 0; i < m_vecTesselationControlShader.size(); ++i)
    glDeleteShader(m_vecTesselationControlShader[i]);
  for (unsigned int i = 0; i < m_vecTesselationEvaluationShader.size(); ++i)
    glDeleteShader(m_vecTesselationEvaluationShader[i]);
  for (unsigned int i = 0; i < m_vecGeometryShader.size(); ++i)
    glDeleteShader(m_vecGeometryShader[i]);
  for (unsigned int i = 0; i < m_vecComputeShader.size(); ++i)
    glDeleteShader(m_vecComputeShader[i]);


  m_bUsable = false;

  m_vecUniforms.clear();
  m_vecVertexShader.clear();
  m_vecFragmentShader.clear();
  m_vecTesselationControlShader.clear();
  m_vecTesselationEvaluationShader.clear();
  m_vecGeometryShader.clear();
  m_vecComputeShader.clear();
}

void Shader::Bind()
{
  if (!m_bUsable)
  {
    tc::err << "CageShader::Bind() -> Shader is not linked. Bind has no Effect." << tc::endl;
  }
  else
  {
    glUseProgram(m_uiProgram);
  }
}

void Shader::Release()
{
  glUseProgram(0);
}

GLuint Shader::GetProgram() const
{
  return m_uiProgram;
}

GLuint Shader::GetUniformLocation(const std::string& sName) const
{
  std::map<std::string, GLint>::const_iterator it = m_vecUniforms.find(sName);

  if (it != m_vecUniforms.end())
    return (*it).second;
  else
    return -1;
}

void Shader::SetUniform(int iLoc, const GLfloat* pValue)
{
  glUniformMatrix4fv(iLoc, 1, GL_FALSE, pValue);
}

void Shader::SetUniform(int iLoc, int iUniform1)
{
  glUniform1i(iLoc, iUniform1);
}

void Shader::SetUniform(int iLoc, float fUniform1)
{
  glUniform1f(iLoc, fUniform1);
}

void Shader::SetUniform(int iLoc, float fUniform1, float fUniform2)
{
  glUniform2f(iLoc, fUniform1, fUniform2);
}

void Shader::SetUniform(int iLoc, float fUniform1, float fUniform2, float fUniform3)
{
  glUniform3f(iLoc, fUniform1, fUniform2, fUniform3);
}

void Shader::SetUniform(int iLoc, float fUniform1, float fUniform2, float fUniform3, float fUniform4)
{
  glUniform4f(iLoc, fUniform1, fUniform2, fUniform3, fUniform4);
}

void Shader::MapUniforms()
{
  m_vecUniforms.clear();
  int iNumOfUniforms, iMaxNameLength;

  glGetProgramiv(m_uiProgram, GL_ACTIVE_UNIFORMS, &iNumOfUniforms);
  glGetProgramiv(m_uiProgram, GL_ACTIVE_UNIFORM_MAX_LENGTH, &iMaxNameLength);

  if (iNumOfUniforms == 0 || iMaxNameLength == 0)
    return;

  GLchar* sName = new GLchar[iMaxNameLength];
  GLsizei iLength;
  GLint iSize;
  GLenum iType;

  for (int i = 0; i < iNumOfUniforms; ++i)
  {
    glGetActiveUniform(m_uiProgram, static_cast<GLuint>(i), iMaxNameLength, &iLength, &iSize, &iType, sName);
    const GLint iLoc = glGetUniformLocation(m_uiProgram, sName);
    m_vecUniforms[std::string(sName)] = iLoc;
  }

  delete[] sName;


  // Use camera uniform buffer for projective and view matrix

  GLuint camBufferIndex = glGetUniformBlockIndex(m_uiProgram, "Camera");
  if (camBufferIndex != -1)
  {
    const GLchar* uniformNames[3] =
    {
      "matView",
      "matProjection",
      "matInvTransView"
    };

    int blockSize;
    glGetActiveUniformBlockiv(m_uiProgram, camBufferIndex, GL_UNIFORM_BLOCK_DATA_SIZE, &blockSize);

    tc::inf << "Shader:: CameraUniformBlock Size : " << blockSize << tc::endl;

    glBindBuffer(GL_UNIFORM_BUFFER, m_uiCameraUniformBufferID);
    glBindBufferBase(GL_UNIFORM_BUFFER, 0, m_uiCameraUniformBufferID);
    glUniformBlockBinding(m_uiProgram, camBufferIndex, 0);

    GLuint indices[3];
    glGetUniformIndices(m_uiProgram, 3, uniformNames, indices);
    m_uiMatViewIndex = indices[0];
    m_uiMatProjectionIndex = indices[1];
    m_uiMatInvTransViewIndex = indices[2];

    GLint offsets[3];
    glGetActiveUniformsiv(m_uiProgram, 3, indices, GL_UNIFORM_OFFSET, offsets);
    m_uiMatViewOffset = offsets[0];
    m_uiMatProjectionOffset = offsets[1];
    m_uiMatIntTransViewOffset = offsets[2];

  }


  glGetError(); // I Know there is a glError because the locations get asked in a wrong way, nothing to worry we use STD140

}

bool Shader::IsUsable()
{
  return m_bUsable;
}

GLuint Shader::GetCameraUniformBufferID()
{
  return m_uiCameraUniformBufferID;
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/