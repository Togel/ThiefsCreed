#include "UpdateTraversalCallback.h"
/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "../SceneGraph/Node.h"
#include "../TC.h"
#include "../CollisionHandling.h"

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/
UpdateTraversalCallback::UpdateTraversalCallback(Node * pRoot, CollisionHandling* p_pCollisionHandler)
  : m_pRoot(pRoot), m_pCollisionHandler(p_pCollisionHandler)
{

}

UpdateTraversalCallback::~UpdateTraversalCallback()
{

}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/
void UpdateTraversalCallback::HandleEvents(Timer * pTimer)
{
  RecUpdate(m_pRoot, pTimer);

  if (m_pCollisionHandler != nullptr)
  {
    m_pCollisionHandler->performCollisionHandling();
  }
}

void UpdateTraversalCallback::RecUpdate(Node * pNode, Timer * pTimer)
{
  pNode->Update(pTimer);

  for (Node* child : pNode->GetChildren())
  {
    RecUpdate(child, pTimer);
  }
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
