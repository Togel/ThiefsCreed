#include "NoDepthBufferDrawCallback.h"
/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/

#include "../SceneGraph/Node.h"
#include "../SceneGraph/TransformNode.h"
#include "../SceneGraph/DrawableNode.h"

#include "GL/glew.h"


/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/
NoDepthBufferDrawCallback::NoDepthBufferDrawCallback(Node* pRoot)
  : m_pRoot(pRoot)
{
}

NoDepthBufferDrawCallback::~NoDepthBufferDrawCallback()
{
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/
void NoDepthBufferDrawCallback::HandleEvents(Timer * pTimer)
{
  recDraw(m_pRoot, glm::mat4());
}


void NoDepthBufferDrawCallback::recDraw(Node* pNode, glm::mat4 matTransformation)
{

  // disable depth buffer
  glDisable(GL_DEPTH_TEST);

  if (pNode->GetNodeType() == "TransformNode")
  {
    TransformNode* pTransfNode = dynamic_cast<TransformNode*>(pNode);
    matTransformation = matTransformation * pTransfNode->GetTransformation();
  }
  else if (pNode->GetNodeType() == "DrawableNode")
  {
    DrawableNode* pDrawableNode = dynamic_cast<DrawableNode*>(pNode);
    pDrawableNode->DrawWithoutDepthBuffer(matTransformation);
  }

  for (Node* child : pNode->GetChildren())
  {
    recDraw(child, matTransformation);
  }

  // enable depth buffer
  glEnable(GL_DEPTH_TEST);

}



/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
