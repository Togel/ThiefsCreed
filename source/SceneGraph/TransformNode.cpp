/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "TransformNode.h"

#include "gtc/matrix_transform.hpp"
/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

TransformNode::TransformNode()
{
  m_matTransformation = glm::mat4();
}

TransformNode::~TransformNode()
{
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

std::string TransformNode::GetNodeType()
{
  return "TransformNode";
}

void TransformNode::SetTransformation(glm::mat4 matTransformation)
{
  m_matTransformation = matTransformation;
}

glm::mat4 TransformNode::GetTransformation()
{
  return m_matTransformation;
}

glm::vec3 TransformNode::GetTranslation()
{
  return glm::vec3(m_matTransformation[3].x, m_matTransformation[3].y, m_matTransformation[3].z);
}

void TransformNode::Translate(glm::vec3 v3Translation)
{
  m_matTransformation[3] += glm::vec4(v3Translation, 0.0f);
}

void TransformNode::SetTranslation(glm::vec3 v3Translation)
{
  m_matTransformation[3] = glm::vec4(v3Translation, m_matTransformation[3][3]);
}

void TransformNode::Rotate(glm::mat3 m3Rotation)
{
  m_matTransformation = glm::mat4(m3Rotation) * m_matTransformation;
}

void TransformNode::Rotate(float angle, glm::vec3 axis)
{
  m_matTransformation = glm::rotate(m_matTransformation, angle, axis);
}

void TransformNode::Scale(float scaleFactor)
{
  m_matTransformation[0] *= scaleFactor;
  m_matTransformation[1] *= scaleFactor;
  m_matTransformation[2] *= scaleFactor;
  // do not scale translation
}

void TransformNode::Scale(glm::vec3 vecScaleFactors)
{
  m_matTransformation[0] *= vecScaleFactors[0];
  m_matTransformation[1] *= vecScaleFactors[1];
  m_matTransformation[2] *= vecScaleFactors[2];
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
