#include "Node.h"
#include "TransformNode.h"

#include <algorithm>



Node::Node()
{
}

Node::~Node()
{
}

std::string Node::GetNodeType()
{
	return "Node";
}

Node * Node::GetParent()
{
	return m_pParent;
}

void Node::SetParent(Node * pParent)
{
	m_pParent = pParent;
}

size_t Node::GetNumChildren()
{
	return m_vecChildren.size();
}

std::vector<Node*>& Node::GetChildren()
{
	return m_vecChildren;
}

void Node::AddChild(Node * pChild)
{
	pChild->SetParent(this);
	m_vecChildren.push_back(pChild);
}

bool Node::RemoveChild(Node * pChild)
{
	// check if child is child of this node
	std::vector<Node*>::iterator pos = std::find(m_vecChildren.begin(), m_vecChildren.end(), pChild);
	if (pos != m_vecChildren.end())
	{
		pChild->SetParent(nullptr);
		m_vecChildren.erase(pos);
		return true;
	}
	else
	{
		return false;
	}
}

void Node::Update(Timer * pTime)
{
	// Basic behaviour is to do nothing in each frame.
}

glm::mat4 Node::getAncestorTransformation()
{
  // accumulation matrix
  glm::mat4 ModelMatrix = glm::mat4();
  Node* pParent = GetParent();
  // iteratively add up transformations
  while(pParent != nullptr)
  {
    if(pParent->GetNodeType() == "TransformNode"
       || pParent->GetNodeType() == "CameraTransformNode")
    {
      TransformNode* pTransParent = dynamic_cast<TransformNode*>(pParent);
      ModelMatrix =  ModelMatrix * pTransParent->GetTransformation();

    }
    pParent = pParent->GetParent();
  }

  return ModelMatrix;
}

void Node::SetActive(bool bActive)
{
  m_bActive = bActive;
}

bool Node::IsActive()
{
  return m_bActive;
}
