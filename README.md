Thief's Creed
==============

General Information
--------------

Thief's Creed is a sneak and creep game, where the player has to run through a maze and find a target position.
The maze has several "dead end" segments, which are separated from the rest by curtains. The player starts in one of these segments and the target position is at another of these segments.
On his/her way through the maze, the player is chased by an enemy AI, embodied by drones. They patrol through the maze and fly towards the player once they spot him/her. If the player gets caught by the drones, the game is lost.
If the player hides behind a curtain, he/she cannot be seen by the AI.
The AI gives a hint about how close they are to catching the player. Their lights change colors according to this:
* White-blue: The drone is on the usual patrol.
* Yellow-green: The drone has spotted the player in some distance and is chasing him/her.
* Orange: The drone is close to the player and leaves it's patrol routes to catch him/her. You should run!
* Red: The drone has caught the player. This will only be recognizable in the endless game mode (see Instructions), since now the game is lost.

A representative screenshot can be found here ![Thiefs Creed](screenshot.jpg) .



Building and Running the Game
--------------

Thief's Creed can be build using CMAKE. Upon cloning the repository, create a build folder at an arbitrary location.
Use
```
cmake path/to/ThiefsCreed
make
```

The binaries will be copied into the folder `ThiefsCreed/binaries` (in the project folder, not the build folder).
Run the game using `ThiefsCreed/binaries/ThiefsCreed`.



Instructions
--------------

The game is controlled in a common first person fashion. The player can move with WASD (or alternatively the arrow keys) and look around using the mouse.
The mouse sensitivity can be set in the `ThiefsCreed/source/Settings.h` file.

Furthermore, the following settings can be specified in this file:
* Screen resolution/size.
* Size of the generated map:
  * Number of the generated segments.
  * Maximum Stretch of the map in x and y direction (MapWidth and MapHeight) Make sure, the number of generated segments is lower than MapWidth*MapHeight or the program will complain.
* The difficulty, i.e. how many AI drones will chase you.
* Cheating modes can be enabled.
* If desired, the game can be continued after the player has won or lost (endless game mode).



Authors
--------------

Alexander Meißner

Lukas Saretzki

Joschka Kliege


Geometry and Textures By
--------------

Benedikt Bockisch (Media Design - TH Amberg)