#ifndef _CLOTH_INTEGRATION_CALLBACK_
#define _CLOTH_INTEGRATION_CALLBACK_

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "Callback.h"

#include <glm.hpp>
/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/
class Node;

/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class ClothIntegrationCallback : public Callback
{
public:
  ClothIntegrationCallback(Node* pGraphRoot);
  virtual ~ClothIntegrationCallback();

  virtual void HandleEvents(Timer* pTimer);
private:
  Node* m_pGraphRoot;
  Timer* m_pTimer;

  virtual void RecIntegrate(Node* pNode, glm::mat4 matTransform);
};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
#endif
