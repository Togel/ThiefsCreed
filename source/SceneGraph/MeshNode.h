#ifndef _DRAWABLE_
#define _DRAWABLE_

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include <map>
#include <vector>
#include "GL/glew.h"
#include "glm.hpp"
#include "DrawableNode.h"

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/

static const glm::mat4 mat4NullMatrix = glm::mat4(0.0f);

/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/
class Mesh;
class Texture;
class Shader;

/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/


struct DrawableMesh
{
  enum DrawType
  {
    DT_OPAQUE,
    DT_TRANSPARENT,
    DT_NO_DEPTH_BUFFER
  };


  Mesh*    m_pMesh      = nullptr;
  float    m_fAlpha     = 1.0f;
  Texture* m_pTexture   = nullptr;
  Texture* m_pNormalMap = nullptr;
  DrawType m_eDrawType = DrawType::DT_OPAQUE;

  DrawableMesh()
  {
  }

  DrawableMesh(Mesh* pMesh, float fAlpha, Texture* pTexture, Texture* pNormalMap, DrawType pDrawType)
  {
    m_pMesh      = pMesh;
    m_fAlpha     = fAlpha;
    m_pTexture   = pTexture;
    m_pNormalMap = pNormalMap;
    m_eDrawType  = pDrawType;
  }
};

class MeshNode : public DrawableNode
{
public:
  /*
  * Constructor.
  */
  MeshNode(Shader* pShader, Mesh* pMesh, Texture* pTexture, Texture* pNormalMap = nullptr, float fAlpha = 1.0f,
           DrawableMesh::DrawType pDrawType = DrawableMesh::DrawType::DT_OPAQUE);
  MeshNode(Shader* pShader, std::vector<DrawableMesh> vecMeshes);

  /*
  * Destructor.
  */
  virtual ~MeshNode();

  /*
  * Gets called by the draw thread in each frame
  * Override in inheriting node types to implement drawing
  * @param matrix that stores all transformations of parents
  */
  void DrawOpaque(glm::mat4 matTransformation) override;

  /*
  * Gets called by the draw thread in each frame
  * Override in inheriting node types to implement drawing
  * @param matrix that stores all transformations of parents
  */
  void DrawTransparent(glm::mat4 matTransformation) override;

  /*
  * Gets called by the draw thread in each frame
  * Override in inheriting nodetypes to implement drawing
  * @param matrix that stores all transformations of parents
  */
  void DrawWithoutDepthBuffer(glm::mat4 matTransformation) override;

  /*
   * Get the stored Mesh. If several Meshes are stored, then
   * the first one is returned.
   * @return The (first) stored Mesh.
   */
  Mesh* getFirstStoredMesh();

protected:
  std::vector<DrawableMesh> m_vecMeshes; // Store all Meshes and their Alpha Values, Textures and Normal Maps.

protected:
  /*
  * Draws the Mesh.
  * @param 'matTransformation' Matrix that stores all transformations of Parents.
  * @param 'oMesh' defines the Drawable Mesh.
  */
  void Draw(glm::mat4 matTransformation, DrawableMesh pMesh);

  /*
  * Uploads updated Uniform Information to Graphics Card.
  * Overwrite this Function.
  * @param 'bSet' defines whether the Uniforms are set or re-set.
  */
  virtual void UpdateUniforms(bool bSet);

private:
  MeshNode();

};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
#endif