/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "TextureLoader.h"

#include <fstream>
#include <sstream>
#include <cstring>

#include "../TC.h"


/******************************************************************************/
/*                                  DEFINES                                   */
/******************************************************************************/
#define FOURCC_DXT1 0x31545844 // Equivalent to "DXT1" in ASCII
#define FOURCC_DXT3 0x33545844 // Equivalent to "DXT3" in ASCII
#define FOURCC_DXT5 0x35545844 // Equivalent to "DXT5" in ASCII


/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/



/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

bool TextureLoader::loadTexture(const std::string pImagePath)
{
  // set texture not read
  m_textureRead = false;

  // DDS file
  std::string sub = pImagePath.substr(pImagePath.size() - 4);
  if (sub == ".dds" || sub == ".DDS")
  {
    m_textureRead = loadDDS(pImagePath);
  }

  /*
   * Keep going with other image formats:
   * else if() ...
   */

  if (m_textureRead)
  {
    m_sPath = pImagePath;
    m_eTextureType = GL_TEXTURE_2D;
  }

  return m_textureRead;
}



Texture* TextureLoader::createTextureFromBuffer()
{
  // nothing in buffer
  if (!m_textureRead)
    return 0;

  Texture* texture = new Texture(m_iTexID, m_sPath, m_eTextureType);

  return texture;
}


Texture* TextureLoader::loadTextureFromFile(const std::string pImagePath)
{
  loadTexture(pImagePath); // success check implicitely in next call
  return createTextureFromBuffer();
}


/*
 * Adapted from http://www.opengl-tutorial.org.
 */
bool TextureLoader::loadDDS(const std::string pImagePath){

  const char * imagepath = pImagePath.c_str();

  unsigned char header[124];

  FILE *fp;

  /* try to open the file */
  fp = fopen(imagepath, "rb");
  if (fp == NULL){
    tc::err << "Texture Loader: " << pImagePath << " could not be opened." << tc::endl;
    return false;
  }

  /* verify the type of file */
  char filecode[4];
  size_t read = fread(filecode, 1, 4, fp);
  if (strncmp(filecode, "DDS ", 4) != 0) {
    fclose(fp);
    tc::err << "Texture Loader: " << pImagePath << " is not a DDS file." << tc::endl;
    return false;
  }

  /* get the surface desc */
  read = fread(&header, 124, 1, fp);

  unsigned int height      = *(unsigned int*)&(header[8 ]);
  unsigned int width	     = *(unsigned int*)&(header[12]);
  unsigned int linearSize	 = *(unsigned int*)&(header[16]);
  unsigned int mipMapCount = *(unsigned int*)&(header[24]);
  unsigned int fourCC      = *(unsigned int*)&(header[80]);

  unsigned char * buffer;
  unsigned int bufsize;
  /* how big is it going to be including all mipmaps? */
  bufsize = mipMapCount > 1 ? linearSize * 2 : linearSize;
  buffer = (unsigned char*)malloc(bufsize * sizeof(unsigned char));
  read = fread(buffer, 1, bufsize, fp);
  /* close the file pointer */
  fclose(fp);

  //unsigned int components  = (fourCC == FOURCC_DXT1) ? 3 : 4;
  unsigned int format;
  switch(fourCC)
  {
  case FOURCC_DXT1:
    format = GL_COMPRESSED_RGBA_S3TC_DXT1_EXT;
    break;
  case FOURCC_DXT3:
    format = GL_COMPRESSED_RGBA_S3TC_DXT3_EXT;
    break;
  case FOURCC_DXT5:
    format = GL_COMPRESSED_RGBA_S3TC_DXT5_EXT;
    break;
  default:
    free(buffer);
    return false;
  }

  // Create one OpenGL texture
  glGenTextures(1, &m_iTexID);

  // "Bind" the newly created texture : all future texture functions will modify this texture
  glBindTexture(GL_TEXTURE_2D, m_iTexID);
  glPixelStorei(GL_UNPACK_ALIGNMENT,1);

  unsigned int blockSize = (format == GL_COMPRESSED_RGBA_S3TC_DXT1_EXT) ? 8 : 16;
  unsigned int offset = 0;

  /* load the mipmaps */
  for (unsigned int level = 0; level < mipMapCount && (width || height); ++level)
  {
    unsigned int size = ((width+3)/4)*((height+3)/4)*blockSize;
    glCompressedTexImage2D(GL_TEXTURE_2D, level, format, width, height,
      0, size, buffer + offset);

    offset += size;
    width  /= 2;
    height /= 2;

    // Deal with Non-Power-Of-Two textures. This code is not included in the webpage to reduce clutter.
    if(width < 1) width = 1;
    if(height < 1) height = 1;

  }

  free(buffer);

  // set filtering and MipMapping mode
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

  // generate MipMaps if necessary
  if (mipMapCount <= 1)
  {
    glGenerateMipmap(GL_TEXTURE_2D);
  }

  // Unbind Buffer.
  glBindTexture(GL_TEXTURE_2D, 0);

  return true;
}


Texture* TextureLoader::loadCubeMapFromDDSFile(const std::string pTop, const std::string pBottom,
                            const std::string pFront, const std::string pBack,
                            const std::string pLeft, const std::string pRight)
{
  loadCubeMapFromDDS(pTop, pBottom, pFront, pBack, pLeft, pRight);
  return createTextureFromBuffer();
}


bool TextureLoader::loadCubeMapFromDDS(const std::string pTop, const std::string pBottom,
                            const std::string pFront, const std::string pBack,
                            const std::string pLeft, const std::string pRight)
{
  // Create one OpenGL texture
  glGenTextures(1, &m_iTexID);
  
  glEnable(GL_TEXTURE_CUBE_MAP);

  // "Bind" the newly created texture : all future texture functions will modify this texture
  glBindTexture(GL_TEXTURE_CUBE_MAP, m_iTexID);
  glPixelStorei(GL_UNPACK_ALIGNMENT,1);


  // create arrays to use a for loop for set up
  std::vector<std::string> paths;
  paths.push_back(pTop);
  paths.push_back(pBottom);
  paths.push_back(pFront);
  paths.push_back(pBack);
  paths.push_back(pLeft);
  paths.push_back(pRight);

  std::vector<GLenum> sideTypes;
  sideTypes.push_back(GL_TEXTURE_CUBE_MAP_POSITIVE_Y);
  sideTypes.push_back(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y);
  sideTypes.push_back(GL_TEXTURE_CUBE_MAP_POSITIVE_Z);
  sideTypes.push_back(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z);
  sideTypes.push_back(GL_TEXTURE_CUBE_MAP_NEGATIVE_X);
  sideTypes.push_back(GL_TEXTURE_CUBE_MAP_POSITIVE_X);

  // load all 6 textures
  for (size_t i = 0; i < 6; ++i)
  {
    const char * imagepath = paths[i].c_str();

    unsigned char header[124];

    FILE *fp;

    /* try to open the file */
    fp = fopen(imagepath, "rb");
    if (fp == NULL){
      tc::err << "Texture Loader: " << paths[i] << " could not be opened." << tc::endl;
      return false;
    }

    /* verify the type of file */
    char filecode[4];
    size_t read = fread(filecode, 1, 4, fp);
    if (strncmp(filecode, "DDS ", 4) != 0) {
      fclose(fp);
      tc::err << "Texture Loader: " << paths[i] << " is not a DDS file." << tc::endl;
      return false;
    }

    /* get the surface desc */
    read = fread(&header, 124, 1, fp);

    unsigned int height      = *(unsigned int*)&(header[8 ]);
    unsigned int width	     = *(unsigned int*)&(header[12]);
    unsigned int linearSize	 = *(unsigned int*)&(header[16]);
    unsigned int mipMapCount = *(unsigned int*)&(header[24]);
    unsigned int fourCC      = *(unsigned int*)&(header[80]);

    unsigned char * buffer;
    unsigned int bufsize;
    /* how big is it going to be including all mipmaps? */
    bufsize = mipMapCount > 1 ? linearSize * 2 : linearSize;
    buffer = (unsigned char*)malloc(bufsize * sizeof(unsigned char));
    read = fread(buffer, 1, bufsize, fp);
    /* close the file pointer */
    fclose(fp);

    //unsigned int components  = (fourCC == FOURCC_DXT1) ? 3 : 4;
    unsigned int format;
    switch(fourCC)
    {
    case FOURCC_DXT1:
      format = GL_COMPRESSED_RGBA_S3TC_DXT1_EXT;
      break;
    case FOURCC_DXT3:
      format = GL_COMPRESSED_RGBA_S3TC_DXT3_EXT;
      break;
    case FOURCC_DXT5:
      format = GL_COMPRESSED_RGBA_S3TC_DXT5_EXT;
      break;
    default:
      free(buffer);
      return false;
    }

    unsigned int blockSize = (format == GL_COMPRESSED_RGBA_S3TC_DXT1_EXT) ? 8 : 16;
    unsigned int offset = 0;

    /* load the mipmaps */
    for (unsigned int level = 0; level < mipMapCount && (width || height); ++level)
    {
      unsigned int size = ((width+3)/4)*((height+3)/4)*blockSize;

      glCompressedTexImage2D(sideTypes[i], level, format, width, height,
                             0, size, buffer + offset);

      offset += size;
      width  /= 2;
      height /= 2;

      // Deal with Non-Power-Of-Two textures. This code is not included in the webpage to reduce clutter.
      if(width < 1) width = 1;
      if(height < 1) height = 1;

    }

    free(buffer);
    
    // TODO: generate Mip Maps if they are not given in the dds file.
  }

  
  // set filtering and MipMapping mode
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);  

  // Unbind Buffer.
  glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

  m_sPath = pTop;
  m_eTextureType = GL_TEXTURE_CUBE_MAP;
  
  m_textureRead = true;

  return true;
}


/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/