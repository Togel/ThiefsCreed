/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "Timer.h"

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

Timer::Timer()
{
  m_tLastTime = std::chrono::high_resolution_clock::now();
}

Timer::~Timer()
{

}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

void Timer::Update()
{
  std::chrono::high_resolution_clock::time_point tCurTime = std::chrono::high_resolution_clock::now();
  std::chrono::microseconds us = std::chrono::duration_cast<std::chrono::microseconds>(tCurTime - m_tLastTime);

  m_fElapsedTime = static_cast<float>(us.count()) / 1000;

  m_tLastTime = tCurTime;
}

float Timer::GetElapsedTime()
{
  return m_fElapsedTime;
}

float Timer::GetElapsedTime_withoutUpdate()
{
  std::chrono::high_resolution_clock::time_point tCurTime = std::chrono::high_resolution_clock::now();
  std::chrono::microseconds us = std::chrono::duration_cast<std::chrono::microseconds>(tCurTime - m_tLastTime);

  return static_cast<float>(us.count()) / 1000;
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/