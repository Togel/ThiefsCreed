set( RelativeDir "Resources/Shaders/ShadowMapping" )
set( RelativeSourceGroup "ResourceFiles\\Shaders\\ShadowMapping" )

set(Directories
 
)

set(DirFiles
  ShadowMapping-Fragment.glsl
  ShadowMapping-Vertex.glsl
  _SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )
	
set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()

source_group( ${RelativeSourceGroup} FILES ${LocalSourceGroupFiles} )

foreach( subdir ${Directories})
	include("${RelativeDir}/${subdir}/_SourceFiles.cmake")
endforeach()

set( RelativeDir "Resources/Shaders")


