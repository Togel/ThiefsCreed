#ifndef _MESH_
#define _MESH_

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include <vector>
#include "GL/glew.h"
#include "glm.hpp"

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/
class Texture;

/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class Mesh
{
public:
  /*
  * Constructor.
  */
  Mesh();

  Mesh(std::vector<glm::vec3> vecVertices,
    std::vector<glm::vec3> vecNormals,
    std::vector<glm::vec2> vecUVs,
    Texture* pTexture,
    Texture* pNormalMap);

  Mesh(std::vector<glm::vec3> vecVertices,
    std::vector<glm::vec3> vecNormals,
    std::vector<glm::vec2> vecUVs,
    std::vector<unsigned int> vecFaces_indices,
    Texture* pTexture,
    Texture* pNormalMap);

  /*
  * Destructor.
  */
  ~Mesh();

  /*
  * Rotates the Vertices around the origin.
  * @param 'v3Rot' defines the Rotation around the x-, y- and z-axis.
  */
  void Rotate(glm::vec3 v3Rot);

  /*
   * Initialize the mesh, i.e. generate buffers etc.
   */
  void init();

  /*
  * Uploads the Data to the Graphics Card.
  */
  void Upload();

  /*
  * Draws the Mesh.
  */
  void Draw();

  /*
   * Computes the tangent basis, used for normal mapping.
   * Only works for indexed meshes.
   */
  void ComputeTangentBasis();

  /*
  * Returns a Vector storing the Vertices of the Mesh.
  * @return A Vector storing the Vertices of the Mesh.
  */
  std::vector<glm::vec3> GetVertices();

  /*
  * Returns a Vector storing the Normals of the Mesh.
  * @return A Vector storing the Normals of the Mesh.
  */
  std::vector<glm::vec3> GetNormals();

  /*
  * Returns a Vector storing the UVs of the Mesh.
  * @return A Vector storing the UVs of the Mesh.
  */
  std::vector<glm::vec2> GetUVs();

  /*
   * Returns a Vector storing for each face the (vertex etc) indices.
   */
  std::vector<unsigned int> getFaces_indices();

  /*
  * Returns a Pointer to the Texture.
  * @return a Pointer to the Texture.
  */
  Texture* GetTexture();

  /*
  * Returns a Pointer to the Normal Map.
  * @return a Pointer to the Normal Map.
  */
  Texture* GetNormalMap();

  /*
  * Returns the Vertex Buffer.
  * @return The Vertex Buffer.
  */
  GLuint& GetVertexBuffer();

  /*
  * Returns the Normal Buffer.
  * @return The Normal Buffer.
  */
  GLuint& GetNormalBuffer();

  /*
  * Returns the UV Buffer.
  * @return The UV Buffer.
  */
  GLuint& GetUVBuffer();

  /*
  * Returns the Element Array Buffer.
  * @return The Element Array Buffer.
  */
  GLuint& GetElementArrayBuffer();


  /*
   * Sets the vertices of the Mesh.
   */
  void setVertices(std::vector<glm::vec3> vecVertices);

  /*
   * Sets the normals of the Mesh.
   */
  void setNormals(std::vector<glm::vec3> vecNormals);

  /*
   * Sets the UV-texture-coordinates of the Mesh.
   */
  void setUVs(std::vector<glm::vec2> vecUVs);

  /*
   * Sets the faces of the Mesh as a list of vertex indices.
   */
  void setFaces_indices(std::vector<unsigned int> vecFaces_indices);

  /*
   * Sets the Texture.
   */
  void setTexture(Texture* pTexture);

  /*
   * Sets the Normal Map.
   */
  void setNormalMap(Texture* pNormalMap);

private:
  std::vector<unsigned int> m_vecFaces_indices;
  std::vector<glm::vec3> m_vecVertices;
  std::vector<glm::vec3> m_vecNormals;
  std::vector<glm::vec2> m_vecUVs;

  // tangent basis
  std::vector<glm::vec3> m_vecTangents;
  std::vector<glm::vec3> m_vecBitangents;

  GLsizei m_iSize = 0;
  bool m_useIndexBuffer = false;

  Texture* m_pTexture = nullptr;
  Texture* m_pNormalMap = nullptr;

  GLuint m_iVAO     = 0; // VAO
  GLuint m_iVerBuff = 0; // VBO
  GLuint m_iNorBuff = 0; // VBO
  GLuint m_iUVBuff  = 0; // VBO
  GLuint m_iEAB     = 0; // Element Array Buffer

  // normal mapping
  GLuint m_iTangBuff = 0; // VBO
  GLuint m_iBitangBuff = 0; // VBO
};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
#endif