/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "ModelLoader.h"

#include <fstream>
#include <sstream>
#include <map>


/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/



/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

bool ModelLoader::loadModel(const std::string pFilename)
{
  // delete privious buffer data
  clearModelBuffer();

  // Object file
  std::string sub = pFilename.substr(pFilename.size() - 4);
  if (sub == ".obj" || sub == ".OBJ")
  {
    m_modelRead = loadObjFile(pFilename);
  }

  /*
   * Keep going with other file formats:
   * else if() ...
   */

  /*
   * TODO: make sure, that one Element Array Buffer
   * can be used, even if the index arrays use
   * different indices
   * (start with: if (m_modelRead)...)
   */

  return m_modelRead;
}


void ModelLoader::copyBufferToMesh(Mesh* pMesh)
{
  if (m_modelRead)
  {
    pMesh->setFaces_indices( m_vecFaces_vertexIds );
    pMesh->setVertices( m_vecVertices );
    pMesh->setNormals( m_vecNormals );
    pMesh->setUVs( m_vecUVs );
  }
}


Mesh* ModelLoader::createMeshFromBuffer()
{
  // nothing in buffer
  if (!m_modelRead)
    return 0;

  Mesh* mesh = new Mesh(m_vecVertices, m_vecNormals, m_vecUVs, m_vecFaces_vertexIds, 0, 0);

  return mesh;
}


Mesh* ModelLoader::loadMeshFromFile(const std::string pFilename)
{
  loadModel(pFilename); // success check implicitely in next call
  return createMeshFromBuffer();
}


void ModelLoader::clearModelBuffer()
{
  m_vecFaces_vertexIds.clear();
  m_vecFaces_normalIds.clear();
  m_vecFaces_UVIds.clear();
  m_vecVertices.clear();
  m_vecNormals.clear();
  m_vecUVs.clear();

  m_modelRead = false;
}


bool ModelLoader::loadObjFile(const std::string pFilename)
{
  // clear previous model
  clearModelBuffer();

  // open file
  std::ifstream modelFile;
  modelFile.open(pFilename.c_str());
  if (!modelFile.is_open())
  {
    // error, could not open file
    return false;
  }

  bool readingError = false;

  // process line by line
  std::string line;
  while (std::getline(modelFile, line))
  {
    std::istringstream iss(line);

    // check first token
    std::string ft;
    iss >> ft;


    // new vertex
    if (ft == "v")
    {
      glm::vec3 vertex;
      iss >> vertex.x >> vertex.y >> vertex.z;
      m_vecVertices.push_back(vertex);

      // check for errors
      if (iss.fail()) readingError = true;
    }


    // new vertex texture coordinta
    if (ft == "vt")
    {
      glm::vec2 uv;
      iss >> uv.x >> uv.y;
      m_vecUVs.push_back(uv);

      // check for errors
      if (iss.fail()) readingError = true;
    }


    // new vertex normal
    if (ft == "vn")
    {
      glm::vec3 normal;
      iss >> normal.x >> normal.y >> normal.z;
      m_vecNormals.push_back(normal);

      // check for errors
      if (iss.fail()) readingError = true;
    }


    // new face
    if (ft == "f")
    {
      // get 4 istringstreams to check all 4 possible formats
      std::istringstream iss2(line);
      std::istringstream iss3(line);
      std::istringstream iss4(line);
      iss2 >> ft;
      iss3 >> ft;
      iss4 >> ft;

      // input variables
      unsigned int vertexId_1;
      unsigned int vertexId_2;
      unsigned int vertexId_3;
      unsigned int uvId_1;
      unsigned int uvId_2;
      unsigned int uvId_3;
      unsigned int normalId_1;
      unsigned int normalId_2;
      unsigned int normalId_3;

      char slash_1;
      char slash_2;
      char slash_3;
      char slash_4;
      char slash_5;
      char slash_6;

      // first option: 1 2 3 (only vertices)
      iss >> vertexId_1 >> vertexId_2 >> vertexId_3;
      if (!iss.fail())
      {
        // store
        m_vecFaces_vertexIds.push_back(vertexId_1);
        m_vecFaces_vertexIds.push_back(vertexId_2);
        m_vecFaces_vertexIds.push_back(vertexId_3);
        // head to next line
        continue;
      }

      // second option: 2/1 3/1 4/1 (vertices and uv-texture-coords)
      iss2 >> vertexId_1 >> slash_1 >> uvId_1;
      iss2 >> vertexId_2 >> slash_2 >> uvId_2;
      iss2 >> vertexId_3 >> slash_3 >> uvId_3;
      if (!iss2.fail() && slash_1 == '/' && slash_2 == '/' && slash_3 == '/')
      {
        // store
        m_vecFaces_vertexIds.push_back(vertexId_1);
        m_vecFaces_vertexIds.push_back(vertexId_2);
        m_vecFaces_vertexIds.push_back(vertexId_3);
        m_vecFaces_UVIds.push_back(uvId_1);
        m_vecFaces_UVIds.push_back(uvId_2);
        m_vecFaces_UVIds.push_back(uvId_3);
        // head to next line
        continue;
      }

      // third option: 2//1 3//1 4//1 (vertices and normals)
      iss3 >> vertexId_1 >> slash_1 >> slash_2 >> normalId_1;
      iss3 >> vertexId_2 >> slash_3 >> slash_4 >> normalId_2;
      iss3 >> vertexId_3 >> slash_5 >> slash_6 >> normalId_3;
      if (!iss3.fail() && slash_1 == '/' && slash_2 == '/' && slash_3 == '/'
          && slash_4 == '/' && slash_5 == '/' && slash_6 == '/')
      {
        // store
        m_vecFaces_vertexIds.push_back(vertexId_1);
        m_vecFaces_vertexIds.push_back(vertexId_2);
        m_vecFaces_vertexIds.push_back(vertexId_3);
        m_vecFaces_normalIds.push_back(normalId_1);
        m_vecFaces_normalIds.push_back(normalId_2);
        m_vecFaces_normalIds.push_back(normalId_3);
        // head to next line
        continue;
      }

      // fourth option: 2/3/1 3/4/1 4/6/1 (vertices, uv-texture-coords and normals)
      iss4 >> vertexId_1 >> slash_1 >> uvId_1 >> slash_2 >> normalId_1;
      iss4 >> vertexId_2 >> slash_3 >> uvId_2 >> slash_4 >> normalId_2;
      iss4 >> vertexId_3 >> slash_5 >> uvId_3 >> slash_6 >> normalId_3;
      if (!iss4.fail() && slash_1 == '/' && slash_2 == '/' && slash_3 == '/'
          && slash_4 == '/' && slash_5 == '/' && slash_6 == '/')
      {
        // store
        m_vecFaces_vertexIds.push_back(vertexId_1);
        m_vecFaces_vertexIds.push_back(vertexId_2);
        m_vecFaces_vertexIds.push_back(vertexId_3);
        m_vecFaces_UVIds.push_back(uvId_1);
        m_vecFaces_UVIds.push_back(uvId_2);
        m_vecFaces_UVIds.push_back(uvId_3);
        m_vecFaces_normalIds.push_back(normalId_1);
        m_vecFaces_normalIds.push_back(normalId_2);
        m_vecFaces_normalIds.push_back(normalId_3);
        // automatically heading to next line now
      }

      // error case, no matching found
      else{
        readingError = true;
      }
    }

  }

  // close file
  modelFile.close();

  // reduce indices by one, since .obj starts at 1, we need start at 0
  for (size_t i = 0; i < m_vecFaces_vertexIds.size(); ++i)
  {
    m_vecFaces_vertexIds[i] = m_vecFaces_vertexIds[i] - 1;
  }
  for (size_t i = 0; i < m_vecFaces_normalIds.size(); ++i)
  {
    m_vecFaces_normalIds[i] = m_vecFaces_normalIds[i] - 1;
  }
  for (size_t i = 0; i < m_vecFaces_UVIds.size(); ++i)
  {
    m_vecFaces_UVIds[i] = m_vecFaces_UVIds[i] - 1;
  }

  // adjust indices to OpenGL format
  adjustObjIndices();

  return !readingError;
}


void ModelLoader::adjustObjIndices()
{
  // keep track of created vertices
  std::map<std::string, unsigned int> inList;

  // new storage
  std::vector<unsigned int> indices;
  std::vector<glm::vec3> vertices;
  std::vector<glm::vec3> normals;
  std::vector<glm::vec2> uvs;

  // go through all vertex IDs since they are always given
  for (size_t i = 0; i < m_vecFaces_vertexIds.size(); ++i)
  {
    // check if the coordinates are inserted already
    std::stringstream sst;
    // vertex index
    sst << m_vecFaces_vertexIds[i] << ":";
    // uv index
    if (i < m_vecFaces_UVIds.size())
    {
      sst << m_vecFaces_UVIds[i] << ":";
    }
    // normal index
    if (i < m_vecFaces_normalIds.size())
    {
      sst << m_vecFaces_normalIds[i];
    }

    // serach for entry
    std::string key = sst.str();
    std::map<std::string, unsigned int>::iterator elmt = inList.find(key);

    // vertex already created
    if ( elmt != inList.end() )
    {
      // use respective index
      indices.push_back( elmt->second );
    }

    // vertex not extisting
    else {
      // create vertex
      vertices.push_back( m_vecVertices[ m_vecFaces_vertexIds[i] ] );

      if (i < m_vecFaces_UVIds.size())
      {
        uvs.push_back( m_vecUVs[ m_vecFaces_UVIds[i] ] );
      }

      if (i < m_vecFaces_normalIds.size())
      {
        normals.push_back( m_vecNormals[ m_vecFaces_normalIds[i] ] );
      }

      // store index of created vertex
      unsigned int index = vertices.size() - 1;
      indices.push_back(index);

      // store in map
      inList.insert( std::pair<std::string, unsigned int>(key, index) );
    }
  }

  // store result
  m_vecVertices = vertices;
  m_vecUVs = uvs;
  m_vecNormals = normals;
  m_vecFaces_vertexIds = indices;
  m_vecFaces_UVIds = indices;
  m_vecFaces_normalIds = indices;
}


/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/