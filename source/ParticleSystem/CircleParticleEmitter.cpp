/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "CircleParticleEmitter.h"
#include "Particle.h"

#include "gtx/rotate_vector.hpp"

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

CircleParticleEmitter::CircleParticleEmitter(float fRadius, float fAngle)
  : m_fRadius(fRadius)
  , m_fAngle(fAngle)
{
}

CircleParticleEmitter::~CircleParticleEmitter()
{
  m_fRadius = 0.0f;
  m_fAngle  = 0.0f;
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

void CircleParticleEmitter::SetRadius(float fRadius)
{
  m_fRadius = fRadius;
}

void CircleParticleEmitter::SetAngle(float fAngle)
{
  m_fAngle = fAngle;
}

void CircleParticleEmitter::EmitParticle(Particle* pParticle)
{
  // Calculate Start Position.
  glm::vec3 v3Pos = GetRandomPos();

  // Calculate Velocity.
  glm::vec3 v3Velocity = GetRandomVelocity();

  // Calculate Life Time.
  float fLifeTime = GetRandomLifeTime();

  // Fill Particle with initial Information.
  pParticle->m_fAge = 0.0f;
  pParticle->m_fLifeTime = fLifeTime;

  pParticle->m_v3Position = v3Pos;
  pParticle->m_v3Veclocity = v3Velocity;
}

glm::vec3 CircleParticleEmitter::GetRandomPos()
{
  float fLow   = -m_fRadius;
  float fHeigh =  m_fRadius;

  float fX = m_v3Position.x + fLow + static_cast<float>(rand()) / static_cast<float>(RAND_MAX / (fHeigh - fLow));
  float fY = m_v3Position.y + fLow + static_cast<float>(rand()) / static_cast<float>(RAND_MAX / (fHeigh - fLow));
  float fZ = m_v3Position.z;

  return glm::vec3(fX, fY, fZ);
}

float CircleParticleEmitter::GetRandomAngle()
{
  float fLow   = -m_fAngle;
  float fHeigh =  m_fAngle;

  return fLow + static_cast<float>(rand()) / static_cast<float>(RAND_MAX / (fHeigh - fLow));
}

glm::vec3 CircleParticleEmitter::GetRandomVelocity()
{
  float fAngle = GetRandomAngle();
  float fRotation = static_cast<float>(rand()) / static_cast<float>(RAND_MAX / 360.0f);
  glm::vec3 v3Velocity = glm::vec3(0.0f, -1.0f, 0.0f);

  v3Velocity = glm::rotateZ(v3Velocity, fAngle);
  v3Velocity = glm::rotateY(v3Velocity, fRotation);

  return v3Velocity;
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/