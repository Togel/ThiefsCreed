#version 420 core

layout (location = 0) in vec3 vertexPosition_modelspace;
layout (location = 1) in vec3 vertexNormal_modelspace;
layout (location = 2) in vec2 textureCoords_modelspace;


layout(std140) uniform Camera{
  mat4 matView;
  mat4 matProjection;
	mat4 matInvTransView;
};


out vec2 textureCoords;


void main(){

  vec4 v4PosCamS = (matView * vec4(vertexPosition_modelspace, 1.0));

  textureCoords = textureCoords_modelspace;
  
  gl_Position =  matProjection * v4PosCamS;

  //gl_Position = vec4(vertexPosition_modelspace, 1.0);
}
