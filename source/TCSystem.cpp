/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "TCSystem.h"
#include "Callbacks/UpdateTraversalCallback.h"
#include "Callbacks/ShadowMappingCallback.h"
#include "Callbacks/CameraBufferCallback.h"
#include "Callbacks/NoDepthBufferDrawCallback.h"
#include "Callbacks/ClothIntegrationCallback.h"
#include "ThreadDraw.h"
#include "ThreadGeometry.h"
#include "TC.h"
#include "GameManager.h"
#include <algorithm>
#include "RessourceManagement/ResourceCollection.h"
#include "CollisionHandling.h"
#include "ParticleSystem/ParticleEffect.h"
#include "ParticleSystem/CircleParticleEmitter.h"

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

TCSystem::TCSystem()
{
  Init();
}

TCSystem::~TCSystem()
{

  delete m_pDrawThread;

  delete m_pGameManager;

  for (ThreadGeometry* pThread : m_vecGeoThreads)
  {
    delete pThread;
  }
  m_vecGeoThreads.clear();
  
  m_vecThreads.clear();
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

void TCSystem::Init()
{
  //m_iNumOfThreads = std::min(GetNumOfCores(), std::stoi(pConfLoader.Get("NumOfThreads")));
  //m_iNumOfThreads = std::max(1, m_iNumOfThreads);
  m_iNumOfThreads = 2; // REPLACE
  
  tc::inf << "Using " << m_iNumOfThreads << " Cores." << tc::endl;
  
  int iNumOfGeoThreads = m_iNumOfThreads - 1;
  
// Init Scene Graph.
  m_pGameManager = new GameManager();

  // Create DrawThread with access to SceneGraph
  m_pDrawThread = new ThreadDraw(m_pGameManager);
  
  m_pGameManager->Init();
  m_pGameManager->GetResourceCollection()->LoadResources();
  m_pGameManager->GenerateMap(NUMBER_OF_MAP_SEGMENTS);
  m_pGameManager->AddPlayer();
  m_pGameManager->InsertAI(NUMBER_OF_AI);
  m_pGameManager->AddSkyBox();
  m_pGameManager->AddAIParticleEffects();
  
  // Initialize Particle Systems.
  for (ParticleEffect* pEffect : m_pGameManager->GetParticleEffects())
  {
    AddPostDrawCallback(pEffect);
  }
  

  // Initialize Collision Handling
  m_pCollisionHandler = new CollisionHandling();
  m_pGameManager->InitCollisionHandler(m_pCollisionHandler);

  //m_pGameManager->LoadTestScene();
  m_pCameraCallback = new CameraBufferCallback(m_pGameManager->GetCameraNode());

  for (int i = 0; i < iNumOfGeoThreads; ++i)
  {
    m_vecGeoThreads.emplace_back(new ThreadGeometry());
  }

  m_pUpdateCallback = new UpdateTraversalCallback(m_pGameManager->GetSceneGraphRoot(), m_pCollisionHandler);
  m_pShadowMappingCallback = new ShadowMappingCallback(m_pGameManager->GetLightManager()
                                                      , m_pGameManager->GetSceneGraphRoot()
                                                      , m_pGameManager->GetResourceCollection()
                                                      , m_pGameManager->GetCameraNode());
  m_pClothIntegrationCallback = new ClothIntegrationCallback(m_pGameManager->GetSceneGraphRoot());
  
  AddGeometryCallback(m_pUpdateCallback);
  AddPreDrawCallback(m_pShadowMappingCallback);
  AddPreDrawCallback(m_pCameraCallback); 
  AddPreDrawCallback(m_pClothIntegrationCallback);

  NoDepthBufferDrawCallback* no_depth_callback = new NoDepthBufferDrawCallback(m_pGameManager->GetSceneGraphRoot());
  AddPreDrawCallback(no_depth_callback);
  
  m_pDrawThread->SetupDeferredShading();
}


void TCSystem::Run()
{
  // Create auto Function for executing Geometry Threads.
  auto RunGeometryThread = [this](unsigned int iThreadNr)
  {
    while (m_bRun && (!m_bEndScreen || INFINITE_GAME))
    {
      m_vecGeoThreads[iThreadNr]->Run();
    }
  };

  // Create Threads.
  for (unsigned int iThreadNr = 0; iThreadNr < m_vecGeoThreads.size(); ++iThreadNr)
  {
    m_vecThreads.emplace_back(std::thread(RunGeometryThread, iThreadNr));
  }

  while (m_bRun && (!m_bEndScreen || INFINITE_GAME))
  {
    m_pDrawThread->Run();

    // Display end screen
    if (m_pGameManager->WonOrLost())
    {
      m_bEndScreen = true;
    }

    // Quit if close button is hit
    if (m_pDrawThread->WindowShouldClose())
    {
      Quit();
    }
  }

  // Display End Screen
  m_pDrawThread->InitEndScreen(m_pGameManager->GetResourceCollection(), m_pGameManager->PlayerHasWon());

  while(m_bRun)
  {
    m_pDrawThread->DrawEndScreen();

    // Quit if close button is hit
    if (m_pDrawThread->WindowShouldClose())
    {
      Quit();
    }
  }
}
  
void TCSystem::Quit()
{
  m_bRun = false;
}

int TCSystem::GetNumOfCores()
{
  unsigned int iNumOfCores = std::thread::hardware_concurrency();

  tc::inf << "Found " << iNumOfCores << " Cores." << tc::endl;

  return iNumOfCores;
}

void TCSystem::AddPreDrawCallback(Callback* pCallback)
{
  m_pDrawThread->AddPreDrawCallback(pCallback);
}

void TCSystem::AddPostDrawCallback(Callback* pCallback)
{
  m_pDrawThread->AddPostDrawCallback(pCallback);
}

void TCSystem::AddGeometryCallback(Callback* pCallback)
{
  // Determine in which Geometry Thread the Callback should be inserted.
  ThreadGeometry* pGeoThread = nullptr;
  float fMin = std::numeric_limits < float >::max();

  if (m_vecGeoThreads.size() == 0)
  {
    m_pDrawThread->AddPreDrawCallback(pCallback);
    tc::inf << "There is no Geometry Thread. Adding Callback as Pre-Draw-Callback to the Draw Thread." << tc::endl;
  }

  for (ThreadGeometry* pThread : m_vecGeoThreads)
  {
    float fDur = pThread->CalcDuration();

    if (fDur < fMin)
    {
      fMin = fDur;
      pGeoThread = pThread;
    }
  }

  // Error Handling
  if (pGeoThread == nullptr)
  {
    tc::err << "TCSystem::AddGeometryCallback -> No Geometry Thread found!" << tc::endl;
    return;
  }

  pGeoThread->AddCallback(pCallback);
}

bool TCSystem::DeleteCallback(Callback* pCallback)
{
  // Search Callback in Pre-Draw Callbacks.
  for (Callback* pDrawCallback : m_pDrawThread->GetPreDrawCallbacks())
  {
    if (pDrawCallback == pCallback)
    {
      m_pDrawThread->DeleteCallback(pDrawCallback);
      return true;
    }
  }

  // Search Callback in Post-Draw Callbacks.
  for (Callback* pDrawCallback : m_pDrawThread->GetPostDrawCallbacks())
  {
    if (pDrawCallback == pCallback)
    {
      m_pDrawThread->DeleteCallback(pDrawCallback);
      return true;
    }
  }

  // Search Callback in Geometry Callbacks.
  for (ThreadGeometry* pThread : m_vecGeoThreads)
  {
    for (Callback* pGeoCallback : pThread->GetCallbacks())
    {
      if (pGeoCallback == pCallback)
      {
        pThread->DeleteCallback(pGeoCallback);
        return true;
      }
    }
  }

  return false;
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/