#ifndef _CONSOLE_IO_
#define _CONSOLE_IO_

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include <iostream>
#include <sstream>
#include <map>
//#include <string>

#ifdef WIN32
  // Windows
  #include <windows.h>
#endif

#include "glm.hpp"

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/
#define inf inf()
#define war war()
#define err err()
#define crs crs()

/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/


/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/

namespace tc
{

  /*
  * Transforms a Rotation Vector into a Rotation Matrix.
  * @param 'v3Rot' defines the Rotation.
  * @return Matrix defining Rotation.
  */
  glm::mat3 TransIntoRotMatrix(glm::vec3 v3Rot);

  /*
  / Struct storing Information about the Color Value on Windows and Linux.
  */
  struct ConsoleColor
  {
    ConsoleColor(int iWinColor, std::string sLinColor)
    {
      m_iWinColor = iWinColor;
      m_sLinColor = sLinColor;
    }

    int m_iWinColor = 0;
    std::string m_sLinColor = "";
  };

  /*
  / Color Definitions.
  */
  static ConsoleColor ColorBlackBlue   = ConsoleColor(9,   "\033[34m");
  static ConsoleColor ColorBlackWhite  = ConsoleColor(07,  "\033[0m");
  static ConsoleColor ColorBlackGreen  = ConsoleColor(10,  "\033[32m");
  static ConsoleColor ColorBlackRed    = ConsoleColor(12,  "\033[31m");
  static ConsoleColor ColorBlackYellow = ConsoleColor(14,  "\033[33m");
  static ConsoleColor ColorRedYellow   = ConsoleColor(206, "\033[33;31m");
  static ConsoleColor ColorYellowRed   = ConsoleColor(236, "\033[31;33m");

  /*
  / Set the Console Color (Background and Foreground) to a given value.
  / @param sColor Struct defining the Color for Windows and Linux.
  */
  void SetConsoleColor(ConsoleColor sColor);

  /*
  / This function is used to preserve that multiple Messages are printed at once.
  / @param bIsCritical 'true' iff we enter a Printing-State.
  */
  void ToggleCriticalSection(bool bIsCritical);


  /*
  / Defining a new Output Type with a Green Font.
  */
  struct IOInfo
  {
    template<typename T>
    IOInfo& operator << (T&& x)
    {
      SetConsoleColor(ColorBlackGreen);
      std::cout << x;
      SetConsoleColor(ColorBlackWhite);

      return *this;
    }
  };

  /*
  / Defining a new Output Type with a Yellow Font.
  */
  struct IOWarning
  {
    template<typename T>
    IOWarning& operator << (T&& x)
    {
      SetConsoleColor(ColorBlackYellow);
      std::cout << x;
      SetConsoleColor(ColorBlackWhite);

      return *this;
    }
  };

  /*
  / Defining a new Output Type with a Red Font and a Log-File.
  */
  struct IOError
  {
    template<typename T>
    IOError& operator << (T&& x)
    {
      SetConsoleColor(ColorBlackRed);
      std::cout << x;
      SetConsoleColor(ColorBlackWhite);

      return *this;
    };
  };

  /*
  / Defining a new O utput Type with a Red Font, a Yellow Background and a Log-File.
  */
  struct IOCrash
  {
    template<typename T>
    IOCrash& operator << (T&& x)
    {
      SetConsoleColor(ColorYellowRed);
      std::cout << x;
      SetConsoleColor(ColorBlackWhite);

      return *this;
    };
  };

  /*
  / Use tc::inf to print an Information Message.
  */
  IOInfo inf;

  /*
  / Use tc::war to print a Warning Message.
  */
  IOWarning war;
  
  /*
  / Use tc::err to print an Error Message.
  / In addition, the Error is stored in a Log-File.
  */
  IOError err;
  
  /*
  / Use tc::crs to print a Crash Message.
  / In addition, the Crash is stored in a Log-File.
  */
  IOCrash crs;
  
  /*
  / Use tc::endl to print an End of Line.
  */
  static const std::string endl = "\n";

  void LoadConfigFile(std::string sName);
  std::string GetConfigStr(std::string sName);
  int GetConfigInt(std::string sName);
  float GetConfigFlt(std::string sName);
  bool GetConfigBoo(std::string sName);
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
#endif
