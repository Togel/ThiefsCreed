set( RelativeDir "source/RessourceManagement" )
set( RelativeSourceGroup "SourceFiles\\RessourceManagement" )

set(Directories
  
)

set(DirFiles
  Geometry.cpp
  Geometry.h
  GeometryLoader.cpp
  GeometryLoader.h
  Mesh.cpp
  Mesh.h
  ModelLoader.cpp
  ModelLoader.h
  ResourceCollection.cpp
  ResourceCollection.h
  Shader.cpp
  Shader.h
  Texture.cpp
  Texture.h
  TextureLoader.cpp
  TextureLoader.h
  _SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${RelativeSourceGroup} FILES ${LocalSourceGroupFiles} )

foreach( subdir ${Directories})
	include("${RelativeDir}/${subdir}/_SourceFiles.cmake")
endforeach()

set( RelativeDir "source")