/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "ResourceCollection.h"
#include "Mesh.h"
#include "Texture.h"
#include "Shader.h"
#include "ModelLoader.h"
#include "TextureLoader.h"
#include "../TC.h"

#ifndef WIN32
#include <dirent.h>
#endif

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

ResourceCollection::ResourceCollection(GLuint iCamUniformBufferID)
{
  m_iCamUniformBufferID = iCamUniformBufferID;

  tc::inf << "Resource Collection has been created." << tc::endl;
}

ResourceCollection::~ResourceCollection()
{
  std::map<std::string, Mesh*>::iterator itMesh;
  for (itMesh = m_mapMeshes.begin(); itMesh != m_mapMeshes.end(); ++itMesh)
  {
    delete itMesh->second;
  }

  std::map<std::string, Texture*>::iterator itTexture;
  for (itTexture = m_mapTextures.begin(); itTexture != m_mapTextures.end(); ++itTexture)
  {
    delete itTexture->second;
  }

  std::map<std::string, Shader*>::iterator itShader;
  for (itShader = m_mapShaders.begin(); itShader != m_mapShaders.end(); ++itShader)
  {
    delete itShader->second;
  }

  m_mapMeshes.clear();
  m_mapTextures.clear();
  m_mapShaders.clear();
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

Mesh* ResourceCollection::GetMesh(std::string sName)
{
  if (m_mapMeshes.find(sName) == m_mapMeshes.end())
  {
    tc::err << "Could not find Mesh with Name: " << sName << tc::endl;
    return nullptr;
  }

  return m_mapMeshes[sName];
}

Texture* ResourceCollection::GetTexture(std::string sName)
{
  if (m_mapTextures.find(sName) == m_mapTextures.end())
  {
    tc::err << "Could not find Texture with Name: " << sName << tc::endl;
    return nullptr;
  }

  return m_mapTextures[sName];
}

Shader* ResourceCollection::GetShader(std::string sName)
{
  if (m_mapShaders.find(sName) == m_mapShaders.end())
  {
    tc::err << "Could not find Shader with Name: " << sName << tc::endl;
    return nullptr;
  }

  return m_mapShaders[sName];
}

void ResourceCollection::LoadMesh(std::string sPath)
{
  ModelLoader oLoader;

  std::string sName = GetNameFromPath(sPath, "obj");

  Mesh* pMesh = oLoader.loadMeshFromFile(sPath);

  m_mapMeshes[sName] = pMesh;

  tc::inf << "Inserted Mesh: " << sName << tc::endl;
}

void ResourceCollection::LoadTexture(std::string sPath)
{
  TextureLoader oLoader;

  std::string sName = GetNameFromPath(sPath, "dds");

  int iTexID = static_cast<int>(m_mapTextures.size());

  Texture* pTexture = oLoader.loadTextureFromFile(sPath);

  m_mapTextures[sName] = pTexture;

  tc::inf << "Inserted Texture " << sName << tc::endl;
}

void ResourceCollection::LoadShader(std::string sPath)
{
  tc::err << "ResourceCollection::LoadShader -> Not implemented, yet." << tc::endl;
}

void ResourceCollection::LoadResources()
{
  std::vector<std::string> vecMeshes;
  std::vector<std::string> vecTextures;
  std::vector<std::string> vecShaders;

  GetFilesInDirectory(vecMeshes, "./Resources/Meshes/MapSegments", "obj");
  GetFilesInDirectory(vecMeshes, "./Resources/Meshes/AI", "obj");
  GetFilesInDirectory(vecMeshes, "./Resources/Meshes/Skybox", "obj");
  GetFilesInDirectory(vecMeshes, "./Resources/Meshes/ScreenSizedQuad", "obj");
  GetFilesInDirectory(vecTextures, "./Resources/Textures/MapSegments", "dds");
  GetFilesInDirectory(vecTextures, "./Resources/Textures/AI", "dds");
  GetFilesInDirectory(vecTextures, "./Resources/Textures", "dds");


  // Load Meshes
  for (std::string sFile : vecMeshes)
  {
    LoadMesh(sFile);
  }

  // Load Textures
  for (std::string sFile : vecTextures)
  {
    LoadTexture(sFile);
  }

  // Load Shaders
  for (std::string sFile : vecShaders)
  {
    LoadShader(sFile);
  }

  InitCubeMapTextures();

  InitCustomShader();
}

void ResourceCollection::GetFilesInDirectory(std::vector<std::string>& vecFiles, const std::string& sDirectory, const std::string sFileType)
{
#ifdef WIN32 // WINDOWS
  HANDLE hDirectory;
  WIN32_FIND_DATA dFileData;

  if ((hDirectory = FindFirstFile((sDirectory + "/*").c_str(), &dFileData)) == INVALID_HANDLE_VALUE)
    return; /* No files found */

  do 
  {
    // Construct the File Name.
    const std::string sFileName = dFileData.cFileName;
    const std::string sPath     = sDirectory + "/" + sFileName;

    const bool bIsDirectory = (dFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0;

    if (sFileName[0] == '.')
      continue;

    if (bIsDirectory)
      continue;

    // Check for correct File Type.
    std::string sType = "";
    size_t sPos = sPath.find_last_of(".");
    
    if (sPos != std::string::npos)
      sType = sPath.substr(sPos + 1);

    if(sType == sFileType)
      vecFiles.push_back(sPath);

  } while (FindNextFile(hDirectory, &dFileData));

  FindClose(hDirectory);

#else // LINUX

  DIR* hDirectory;
  class dirent* ent;

  // dirent types
  unsigned char dirent_folder =0x4;
  unsigned char dirent_file =0x8;

  // open directory
  hDirectory = opendir(sDirectory.c_str());
  if (hDirectory == NULL) return;

  // read all elements in the directory
  while ((ent = readdir(hDirectory)) != NULL) {
    // element path
    const std::string sEntryName = ent->d_name;
    const std::string sPath = sDirectory + "/" + sEntryName;

    // skip hidden files
    if (sEntryName[0] == '.')
      continue;

    // file element
    if ( ent->d_type == dirent_file )
    {
      // Check for correct File Type.
      std::string sType = "";
      size_t sPos = sPath.find_last_of(".");

      if (sPos != std::string::npos)
        sType = sPath.substr(sPos + 1);

      if(sType == sFileType)
    vecFiles.push_back(sPath);
  }

    // folder element, recursion can be added if desired
    /*
    else if ( ent->d_type == dirent_folder )
    {
      // recursive call
      GetFilesInDirectory( vecFiles, sPath, sFileType );
    }
    */
  }
  closedir(hDirectory);
#endif
}

std::string ResourceCollection::GetNameFromPath(const std::string sPath, const std::string sType)
{
  std::string sName = "";
  size_t sPos1 = sPath.find_last_of("/");

  if (sPos1 != std::string::npos)
  {
    sName = sPath.substr(sPos1 + 1);

    size_t sPos2 = sName.find_last_of(".");
    
    if (sPos2 != std::string::npos)
    {
      sName = sName.substr(0, sPos2);
    }
  }
    
  return sName;
}

void ResourceCollection::InitCustomShader()
{
  if (m_iCamUniformBufferID == 0)
  {
    tc::err << "ResourceCollection::InitCustomShader -> Could not create Shaders, since there is no Uniform Buffer, yet." << tc::endl;
    return;
  }

  // Deferred Shading Geometry Pass
  Shader* pDeferredGeometryShader = new Shader(m_iCamUniformBufferID);
  pDeferredGeometryShader->InitFromFiles("./Resources/Shaders/DeferredShading/geometry_pass-vertex.glsl", "./Resources/Shaders/DeferredShading/geometry_pass-fragment.glsl");
  pDeferredGeometryShader->Link();
  m_mapShaders["DeferredGeometryShader"] = pDeferredGeometryShader;

  // End Screen Shader
  Shader* pEndScreenShader = new Shader(m_iCamUniformBufferID);
  pEndScreenShader->InitFromFiles("./Resources/Shaders/EndScreen/EndScreen-Vertex.glsl", "./Resources/Shaders/EndScreen/EndScreen-Fragment.glsl");
  pEndScreenShader->Link();
  m_mapShaders["EndScreenShader"] = pEndScreenShader;

  // SkyBox Shader
  Shader* pSkyBoxShader = new Shader(m_iCamUniformBufferID);
  pSkyBoxShader->InitFromFiles("./Resources/Shaders/Skybox/SkyBoxShader-Vertex.glsl", "./Resources/Shaders/Skybox/SkyBoxShader-Fragment.glsl");
  pSkyBoxShader->Link();
  m_mapShaders["SkyBoxShader"] = pSkyBoxShader;

  // Particle Shader
  Shader* pParticleShader = new Shader(m_iCamUniformBufferID);
  pParticleShader->InitFromFiles("./Resources/Shaders/Particle/Particle-Vertex.glsl", "./Resources/Shaders/Particle/Particle-Fragment.glsl");
  pParticleShader->Link();
  m_mapShaders["ParticleShader"] = pParticleShader;

  // Shadow Mapping Shader
  Shader* pShadowMappingShader = new Shader(m_iCamUniformBufferID);
  pShadowMappingShader->InitFromFiles("./Resources/Shaders/ShadowMapping/ShadowMapping-Vertex.glsl","./Resources/Shaders/ShadowMapping/ShadowMapping-Fragment.glsl");
  pShadowMappingShader->Link();
  m_mapShaders["ShadowMappingShader"] = pShadowMappingShader;

  // ClothIntegration Shader
  Shader* pClothIntegrationShader = new Shader(m_iCamUniformBufferID);
  pClothIntegrationShader->InitFromFiles("./Resources/Shaders/ClothShaders/ClothIntegration-Vertex.glsl", "./Resources/Shaders/ClothShaders/ClothIntegration-Fragment.glsl");
  static const char * tf_varyings[] =
  {
    "tf_position_mass",
    "tf_velocity",
    "tf_normal"
  };

  glTransformFeedbackVaryings(pClothIntegrationShader->GetProgram(), 3, tf_varyings, GL_SEPARATE_ATTRIBS);
  pClothIntegrationShader->Link();
  m_mapShaders["ClothIntegrationShader"] = pClothIntegrationShader;

}

void ResourceCollection::InitCubeMapTextures()
{
  TextureLoader tl;

  // SkyBox CubeMap
  Texture* skyBoxTexture = tl.loadCubeMapFromDDSFile("./Resources/Textures/Skybox/Skybox_Top.dds",
                                                     "./Resources/Textures/Skybox/Skybox_Bottom.dds",
                                                     "./Resources/Textures/Skybox/Skybox_Front.dds",
                                                     "./Resources/Textures/Skybox/Skybox_Back.dds",
                                                     "./Resources/Textures/Skybox/Skybox_Left.dds",
                                                     "./Resources/Textures/Skybox/Skybox_Right.dds");
  m_mapTextures["Skybox"] = skyBoxTexture;
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
