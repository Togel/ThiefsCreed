/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "ThreadGeometry.h"
#include "Callbacks/Callback.h"
#include "Timer.h"

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

ThreadGeometry::ThreadGeometry()
  :m_pTimer(new Timer())
{
}
  
ThreadGeometry::~ThreadGeometry()
{
  for (Callback* pCallback : m_vecCallbacks)
  {
    delete pCallback;
  }
  m_vecCallbacks.clear();

  delete m_pTimer;
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

void ThreadGeometry::Run()
{
  m_pTimer->Update();

  for (Callback* pCallback : m_vecCallbacks)
    pCallback->Execute();

  m_fDuration = m_pTimer->GetElapsedTime();
}

std::vector<Callback*>& ThreadGeometry::GetCallbacks()
{
  return m_vecCallbacks;
}

void ThreadGeometry::AddCallback(Callback* pCallback)
{
  m_vecCallbacks.push_back(pCallback);
}

bool ThreadGeometry::DeleteCallback(Callback* pCallback)
{
  // Search the Callback in the Vector and delete it.
  for (std::vector<Callback*>::iterator it = m_vecCallbacks.begin(); it != m_vecCallbacks.end(); ++it)
  {
    if ((*it) == pCallback)
    {
      m_vecCallbacks.erase(it);
      delete (*it);
      
      return true;
    }
  }

  return false;
}

float ThreadGeometry::CalcDuration()
{
  return m_fDuration;
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/