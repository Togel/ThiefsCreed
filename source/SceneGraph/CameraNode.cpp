#include "CameraNode.h"
/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/

#include <gtc/matrix_transform.hpp>
#include <gtx/projection.hpp>

#include "../TC.h"

#include "Node.h"
#include "TransformNode.h"

#include <cstring>

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/
CameraNode::CameraNode()
{
  glGenBuffers(1, &m_gluiUniformBufferID);
}

CameraNode::~CameraNode()
{
  glDeleteBuffers(1, &m_gluiUniformBufferID);
}

glm::mat4 CameraNode::GetViewMatrix()
{
  glm::mat4 ModelMatrix = glm::mat4();
  Node* pParent = GetParent();

  while (pParent != nullptr)
  {
    if (pParent->GetNodeType() == "TransformNode"
      || pParent->GetNodeType() == "CameraTransformNode")
    {
      TransformNode* pTransParent = dynamic_cast<TransformNode*>(pParent);
      ModelMatrix = ModelMatrix * pTransParent->GetTransformation();

    }
    pParent = pParent->GetParent();
  }
  glm::vec4 translation = ModelMatrix[3];
  glm::vec3 v3Transl(translation[0], translation[1], translation[2]);
  glm::vec4 v4ViewDir = ModelMatrix * glm::vec4(0.0, 0.0, -1.0, 0.0);
  glm::vec4 v4UpDir = ModelMatrix * glm::vec4(0.0, 1.0, 0.0, 0.0);

  glm::mat4 tmpView = glm::lookAt(v3Transl,
    v3Transl + glm::vec3(v4ViewDir[0], v4ViewDir[1], v4ViewDir[2]),
    glm::vec3(v4UpDir[0], v4UpDir[1], v4UpDir[2]));

  return tmpView;
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/
void CameraNode::FillBuffer()
{
  glm::mat4 ModelMatrix = glm::mat4();
  Node* pParent = GetParent();

  while(pParent != nullptr)
  {
    if(pParent->GetNodeType() == "TransformNode")
    {
      TransformNode* pTransParent = dynamic_cast<TransformNode*>(pParent);
      ModelMatrix =  ModelMatrix * pTransParent->GetTransformation();

    }
    pParent = pParent->GetParent();
  }


  glm::vec4 translation = ModelMatrix[3];
  glm::vec3 v3Transl(translation[0], translation[1], translation[2]);
  glm::vec4 v4ViewDir = ModelMatrix * glm::vec4(0.0, 0.0, -1.0, 0.0);
  glm::vec4 v4UpDir = ModelMatrix * glm::vec4(0.0, 1.0, 0.0, 0.0);


  glm::mat4 tmpView = glm::lookAt(v3Transl,
                                  v3Transl + glm::vec3(v4ViewDir[0], v4ViewDir[1], v4ViewDir[2]),
                                  glm::vec3(v4UpDir[0], v4UpDir[1], v4UpDir[2]));


  //tmpView = tmpView._inverse();

  glm::mat4 inverseTransposedView = glm::transpose(glm::inverse(tmpView));

  GLfloat buffer[48];

  memcpy(buffer, &tmpView[0][0], 16 * sizeof(float));

  memcpy(buffer + 16, &m_matProjection[0][0], 16 * sizeof(float));

  memcpy(buffer + 32, &inverseTransposedView[0][0], 16 * sizeof(float));

  glBindBuffer(GL_UNIFORM_BUFFER, m_gluiUniformBufferID);
  glBufferData(GL_UNIFORM_BUFFER, 48 * sizeof(float), buffer, GL_DYNAMIC_DRAW);
}

void CameraNode::SetView(glm::vec3 eye, glm::vec3 viewDir, glm::vec3 up)
{

  m_matCamTransform = glm::lookAt(eye, eye + viewDir, up);

}

void CameraNode::SetProjective(float fovy, float aspect, float zNear, float zFar)
{
  m_matProjection = glm::perspective(fovy, aspect, zNear, zFar);
}

GLuint CameraNode::GetUniformBufferID()
{
  return m_gluiUniformBufferID;
}


/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
