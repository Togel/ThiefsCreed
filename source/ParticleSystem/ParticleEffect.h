#ifndef _PARTICLE_EFFECT_
#define _PARTICLE_EFFECT_

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include <vector>

#include "GL/glew.h"
#include "glm.hpp"
#include "../Callbacks/Callback.h"

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/
class Shader;
class TransformNode;
class CameraTransformNode;
class Texture;
class ParticleEmitter;
struct Particle;
struct ParticleVertex;

/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/

class ParticleEffect : public Callback
{
public:

  /*
  * Constructor.
  */
  ParticleEffect(Shader* pShader, TransformNode* pEffectTrans, CameraTransformNode* pTransNode, ParticleEmitter* pEmitter, const unsigned int iNumOfParticles, Texture* pTexture, float fInitSize, glm::vec3 v3Force);

  /*
  * Destructor.
  */
  virtual ~ParticleEffect();

  /*
  * Updates all Particles of the Effect.
  */
  void HandleEvents(Timer* pTimer);

  /*
  */
  void UpdateVertexBuffer();

  /*
  * Multi-Draws the Particles.
  */
  void Render();

  /*
  * Emits Particles.
  * @param 'fTime' defines the Time since the last Call.
  */
  void EmitParticles(float fTime);

  /*
  * Collects all dead Particles and saves them in the corresponding Vector.
  */
  void CollectDeadParticles();

  /*
  * Redefines the Size of the Particles.
  * @param 'fSize' defines the new Size of the Particles.
  */
  void SetInitalSize(float fSize);

protected:
  /*
  * Emits a Particle in the Scene.
  * @param 'pParticle' Pointer on the Particle which should be emitted.
  */
  virtual void EmitParticle(Particle* pParticle);

  /*
  * Resize the Particle Vertex Buffer.
  * @param 'iParticles' defines the new amount of Particles which get rendered.
  */
  void ResizePVB(unsigned int iParticles);

  /*
  * Age the alive Particles computing new Position, Velocity, Size, etc.
  * @param 'fTime' defines the Time since the last Call.
  */
  virtual void AgeParticles(float fTime);

  /*
  * Initializes the Particle Effect.
  */
  void Init();

  /*
  * Uploads the Vertices to the Graphics Card.
  */
  void Upload();

private:
  GLuint m_iVAO = 0; // VAO
  GLuint m_iVBO = 0; // VBO

  Shader* m_pShader = nullptr;

  CameraTransformNode* m_pCamTransNode = nullptr;
  TransformNode* m_pEffectTransNode = nullptr;

  ParticleEmitter* m_pParticleEmitter = nullptr;

  Texture* m_pTexture = nullptr;

  float m_fInitSize = 0.0f;

  glm::vec3 m_v3Force = glm::vec3(0.0f);

  unsigned int m_iNumOfParticles = 0;

  std::vector<Particle*> m_vecDeadParticles;
  std::vector<Particle*> m_vecAliveParticles;
  std::vector<ParticleVertex> m_vecParticleVertexBuffer;
};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
#endif