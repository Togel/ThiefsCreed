#ifndef DrawableNode_h_
#define DrawableNode_h_

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "Node.h"
#include <glm.hpp>

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/
class Shader;


/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class DrawableNode : public Node
{
public:
	DrawableNode(Shader* pShader);
	virtual ~DrawableNode();

	/*
	* Returns a string used to determine the type of the node
	* To work properly this has to be implemented in all inheriting nodetypes
	* @return string that encodes type of the node
	*/
	std::string GetNodeType() override;

	/*
	* Returns if this node has some transparent drawings
	* @return 'true' iff the node wants to draw transparent
	*/
	virtual bool GetIsTransparent();

	/*
	* Sets if the node wants to draw transparent
	* @param whether the node wants to draw transparent
	*/
	virtual void SetIsTransparent(bool bTransparent);

	/*
	* Gets called by the draw thread in each frame
	* Override in inheriting nodetypes to implement drawing
	* @param matrix that stores all transformations of parents
	*/
	virtual void DrawOpaque(glm::mat4 matTransformation);

	/*
	* Gets called by the draw thread in each frame
	* Override in inheriting nodetypes to implement drawing
	* @param matrix that stores all transformations of parents
	*/
	virtual void DrawTransparent(glm::mat4 matTransformation);

  /*
  * Gets called by the draw thread in each frame
  * Override in inheriting nodetypes to implement drawing
  * @param matrix that stores all transformations of parents
  */
  virtual void DrawWithoutDepthBuffer(glm::mat4 matTransformation);

  virtual void SetShader(Shader* pShader);

  virtual Shader* GetShader();

protected:
  
  Shader* m_pShader;


private:

	bool m_bTransparent;

};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
#endif
