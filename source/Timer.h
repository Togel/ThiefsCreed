#ifndef _TIMER_
#define _TIMER_

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include <chrono>

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/


/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/

class Timer
{
public:
  /*
  * Constructor.
  */
  Timer();

  /*
  * Destructor.
  */
  ~Timer();

  /*
  * Update the Timer.
  */
  void Update();

  /*
  * Returns the Time which has elapsed since the last Call of the Callback in Milliseconds.
  * @return the Time which has elapsed since the last Call of the Callback in Milliseconds.
  */
  float GetElapsedTime();
  
  /*
   * Returns the time passed since the last updat, without resetting the internally stored time.
   * @return The time passed since the last updat.
   */
  float GetElapsedTime_withoutUpdate();

private:
  std::chrono::high_resolution_clock::time_point m_tLastTime;

  float m_fElapsedTime = 0.0f;
};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
#endif