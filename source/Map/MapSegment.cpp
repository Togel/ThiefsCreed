/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MapSegment.h"

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

MapSegment::MapSegment(int iID, glm::vec2 v2Pos)
  : m_iID(iID)
{
  m_mNeighbors[Direction::North] = nullptr;
  m_mNeighbors[Direction::South] = nullptr;
  m_mNeighbors[Direction::West] = nullptr;
  m_mNeighbors[Direction::East] = nullptr;

  m_v2Position = v2Pos;
}

MapSegment::~MapSegment()
{
  m_mNeighbors.clear();
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

void MapSegment::SetNeighbor(Direction eDir, MapSegment* pNeighbor)
{
  m_mNeighbors[eDir] = pNeighbor;
}

MapSegment* MapSegment::GetNeighbor(Direction eDir)
{
  std::map<Direction, MapSegment*>::iterator it = m_mNeighbors.find(eDir);

  if (it != m_mNeighbors.end())
    return m_mNeighbors[eDir];
  else
    return nullptr;
}

glm::vec2 MapSegment::GetPosition()
{
  return m_v2Position;
}

void MapSegment::SetFinal(bool bFinal)
{
  m_bIsFinal = bFinal;
}

bool MapSegment::IsFinal()
{
  return m_bIsFinal;
}

std::vector<MapSegment*> MapSegment::GetNeighbors()
{
  std::vector<MapSegment*> vecResult;

  MapSegment* pNorth = GetNeighbor(Direction::North);
  MapSegment* pSouth = GetNeighbor(Direction::South);
  MapSegment* pWest = GetNeighbor(Direction::West);
  MapSegment* pEast = GetNeighbor(Direction::East);

  if (pNorth != nullptr)
    vecResult.push_back(pNorth);
  if (pSouth != nullptr)
    vecResult.push_back(pSouth);
  if (pWest != nullptr)
    vecResult.push_back(pWest);
  if (pEast != nullptr)
    vecResult.push_back(pEast);

  return vecResult;
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/