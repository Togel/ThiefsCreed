#ifndef _NAVIGATION_
#define _NAVIGATION_

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "Callback.h"
#include <vector>
#include "GL/glew.h"

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/
struct GLFWwindow;

/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/

class NavigationKey : public Callback
{
public:
  /*
  * Constructor.
  */
  NavigationKey(GLuint iKey);

  /*
  * Destructor
  */
  ~NavigationKey();

  /*
  * Updates the status whether the Key has been pushed and calls the HandleEvents Function.
  * @param 'bPressed' defines whether the Key is pressed or not.
  */
  void ProcessKey(bool bPressed);

  /*
  * Overwrite this Function. This Function is called if the corresponding Key is pressed.
  */
  virtual void Pressed();

  /*
  * Overwrite this Function. This Function is called if the corresponding Key is released.
  */
  virtual void Released();

  /*
  * Returns the Key Identifier.
  * @return The Key Identifier.
  */
  GLuint GetKey();

  /*
  * Returns whether the Key was pressed during the last Call.
  * @return 'true' if the Key was pressed during the last Call.
  */
  bool WasPressed();

private:
  GLuint m_iKey = -1;
  bool m_bPressed = false;
};



class Navigation : public Callback
{
public:
  /*
  * Constructor.
  */
  Navigation(GLFWwindow* pWindow);

  /*
  * Destructor
  */
  ~Navigation();

  /*
  * Define the Behavior of the Callback.
  */
  void HandleEvents(Timer* pTimer);

private:
  GLFWwindow* m_pWindow = nullptr;
  std::vector<NavigationKey*> m_vecKeys;
};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
#endif
