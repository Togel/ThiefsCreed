set( RelativeDir "source/DeferredShading" )
set( RelativeSourceGroup "SourceFiles\\DeferredShading" )

set(Directories
  
)

set(DirFiles
  DeferredShading.cpp
  DeferredShading.h
  GBuffer.cpp
  GBuffer.h
  _SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${RelativeSourceGroup} FILES ${LocalSourceGroupFiles} )

foreach( subdir ${Directories})
	include("${RelativeDir}/${subdir}/_SourceFiles.cmake")
endforeach()

set( RelativeDir "source")