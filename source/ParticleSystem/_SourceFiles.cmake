set( RelativeDir "source/ParticleSystem" )
set( RelativeSourceGroup "SourceFiles\\ParticleSystem" )

set(Directories
  
)

set(DirFiles
  CircleParticleEmitter.h
  CircleParticleEmitter.cpp
  Particle.h
  Particle.cpp
  ParticleEmitter.h
  ParticleEmitter.cpp
  ParticleEffect.h
  ParticleEffect.cpp
  _SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${RelativeSourceGroup} FILES ${LocalSourceGroupFiles} )

foreach( subdir ${Directories})
	include("${RelativeDir}/${subdir}/_SourceFiles.cmake")
endforeach()

set( RelativeDir "source")