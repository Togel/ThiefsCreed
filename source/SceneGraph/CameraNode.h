#ifndef _Camera_Node_
#define _Camera_Node_

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "Node.h"

#include <GL/glew.h>
#include <glm.hpp>
/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/
class Timer;

/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class CameraNode : public Node
{
public:
  CameraNode();
  virtual ~CameraNode();

  glm::mat4 GetViewMatrix();
  void FillBuffer();

  void SetView(glm::vec3 eye, glm::vec3 viewDir, glm::vec3 up);
  void SetProjective(float fovy, float aspect, float zNear, float zFar);

  GLuint GetUniformBufferID();
private:
  GLuint m_gluiUniformBufferID;

  glm::mat4 m_matCamTransform;
  glm::mat4 m_matProjection;
};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
#endif
