set( RelativeDir "Resources/Shaders/DeferredShading" )
set( RelativeSourceGroup "ResourceFiles\\Shaders\\DeferredShading" )

set(Directories
  
)

set(DirFiles
  geometry_pass-fragment.glsl
  geometry_pass-vertex.glsl
  LightingShader-fragment.glsl
  LightingShader-vertex.glsl
  _SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )
	
set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()

source_group( ${RelativeSourceGroup} FILES ${LocalSourceGroupFiles} )

foreach( subdir ${Directories})
	include("${RelativeDir}/${subdir}/_SourceFiles.cmake")
endforeach()

set( RelativeDir "Resources/Shaders")

