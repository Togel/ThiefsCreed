#ifndef _PARTICLE_EMITTER_
#define _PARTICLE_EMITTER_

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "glm.hpp"

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/
struct Particle;

/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/

class ParticleEmitter
{
public:
  /*
  * Constructor.
  */
  ParticleEmitter();

  /*
  * Destructor.
  */
  virtual ~ParticleEmitter();

  /*
  * Sets the minimal at maximal Life Time of the Particles.
  * @param 'fMin' defines the minimal Life Time of the Particles.
  * @param 'fMax' defines the minimal Life Time of the Particles.
  */
  void SetLifeTime(float fMin, float fMax);

  /*
  * Emits a Particle in the Scene.
  * @param 'pParticle' Pointer on the Particle which should be emitted.
  */
  virtual void EmitParticle(Particle* pParticle);

  /*
  * Returns the average Life Time of a Particle.
  * @return The average Life Time of a Particle.
  */
  float GetAverageLifeTime();

  /*
  * Redefines the Position of the Emitter.
  * @param 'v3Pos' defines the new Position of the Emitter.
  */
  void SetPosition(glm::vec3 v3Pos);

protected:
  /*
  * Returns a random Life Time for the Particle inside the Conditions.
  * @return A random Life Time for the Particle.
  */
  float GetRandomLifeTime();

  glm::vec3 m_v3Position = glm::vec3(0.0f, 0.0f, 0.0f);

private:
  float m_fParticleMinLifeTime = 0.0f;
  float m_fParticleMaxLifeTime = 0.0f;
};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
#endif