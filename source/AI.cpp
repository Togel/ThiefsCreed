#define AI_HOVER_HEIGHT 2.8f
#define AI_SPEED 0.002f
#define AI_ROT_SPEED 0.10f
#define AI_HOVER_SPEED 0.002f

#define AI_LEG_ANIMATION_SPEED 0.01f
#define AI_LEG_MAX_ANGLE 15.0f

#define AI_LEFT_LEG_OFFSET    glm::vec3( 0.781f, -0.165490f,    0.0f)
#define AI_RIGHT_LEG_OFFSET   glm::vec3(-0.781f, -0.165490f,    0.0f)
#define AI_FRONT_LEG_OFFSET   glm::vec3(   0.0f, -0.165490f, -0.781f)
#define AI_BACK_LEG_OFFSET    glm::vec3(   0.0f, -0.165490f,  0.781f)
#define AI_LIGHT_OFFSET       glm::vec3(   0.0f,      0.35f,    2.4f)

#define AI_LEG_ROT_AXIS glm::vec3(0.0f, 0.0f, -1.0f)

#define AI_VIEW_CONE_LENGTH 5.0f
#define AI_VIEW_CONE_RATIO_OUTER 1.0f // side_length / front_length
#define AI_VIEW_CONE_RATIO_INNER 0.1f // side_length / front_length

#define AI_LIGHT_COLOR_NORMAL      glm::vec3(0.4, 0.4, 0.7)
#define AI_LIGHT_COLOR_CHASING     glm::vec3(0.6, 0.7, 0.0)
#define AI_LIGHT_COLOR_FREE_CHASE  glm::vec3(0.9, 0.4, 0.0)
#define AI_LIGHT_COLOR_CAUGHT      glm::vec3(1.0, 0.0, 0.0)

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "AI.h"
#include "Player.h"
#include "TC.h"
#include "Timer.h"
#include "GameManager.h"
#include "LightManager.h"
#include "SceneGraph/LightNode.h"
#include "Defines.h"

#include "Map/Map.h"

#include "./SceneGraph/TransformNode.h"
#include "./SceneGraph/MeshNode.h"
#include "./SceneGraph/RotatingMeshNode.h"
#include "./SceneGraph/PlayerNode.h"

#include "./RessourceManagement/ResourceCollection.h"
#include "./RessourceManagement/Mesh.h"
#include "./RessourceManagement/Shader.h"

#include <math.h>
#include <gtx/vector_angle.hpp>
#include <gtx/rotate_vector.hpp>


/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

AI::AI(GameManager* pManager, std::vector<MapSegment*> vecRoute, PlayerNode* pPlayer, LightManager* pLightManager, Map* pMap)
{
  for (MapSegment* pSegment : vecRoute)
  {
    m_vecRoute.push_back(pSegment);
  }

  m_pPlayer = pPlayer;
  m_pMap = pMap;

  m_pCurSegment = vecRoute.at(0);
  glm::vec2 v2Pos = vecRoute.at(0)->GetPosition();
  m_v3Position = glm::vec3(v2Pos.x, AI_HOVER_HEIGHT, v2Pos.y);

  Node* pRoot = pManager->GetSceneGraphRoot();
  ResourceCollection* pCollection = pManager->GetResourceCollection();
  PlaceInSceneGraph(pRoot, pCollection, pLightManager);

  this->Scale(AI_SCALING_FACTOR);

  this->SetTranslation(m_v3Position);
}

AI::~AI()
{

}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

void AI::Update(Timer* pTimer)
{
  // store for collision handling
  m_fLastDeltaT = pTimer->GetElapsedTime();

  Direction eDir;

  // Check if Player is spotted
  if (IsTargetInViewCone(m_pPlayer->getPlayerPosition(), false))
  {
    m_bMoveFreely = true;
    m_bChasing = true;

    m_bCollisisonWithAI = false;

    // set light color
    if (!m_bCaught)
      SetDroneLightColor(AI_LIGHT_COLOR_FREE_CHASE);

    MoveFreely(m_pPlayer->getPlayerPosition(), pTimer->GetElapsedTime(), true);

    // update variables
    UpdateCurrentSegment_FreeMove();
    UpdateCurrentDirection_FreeMove();
  }

  // player lost, move to some segment's center and continue as normal
  else if (m_bMoveFreely)
  {
    // have reached some segments center
    if (IsAtSegmentCenter())
    {
      m_bMoveFreely = false;

      m_bCollisisonWithAI = false;

      // set light color
      if (!m_bCaught && m_bChasing)
        SetDroneLightColor(AI_LIGHT_COLOR_CHASING);
    }

    // move to useful segment center
    else
    {
      glm::vec3 v3ReturnPoint;
      MapSegment* pReturnSeg = FreeMoveReturnPoint(v3ReturnPoint);
      MoveFreely(v3ReturnPoint, pTimer->GetElapsedTime());
      m_pLastKnownPos = pReturnSeg;
    }

    // update variables
    UpdateCurrentSegment_FreeMove();
    UpdateCurrentDirection_FreeMove();
  }


  if (!m_bMoveFreely)
  {

    // Check whether the Player is in front of the AI.
    if (IsPlayerInDir(m_eDirection)
        && IsViewingInTargetDirection())
    {
      MapSegment* pTarget = m_pPlayer->GetCurrentMapSegment();
      if (pTarget != nullptr)
      {
        m_pLastKnownPos = pTarget;
        eDir = FindShortestPath(pTarget);
        m_bChasing = true;

        // set light color
        if (!m_bCaught)
          SetDroneLightColor(AI_LIGHT_COLOR_CHASING);
      }

      else {
        // error, no map segment for player, default value
        eDir = m_eDirection;
      }
    }

    // When Chasing move to last known Position of Player.
    else if (m_bChasing)
    {
      // Move to last known Position
      if (m_pCurSegment != m_pLastKnownPos)
        eDir = FindShortestPath(m_pLastKnownPos);

      // Check whether the Player has turned.
      else if (m_pCurSegment == m_pLastKnownPos)
      {
        // Continue chasing when the Player is in any Direction.
        if (IsPlayerInDir(Direction::North))
          eDir = Direction::North;
        else if (IsPlayerInDir(Direction::South))
          eDir = Direction::South;
        else if (IsPlayerInDir(Direction::West))
          eDir = Direction::West;
        else if (IsPlayerInDir(Direction::East))
          eDir = Direction::East;

        // Return to Route if the Player has been lost.
        else
        {
          MapSegment* pTarget = m_vecRoute.at(m_iCurRoutePos);
          eDir = FindShortestPath(pTarget);
          m_bChasing = false;

          // set light color
          if (!m_bCaught)
            SetDroneLightColor(AI_LIGHT_COLOR_NORMAL);
        }
      }
    }

    // Continue Route
    else
    {
      MapSegment* pTarget = m_vecRoute.at(m_iCurRoutePos);
      eDir = FindShortestPath(pTarget);
    }

    // Move in Direction.
    Move(eDir, pTimer->GetElapsedTime());
  }
}


MapSegment* AI::GetSegment()
{
  return m_pCurSegment;
}

MeshNode* AI::GetCurrentMapMeshNode()
{
  // no map given
  if (m_pMap == nullptr)
  {
    tc::err << "AI has no map given, cannot return the current Map Segment Mesh Node." << tc::endl;
    return nullptr;
  }

  int xInd;
  int yInd;
  GetMapIndices(xInd, yInd);

  MeshNode* pMeshNode = m_pMap->getMeshNode(xInd, yInd);
  if (pMeshNode == nullptr)
  {
    tc::err << "AI: Could not return Mesh Node." << tc::endl;
  }

  return pMeshNode;
}


glm::vec3 AI::GetAiPosition()
{
  return m_v3Position;
}


void AI::CollisionWithPlayer(glm::vec3 v3Translation)
{
  Collision(v3Translation);

  // player has lost
  if (m_bChasing)
  {
    m_bCaught = true;

    // set light color
    SetDroneLightColor(AI_LIGHT_COLOR_CAUGHT);
  }
}


void AI::CollisionWithAI(glm::vec3 v3Translation, glm::vec3 v3PosOfOtherAI)
{
  m_bCollisisonWithAI = true;

  // check if two drones got stuck, i.e. if the drone was
  // pushed back in the opposite direction it is heading to
  glm::vec3 v3TransNorm = glm::normalize(v3Translation);
  float fDot = glm::dot(v3TransNorm, m_v3ViewDir);
  if (fDot < -0.1) // almost opposite directions
  {
    // store position of other AI for poper path finding next update call
    m_v3PosOfOtherAI = v3PosOfOtherAI;

    // needs sideways movement
    float fAIMoveDist = AI_SPEED * m_fLastDeltaT * -fDot * 0.5f; // more movement if collision is more problematic

    // check if it was pushed to the right
    glm::vec3 v3Right = glm::normalize( glm::cross(m_v3ViewDir, glm::vec3(0.0f, 1.0f, 0.0f)) );
    if (glm::dot(v3TransNorm, v3Right) >= 0)
    {
      // move to the right
      v3Translation += fAIMoveDist * v3Right;
    }
    else {
      // move to the left
      v3Translation -= fAIMoveDist * v3Right;
    }
  }

  // call general collision routine with updated tranlation vector
  Collision(v3Translation);
}


void AI::Collision(glm::vec3 v3Translation)
{
  // needs to get back on the track
  m_bMoveFreely = true;

  // update position
  m_v3Position += v3Translation;
  this->SetTranslation(m_v3Position);

  // update current segment
  UpdateCurrentSegment_FreeMove();
}


bool AI::CaughtPlayer()
{
  return m_bCaught;
}

void AI::Move(Direction eDir, float fDeltaT)
{
  // Hover
  Hover(fDeltaT);

  // Rotate
  if (!IsRotationNeeded(eDir, fDeltaT))
  {
    // Fix the View Vector.
    RepairViewDir();

    // Else Move.
    // Calculate Distance between AI and the next Map Segment Center.
    MapSegment* pNext = GetNextSegment();
    glm::vec2 v2Pos = glm::vec2(m_v3Position.x, m_v3Position.z);

    float fAIMoveDist = AI_SPEED * fDeltaT;
    float fDistToCenter = glm::distance(v2Pos, pNext->GetPosition());

    // Go in x.xX0000 Steps
    //float fTravelBy = floor(glm::min(fAIMoveDist, fDistToCenter) * 100.0f) / 100.0f;
    float fTravelBy = glm::min(fAIMoveDist, fDistToCenter);

    // Avoid Numerical Errors.
    if (fDistToCenter < 0.1f)
    {
      m_v3Position.x = pNext->GetPosition().x;
      m_v3Position.z = pNext->GetPosition().y;
    }
    // Update Position
    else if (m_eDirection == Direction::North)
      m_v3Position.z += -fTravelBy;
    else if (m_eDirection == Direction::South)
      m_v3Position.z += fTravelBy;
    else if (m_eDirection == Direction::West)
      m_v3Position.x += -fTravelBy;
    else if (m_eDirection == Direction::East)
      m_v3Position.x += fTravelBy;
  }

  // Apply Transformation.
  this->SetTranslation(m_v3Position);

  // Update current Map Segment of the AI.
  // Compare Distance between current Segment Position and 'm_v3Pos'.
  // Switch if > SegmentWidth / 2
  glm::vec2 v2SegmentCenter = m_pCurSegment->GetPosition();
  glm::vec3 v3SegmentCenter = glm::vec3(v2SegmentCenter.x, m_v3Position.y, v2SegmentCenter.y);
  float fDistFromCurCenter = glm::distance(m_v3Position, v3SegmentCenter);
  
  if (fDistFromCurCenter > 4.0f)
    tc::err << "Distance to Segment Center: " << fDistFromCurCenter << tc::endl;

  if (fDistFromCurCenter > SEGMENT_WIDTH / 2)
  {
    m_pCurSegment = m_pCurSegment->GetNeighbor(m_eDirection);
    if (m_pCurSegment == nullptr)
    {
      tc::err << "AI: Could not update current segment correctly, neighbor was nullptr." << tc::endl;
    }
  }
}


void AI::Hover(float fDeltaT)
{
  m_fHeight += (AI_HOVER_SPEED * fDeltaT);
  while (m_fHeight >= 360.0f) // Modulo
  {
    m_fHeight -= 360.0f;
  }

  m_v3Position.y = AI_HOVER_HEIGHT + 0.2f * glm::sin(m_fHeight);
}


MapSegment* AI::GetNextSegment()
{
  MapSegment* pNext = nullptr;

  if (m_eDirection == Direction::North)
  {
    // Go away from Current Center.
    if (m_v3Position.z <= m_pCurSegment->GetPosition().y)
      pNext = m_pCurSegment->GetNeighbor(Direction::North);
    // Go in Direction of Current Center
    else
      pNext = m_pCurSegment;
  }
  else if (m_eDirection == Direction::South)
  {
    // Go in Direction of Current Center
    if (m_v3Position.z < m_pCurSegment->GetPosition().y)
      pNext = m_pCurSegment;
    // Go away from Current Center.
    else
      pNext = m_pCurSegment->GetNeighbor(Direction::South);
  }
  else if (m_eDirection == Direction::West)
  {
    // Go away from Current Center.
    if (m_v3Position.x <= m_pCurSegment->GetPosition().x)
      pNext = m_pCurSegment->GetNeighbor(Direction::West);
    // Go in Direction of Current Center
    else
      pNext = m_pCurSegment;
  }
  else if (m_eDirection == Direction::East)
  {
    // Go in Direction of Current Center
    if (m_v3Position.x < m_pCurSegment->GetPosition().x)
      pNext = m_pCurSegment;
    // Go away from Current Center.
    else
      pNext = m_pCurSegment->GetNeighbor(Direction::East);
  }

  return pNext;
}


void AI::PlaceInSceneGraph(Node* pRoot, ResourceCollection* pCollection, LightManager* pLightManager)
{
  // Structure in SceneGraph -> R_N: Root Node, AIN: AI Node, T: TransformNode, M_N: MeshNode, R_M_N: RotatingMeshNode
  //              R_N
  //               |
  //              AIN
  //     __________|__________
  //    /   \      |      /   \
  //   T     T    M_N    T     T
  //   |     |           |     |
  // R_M_N R_M_N       R_M_N R_M_N

  // Load Meshes, Textures and Shader.
  Mesh* pCorpusMesh   = pCollection->GetMesh("AI_Corpus");
  Mesh* pLegMesh      = pCollection->GetMesh("AI_Leg");

  Texture* pCorpusTex = pCollection->GetTexture("AI_Corpus");
  Texture* pLegTex    = pCollection->GetTexture("AI_Leg");

  Shader* pShader = pCollection->GetShader("DeferredGeometryShader");

  // Create Nodes.
  TransformNode* pLegLTrans   = new TransformNode();
  TransformNode* pLegRTrans   = new TransformNode();
  TransformNode* pLegFTrans   = new TransformNode();
  TransformNode* pLegBTrans   = new TransformNode();
  
  MeshNode* pCorpusMeshNode = new MeshNode(pShader, pCorpusMesh, pCorpusTex);

  RotatingMeshNode* pLegLMeshNode = new RotatingMeshNode(AI_LEG_ROT_AXIS, AI_LEG_MAX_ANGLE, AI_LEG_ANIMATION_SPEED, pShader, pLegMesh, pLegTex);
  RotatingMeshNode* pLegRMeshNode = new RotatingMeshNode(AI_LEG_ROT_AXIS, AI_LEG_MAX_ANGLE, AI_LEG_ANIMATION_SPEED, pShader, pLegMesh, pLegTex);
  RotatingMeshNode* pLegFMeshNode = new RotatingMeshNode(AI_LEG_ROT_AXIS, AI_LEG_MAX_ANGLE, AI_LEG_ANIMATION_SPEED, pShader, pLegMesh, pLegTex);
  RotatingMeshNode* pLegBMeshNode = new RotatingMeshNode(AI_LEG_ROT_AXIS, AI_LEG_MAX_ANGLE, AI_LEG_ANIMATION_SPEED, pShader, pLegMesh, pLegTex);

  // Rotate Nodes.
  pLegLTrans->Rotate(  0.0f, glm::vec3(0.0f, 1.0f, 0.0f));
  pLegRTrans->Rotate(180.0f, glm::vec3(0.0f, 1.0f, 0.0f));
  pLegFTrans->Rotate( 90.0f, glm::vec3(0.0f, 1.0f, 0.0f));
  pLegBTrans->Rotate(-90.0f, glm::vec3(0.0f, 1.0f, 0.0f));

  // Translate Nodes.
  pLegLTrans->Translate(AI_LEFT_LEG_OFFSET);
  pLegRTrans->Translate(AI_RIGHT_LEG_OFFSET);
  pLegFTrans->Translate(AI_FRONT_LEG_OFFSET);
  pLegBTrans->Translate(AI_BACK_LEG_OFFSET);
  
  // Connect Nodes.
  pLegLTrans->AddChild(pLegLMeshNode);
  pLegRTrans->AddChild(pLegRMeshNode);
  pLegFTrans->AddChild(pLegFMeshNode);
  pLegBTrans->AddChild(pLegBMeshNode);

  this->AddChild(pCorpusMeshNode);
  this->AddChild(pLegLTrans);
  this->AddChild(pLegRTrans);
  this->AddChild(pLegFTrans);
  this->AddChild(pLegBTrans);

  pRoot->AddChild(this);
  
 
  // add light
  m_pDroneLight = pLightManager->CreateLight();
  m_pDroneLight->SetLightAmbientCoefficient(0.0f);
  m_pDroneLight->SetLightAttenuation(1.0f);
  m_pDroneLight->SetLightConeAngle(15.0f);
  m_pDroneLight->SetLightIntensity(glm::vec3(0.4, 0.4, 0.7));
  m_pDroneLight->SetLightType(LightNode::SPOTLIGHT);
  
  TransformNode* pLightTrans   = new TransformNode();
  pLightTrans->Translate(AI_LIGHT_OFFSET);
  
  pLightTrans->AddChild(m_pDroneLight);
  this->AddChild(pLightTrans);
  
}


Direction AI::FindShortestPath(MapSegment* pTarget)
{

  std::vector<std::vector<MapSegment*>> vecPaths;
  std::vector<MapSegment*> vecPath;

  // Special Case: We are already inside the Segment.
  if (m_pCurSegment == pTarget)
  {
    return m_eDirection;
  }
    

  // Find Paths to the Target Segment.
  FindPaths(m_pCurSegment, pTarget, vecPaths, vecPath);
  FilterPaths(vecPaths, pTarget);

  // Find the shortest Path.
  if (vecPaths.size() > 0)
  {
    std::vector<MapSegment*> vecShortestPath = vecPaths.at(0);

    for (std::vector<MapSegment*>& vecPath : vecPaths)
    {
      if (vecPath.size() < vecShortestPath.size())
      {
        vecShortestPath = vecPath;
      }
    }

    // Check in which Direction the first Element of the shortest Path is.
    if (vecShortestPath.at(0) == m_pCurSegment->GetNeighbor(North))
      return North;
    else if (vecShortestPath.at(0) == m_pCurSegment->GetNeighbor(South))
      return South;
    else if (vecShortestPath.at(0) == m_pCurSegment->GetNeighbor(West))
      return West;
    else
      return East;
  }
  // There is no Path.
  else
  {
    tc::err << "AI::FindShortestPath -> There is no Path." << tc::endl;

    if (m_pCurSegment->GetNeighbor(Direction::North) != nullptr)
      return Direction::North;
    else if (m_pCurSegment->GetNeighbor(Direction::South) != nullptr)
      return Direction::South;
    else if (m_pCurSegment->GetNeighbor(Direction::West) != nullptr)
      return Direction::West;
    else
      return Direction::East;
  }
}


void AI::FindPaths(MapSegment* pCurPos, MapSegment* pTarget, std::vector<std::vector<MapSegment*>>& vecPaths, std::vector<MapSegment*>& vecPath)
{
  // Get Neighbors.
  MapSegment* pNorthNeighbor = pCurPos->GetNeighbor(Direction::North);
  MapSegment* pSouthNeighbor = pCurPos->GetNeighbor(Direction::South);
  MapSegment* pWestNeighbor  = pCurPos->GetNeighbor(Direction::West);
  MapSegment* pEastNeighbor  = pCurPos->GetNeighbor(Direction::East);
  
  std::vector<MapSegment*> vecNeighbors;
  vecNeighbors.push_back(pNorthNeighbor);
  vecNeighbors.push_back(pSouthNeighbor);
  vecNeighbors.push_back(pWestNeighbor);
  vecNeighbors.push_back(pEastNeighbor);

  // Check all Neighbors.
  for (MapSegment* pNeighbor : vecNeighbors)
  {
    if (pNeighbor != nullptr)
    {
      std::vector<MapSegment*> vecNewPath;

      // We discard a new Path if it is a Circle.
      bool bDiscard = false;

      // Generate and store new Path.
      for (MapSegment* pSegment : vecPath)
      {
        // We have found a Circle.
        if (pNeighbor == pSegment)
        {
          bDiscard = true;
          break;
        }
        vecNewPath.push_back(pSegment);
      }
      vecNewPath.push_back(pNeighbor);

      // Do not include any Circles i.e. if the Segment is Part of the Path we do not generate new Paths.
      if (!bDiscard)
      {
        // Add new Path to Result.
        vecPaths.push_back(vecNewPath);

        // Only further Recursion if we have NOT reached the Target.
        if (!(pNeighbor == pTarget))
          FindPaths(pNeighbor, pTarget, vecPaths, vecNewPath);
      }
    }
  }
}


void AI::FilterPaths(std::vector<std::vector<MapSegment*>>& vecPaths, MapSegment* pTarget)
{
  std::vector<std::vector<MapSegment*>> vecPathsToPlayers;

  // Check whether the Target Segment is reached.  
  for (std::vector<MapSegment*>& vecPath : vecPaths)
  {
    MapSegment* pLast = vecPath.back();

    if (pLast == pTarget)
    {
      vecPathsToPlayers.push_back(vecPath);
    }
  }

  // Refill Paths.
  vecPaths.clear();

  for (std::vector<MapSegment*>& vecSegments : vecPathsToPlayers)
  {
    vecPaths.push_back(vecSegments);
  }
}


bool AI::IsPlayerInDir(Direction eDir)
{
  if (m_pPlayer == nullptr)
  {
    tc::err << "AI::IsPlayerInDir -> AI does not know the Player." << tc::endl;
    return false;
  }

  MapSegment* pCurSegment = m_pCurSegment;

  // check current segment, player needs to be in front of AI then
  MapSegment* pPlayerSegment = m_pPlayer->GetCurrentMapSegment();
  if (pPlayerSegment != nullptr && pCurSegment == pPlayerSegment)
  {
    // check if player is in front of AI
    glm::vec3 v3AiPos = m_v3Position;
    v3AiPos.y = 0.0f;
    glm::vec3 v3PlayerPos = m_pPlayer->getPlayerPosition();
    v3PlayerPos.y = 0.0f;

    float fDot = glm::dot(v3PlayerPos - v3AiPos, m_v3ViewDir);
    return fDot > 0.0f;
  }

  // check all further segments in view direction
  bool bWallReached = false;

  while (!bWallReached)
  {
    pCurSegment = pCurSegment->GetNeighbor(eDir);

    MapSegment* pPlayerSegment = m_pPlayer->GetCurrentMapSegment();

    if (pPlayerSegment != nullptr && pCurSegment == pPlayerSegment)
    {
      // Do not look in Dead-Ends
      if (pCurSegment->GetNeighbors().size() <= 1)
        return false;
      else 
        return true;        
    }      
    else if (pCurSegment == nullptr)
      return false;
  }

  return false;
}


bool AI::IsRotationNeeded(Direction eDir, float fDeltaT)
{ 
  // Only Rotate when on Center Position
  if (!IsAtSegmentCenter())
  {
    return false;
  }

  else
  {
    m_eDirection = eDir;
    
    // If we reached a Control Point we need switch to the next one and re-compute the Direction.
    if (m_pCurSegment == m_vecRoute.at(m_iCurRoutePos))
    {
      size_t iRouteSize = m_vecRoute.size();

      m_iCurRoutePos++;
      m_iCurRoutePos = m_iCurRoutePos % iRouteSize;

      return true;
    }

    // Get Target Direction.
    glm::vec3 v3TargetDir;

    if (m_eDirection == Direction::North)
      v3TargetDir = glm::vec3(0.0f, 0.0f, -1.0f);
    else if (m_eDirection == Direction::South)
      v3TargetDir = glm::vec3(0.0f, 0.0f, 1.0f);
    else if (m_eDirection == Direction::West)
      v3TargetDir = glm::vec3(-1.0f, 0.0f, 0.0f);
    else if (m_eDirection == Direction::East)
      v3TargetDir = glm::vec3(1.0f, 0.0f, 0.0f);

    // Check whether the Angle between Target Direction and Current Direction is larger than 0
    float dot = glm::dot(m_v3ViewDir, v3TargetDir);
    if (dot < 0.99f) // Allow for precision errors, will be fixed in RepairViewDir()
    {
      RotateTowardsDirection(v3TargetDir, fDeltaT);
      return true;
    }
  }

  return false;  
}


bool AI::IsAtSegmentCenter()
{
  float fXIntOffset = glm::abs(m_v3Position.x);
  float fZIntOffset = glm::abs(m_v3Position.z);

  while (fXIntOffset >= 4.0f) // Modulo.
  {
    fXIntOffset -= 4.0f;
  }

  while (fZIntOffset >= 4.0f) // Modulo.
  {
    fZIntOffset -= 4.0f;
  }

  if (fXIntOffset <= 0.0001f && fZIntOffset <= 0.0001f)
  {
    // round to get a precise position
    m_v3Position.x = round(m_v3Position.x);
    m_v3Position.z = round(m_v3Position.z);
    return true;
  }

  return false;
}


void AI::RotateTowardsDirection(glm::vec3 v3TargetDir, float fDeltaT)
{
  v3TargetDir = glm::normalize(v3TargetDir);

  // Check whether the Angle between Target Direction and Current Direction is larger than 0
  float fAngle = glm::orientedAngle(m_v3ViewDir, v3TargetDir, glm::vec3(0.0f, 1.0f, 0.0f));
  float fAngleAbs = glm::abs(fAngle);

  if (fAngleAbs > 0.0f)
  {
    // Avoid Permanent Rotation
    if (fAngleAbs < 0.1f)
    {
      m_v3ViewDir = v3TargetDir;
      return;
    }

    // Calculate Angle.
    float fRotation = 0.0f;
    fRotation = glm::min(fAngleAbs, AI_ROT_SPEED * fDeltaT); // Do not Over-Shoot.

    // Handle Backward Rotation.
    if (fAngle < 0.0f)
      fRotation = -fRotation;

    // Rotate View Dir.
    m_v3ViewDir = glm::rotateY(m_v3ViewDir, fRotation);

    // numerical corrections
    m_v3ViewDir[1] = 0.0f;
    m_v3ViewDir = glm::normalize(m_v3ViewDir);

    // rotate geometry
    AdjustRotationMatrix();
  }
}


void AI::AdjustRotationMatrix()
{
  // calculate right vector
  glm::vec3 right = glm::cross(m_v3ViewDir, glm::vec3(0.0f, 1.0f, 0.0f));

  // correct the rotation
  glm::mat4 transform = this->GetTransformation();
  transform[0] = glm::vec4(right, 0.0f);
  transform[1] = glm::vec4(0.0f, 1.0f, 0.0f, 0.0f);
  transform[2] = glm::vec4(m_v3ViewDir, 0.0f);
  this->SetTransformation(transform);

  // scale again
  this->Scale(AI_SCALING_FACTOR);
}


void AI::RepairViewDir()
{
  float fX = m_v3ViewDir.x;
  float fY = m_v3ViewDir.y;
  float fZ = m_v3ViewDir.z;

  // Fix X.
  if (fX > 0.0f)
    if (fX > 0.5f)
      fX = 1.0f;
    else
      fX = 0.0f;
  else if (fX < 0.0f)
    if (fX < -0.5f)
      fX = -1.0f;
    else
      fX = 0.0f;

  // Fix Y.
  if (fY > 0.0f)
    if (fY > 0.5f)
      fY = 1.0f;
    else
      fY = 0.0f;
  else if (fY < 0.0f)
    if (fY < -0.5f)
      fY = -1.0f;
    else
      fY = 0.0f;

  // Fix Z.
  if (fZ > 0.0f)
    if (fZ > 0.5f)
      fZ = 1.0f;
    else
      fZ = 0.0f;
  else if (fZ < 0.0f)
    if (fZ < -0.5f)
      fZ = -1.0f;
    else
      fZ = 0.0f;

  // Apply Changes
  m_v3ViewDir.x = fX;
  m_v3ViewDir.y = fY;
  m_v3ViewDir.z = fZ;

  m_v3ViewDir = glm::normalize(m_v3ViewDir);

  AdjustRotationMatrix();
}


bool AI::IsTargetInViewCone(glm::vec3 v3TargetPos, bool bMovement)
{
  glm::vec2 v2AiPos(m_v3Position.x, m_v3Position.z);
  glm::vec2 v2TargetPos(v3TargetPos.x, v3TargetPos.z);

  // check if player is in distance
  if ( !bMovement && glm::distance(v2AiPos, v2TargetPos) > AI_VIEW_CONE_LENGTH )
  {
    return false;
  }

  // check if target is in front of the drone
  glm::vec2 v2AiViewDir(m_v3ViewDir.x, m_v3ViewDir.z);
  glm::vec2 v2Diff = v2TargetPos - v2AiPos;
  float fFrontDot = glm::dot(v2AiViewDir, v2Diff);
  if (fFrontDot < 0.0f)
  {
    return false;
  }

  // view cone width at target position
  float fSideLength = fFrontDot * AI_VIEW_CONE_RATIO_OUTER;
  if (bMovement)
  {
    fSideLength = fFrontDot * AI_VIEW_CONE_RATIO_INNER;
  }

  // check if target is within the cone angle
  glm::vec2 v2AiViewNormal(-v2AiViewDir[1], v2AiViewDir[0]);
  float fSideDot = glm::dot(v2AiViewNormal, v2Diff);
  if (std::abs(fSideDot) > fSideLength)
  {
    return false;
  }

  // further tests for spotting
  if (!bMovement)
  {
    // check if player is in a dead end, shall not be spotted there
    MapSegment* pPlayerSegment = m_pPlayer->GetCurrentMapSegment();
    if (pPlayerSegment == nullptr || pPlayerSegment->GetNeighbors().size() <= 1)
    {
      return false;
    }

    // check if player is in the hallway, visible by the drone, to prevent spotting through walls
    if (!IsPlayerInDir(Direction::South)
        && !IsPlayerInDir(Direction::North)
        && !IsPlayerInDir(Direction::East)
        && !IsPlayerInDir(Direction::West))
    {
      return false;
    }

    // check if the player is in the same segment and blocked by geometry (i.e. a corner)
    if (IsSightBlockedBySegmentGeometry())
    {
      return false;
    }
  }

  return true;
}


void AI::MoveFreely(glm::vec3 v3TargetPos, float fDeltaT, bool bStopInFront)
{
  // Hover
  Hover(fDeltaT);

  // vector from AI to player
  glm::vec3 v3Diff = v3TargetPos - m_v3Position;
  v3Diff.y = 0.0f; // no movement in y-direction

  // try to keep the target within the inner cone
  if (!IsTargetInViewCone(v3TargetPos, true))
  {
    // rotate towards player
    RotateTowardsDirection(v3Diff, fDeltaT);
  }

  // move towards target
  else
  {
    float fAIMoveDist = AI_SPEED * fDeltaT;
    float fDistToTarget = glm::length(v3Diff);

    // stop slightly before target, s.t. Collision Handling knows where the drone came from
    if (bStopInFront)
    {
      fDistToTarget -= 0.0001f;
    }

    float fTravelBy = glm::min(fAIMoveDist, fDistToTarget);
    // Update Position
    m_v3Position += fTravelBy * glm::normalize(v3Diff);

    // stop at precisely at the correct position
    if (!bStopInFront && fAIMoveDist >= fDistToTarget)
    {
      m_v3Position.x = v3TargetPos.x;
      m_v3Position.z = v3TargetPos.z;
    }
  }

  // Apply Transformation.
  this->SetTranslation(m_v3Position);
}

void AI::UpdateCurrentSegment_FreeMove()
{
  // no map given
  if (m_pMap == nullptr)
  {
    tc::err << "AI has no map given, cannot update Map Segment in free move mode." << tc::endl;
    return;
  }

  int xInd;
  int yInd;
  GetMapIndices(xInd, yInd);

  MapSegment* pCurrSeg = m_pMap->getMapSegment(xInd, yInd);
  if (pCurrSeg == nullptr)
  {
    tc::war << "AI: Could not update current Map Segment in Free Move Mode." << tc::endl;
  }

  m_pCurSegment = pCurrSeg;
}


void AI::UpdateCurrentDirection_FreeMove()
{
  glm::vec3 v3South(0.0f, 0.0f, 1.0f);
  glm::vec3 v3East(1.0f, 0.0f, 0.0f);

  float fDotSouth = glm::dot(m_v3ViewDir, v3South);
  float fDotEast = glm::dot(m_v3ViewDir, v3East);

  // North-South
  if (std::abs(fDotSouth) >= std::abs(fDotEast))
  {
    if (fDotSouth >= 0.0f)
    {
      m_eDirection = Direction::South;
    }
    else {
      m_eDirection = Direction::North;
    }
  }

  // East-West
  else {
    if (fDotEast >= 0.0f)
    {
      m_eDirection = Direction::East;
    }
    else {
      m_eDirection = Direction::West;
    }
  }
}


void AI::GetMapIndices(int& xInd, int& yInd)
{
  glm::vec2 v2Pos(m_v3Position.x, m_v3Position.z);

  // shift Drone's position, s.t. for position at outer corner of the map it will end up at (0,0)
  glm::vec2 shiftPos = v2Pos + glm::vec2(SEGMENT_WIDTH/2.0f, SEGMENT_WIDTH/2.0f);

  // x-index
  float xInd_ = shiftPos[0] / SEGMENT_WIDTH;
  xInd = int(xInd_);

  // y-index
  float yInd_ = shiftPos[1] / SEGMENT_WIDTH;
  yInd = int(yInd_);

  // test for extrem cases
  if (xInd < 0) xInd = 0;
  if (yInd < 0) yInd = 0;
  if (xInd >= MapWidth) xInd = MapWidth - 1;
  if (yInd >= MapHeight) yInd = MapHeight - 1;
}


MapSegment* AI::FreeMoveReturnPoint(glm::vec3& v3ReturnPoint)
{
  // check if the center of the current segment is in front of the drone
  glm::vec2 v2CurSegCenter = m_pCurSegment->GetPosition();
  glm::vec3 v3CurSegCenter(v2CurSegCenter[0], 0.0f, v2CurSegCenter[1]);

  glm::vec3 v3AIPos = m_v3Position;

  // in a collision case, chose return segment as being at the other
  // AI's position, to avoid circling around center
  if (m_bCollisisonWithAI)
  {
    // check if we have passed the other AI yet
    glm::vec3 v3ColDiff = m_v3PosOfOtherAI - m_v3Position;
    v3ColDiff.y  = 0.0;

    if (glm::dot(v3ColDiff, m_v3ViewDir) < 0.0f)
    {
      // passed
      m_bCollisisonWithAI = false;
    }
    else {
      // not passed yet
      v3AIPos = m_v3PosOfOtherAI;
    }
  }

  glm::vec3 v3Diff = v3CurSegCenter - v3AIPos;
  v3Diff.y = 0.0f; // not interested in y-direction

  float fDot = glm::dot(v3Diff, m_v3ViewDir);

  // current segment center in front
  if (fDot >= 0.0)
  {
    // move there
    v3ReturnPoint = v3CurSegCenter;
    return m_pCurSegment;
  }

  // try to find a segment in direction of the drone's view
  MapSegment* pNeighbor = m_pCurSegment->GetNeighbor(m_eDirection);
  if (pNeighbor != nullptr &&
      pNeighbor->GetNeighbors().size() > 1)  // do not fly into dead ends
  {
    // move to the neighor's center
    glm::vec2 v2NeigCtr = pNeighbor->GetPosition();
    glm::vec3 v3TargetPos(v2NeigCtr[0], 0.0f, v2NeigCtr[1]);
    v3ReturnPoint = v3TargetPos;
    return pNeighbor;
  }

  // no neighbor found, go to current segment's center
  v3ReturnPoint = v3CurSegCenter;
  return m_pCurSegment;
}


void AI::SetDroneLightColor(glm::vec3 v3LightCol)
{
  m_pDroneLight->SetLightIntensity(v3LightCol);
}


bool AI::IsSightBlockedBySegmentGeometry()
{
  // player not stored
  if (m_pPlayer == nullptr)
  {
    tc::err << "AI: No player stored." << tc::endl;
    return false;
  }

  // get the mesh of the AI's segment
  MeshNode* pCurrMapMeshNode_AI = GetCurrentMapMeshNode();
  if (pCurrMapMeshNode_AI == nullptr)
  {
    tc::err << "AI: No current map mesh node stored for AI." << tc::endl;
    return false;
  }

  // get the mesh of the player's segment
  MeshNode* pCurrMapMeshNode_Player = m_pPlayer->GetCurrentMapMeshNode();
  if (pCurrMapMeshNode_Player == nullptr)
  {
    tc::war << "AI: No current map mesh node stored for Player. Ignore, when in cheater mode." << tc::endl;
    return false;
  }

  // positions
  glm::vec3 v3PlayerPos = m_pPlayer->getPlayerPosition();
  glm::vec3 v3AiPos = m_v3Position;

  // AI cannot spot around corners at all
  if (AI_CANNOT_VIEW_THROUGH_EMPTY_SPACE_IN_CORNERS)
  {
    v3PlayerPos.y = HEIGHT_FOR_COL_DET_WALLS + MAP_OFFSET;
    v3AiPos.y = HEIGHT_FOR_COL_DET_WALLS + MAP_OFFSET;
  }

  // put all meshes to be checked in vector and use for loop
  std::vector<MeshNode*> vecMeshNodes;
  vecMeshNodes.push_back(pCurrMapMeshNode_AI);
  vecMeshNodes.push_back(pCurrMapMeshNode_Player);

  for (size_t j = 0; j < vecMeshNodes.size(); ++j)
  {
    MeshNode* pCurrMapMeshNode = vecMeshNodes[j];

    // get mesh
    Mesh* pMesh = pCurrMapMeshNode->getFirstStoredMesh();
    if (pMesh == nullptr)
    {
      tc::err << "AI: Cannot get MapSegment Mesh." << tc::endl;
      return false;
    }

    // transform the mesh according to its position and orientation
    glm::mat4 m4Transform = pCurrMapMeshNode->getAncestorTransformation();
    // get vertices and transform
    std::vector<glm::vec3> vecVertices = pMesh->GetVertices();
    for (size_t i = 0; i < vecVertices.size(); ++i)
    {
      glm::vec4 v4TransVert = m4Transform * glm::vec4( vecVertices[i], 1.0 );
      vecVertices[i] = glm::vec3(v4TransVert[0], v4TransVert[1], v4TransVert[2]);
    }

    // get indices
    std::vector<unsigned int> vecIndices = pMesh->getFaces_indices();
    // if not given as index mesh, add indices
    if (vecIndices.size() == 0)
    {
      vecIndices.resize( vecVertices.size() );
      for (size_t i = 0; i < vecIndices.size(); ++i)
      {
        vecIndices[i] = i;
      }
    }

    // no proper indices
    if (vecIndices.size() % 3 != 0)
    {
      tc::err << "AI: Mesh indices not dividable by 3." << tc::endl;
      return false;
    }

    // test all faces for intersection
    for (size_t i = 0; i < vecIndices.size() / 3; ++i)
    {
      // face's vertices
      glm::vec3 v3Vec1 = vecVertices[ vecIndices[3*i] ];
      glm::vec3 v3Vec2 = vecVertices[ vecIndices[3*i + 1] ];
      glm::vec3 v3Vec3 = vecVertices[ vecIndices[3*i + 2] ];

      // face normal
      glm::vec3 v3Normal = glm::cross(v3Vec2 - v3Vec1, v3Vec3 - v3Vec1  );
      v3Normal = glm::normalize(v3Normal);

      // calculate intersection of the face with the line player - AI
      float fLambda = glm::dot( (v3Vec1 - v3AiPos), v3Normal ) / glm::dot( (v3PlayerPos - v3AiPos), v3Normal );

      // check if plane is between end points
      if (fLambda >= 1.0f || fLambda <= 0.0f)
      {
        continue;
      }

      // project on plane and test if projection is inside the triangle
      glm::vec3 v3Project = v3AiPos + fLambda * (v3PlayerPos - v3AiPos);

      // check if orthogonal projection of the player is inside the face
      // test all three edges, edge 1
      glm::vec3 v3Edge = v3Vec2 - v3Vec1;
      glm::vec3 v3Edge_n = glm::cross(v3Edge, v3Normal);
      // check if edge normal points towards third vertex
      if (glm::dot(v3Vec3 - v3Vec1, v3Edge_n) < 0.0f)
        v3Edge_n = -v3Edge_n;
      // test if point is outside the triangle
      if (glm::dot(v3Project - v3Vec1, v3Edge_n) < 0.0f)
      {
        // outside, no intersection
        continue;
      }

      // test all three edges, edge 2
      v3Edge = v3Vec3 - v3Vec2;
      v3Edge_n = glm::cross(v3Edge, v3Normal);
      // check if edge normal points towards third vertex
      if (glm::dot(v3Vec1 - v3Vec2, v3Edge_n) < 0.0f)
        v3Edge_n = -v3Edge_n;
      // test if point is outside the triangle
      if (glm::dot(v3Project - v3Vec2, v3Edge_n) < 0.0f)
      {
        // outside, no intersection
        continue;
      }

      // test all three edges, edge 3
      v3Edge = v3Vec1 - v3Vec3;
      v3Edge_n = glm::cross(v3Edge, v3Normal);
      // check if edge normal points towards third vertex
      if (glm::dot(v3Vec2 - v3Vec3, v3Edge_n) < 0.0f)
        v3Edge_n = -v3Edge_n;
      // test if point is outside the triangle
      if (glm::dot(v3Project - v3Vec3, v3Edge_n) < 0.0f)
      {
        // outside, no intersection
        continue;
      }

      // the face is blocking player and AI
      return true;
    } // next face

  } // next Map Mesh Node

  // no face is blocking
  return false;
}


bool AI::IsViewingInTargetDirection()
{
  // Get Target Direction.
  glm::vec3 v3TargetDir;

  if (m_eDirection == Direction::North)
    v3TargetDir = glm::vec3(0.0f, 0.0f, -1.0f);
  else if (m_eDirection == Direction::South)
    v3TargetDir = glm::vec3(0.0f, 0.0f, 1.0f);
  else if (m_eDirection == Direction::West)
    v3TargetDir = glm::vec3(-1.0f, 0.0f, 0.0f);
  else if (m_eDirection == Direction::East)
    v3TargetDir = glm::vec3(1.0f, 0.0f, 0.0f);

  // Check whether the Angle between Target Direction and current view direction
  // is smaller than 45 degrees
  float dot = glm::dot(m_v3ViewDir, v3TargetDir);
  return (dot > 0.707f);
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/