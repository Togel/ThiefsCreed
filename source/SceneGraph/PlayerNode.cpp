/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "PlayerNode.h"

#include "../Defines.h"
#include "../TC.h"
#include "TransformNode.h"
#include "../Map/Map.h"
#include "../Map/MapSegment.h"

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

PlayerNode::PlayerNode(Map* p_map)
  : m_pMap(p_map)
{
}

PlayerNode::~PlayerNode()
{
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

MapSegment* PlayerNode::GetCurrentMapSegment()
{
  // no map given
  if (m_pMap == nullptr)
    return nullptr;

  int xInd;
  int yInd;
  getMapIndices(xInd, yInd);

  m_pCurSegment = m_pMap->getMapSegment(xInd, yInd);

  return m_pCurSegment;
}


MeshNode* PlayerNode::GetCurrentMapMeshNode()
{
  // no map given
  if (m_pMap == nullptr)
    return nullptr;

  int xInd;
  int yInd;
  getMapIndices(xInd, yInd);

  return m_pMap->getMeshNode(xInd, yInd);
}


void PlayerNode::Translate(glm::vec3 p_translation)
{
  // get CameraTransformNode, parent of PlayerNode
  Node* pParent = GetParent();
  if (pParent == nullptr)
  {
    tc::war << "PlayerNode does not have a parent." << tc::endl;
    return;
  }

  if(pParent->GetNodeType() == "TransformNode"
     || pParent->GetNodeType() == "CameraTransformNode")
  {
    TransformNode* pTransParent = dynamic_cast<TransformNode*>(pParent);
    pTransParent->Translate(p_translation);
  }
}


glm::vec3 PlayerNode::getPlayerPosition()
{
  return m_playerPos;
}


glm::vec2 PlayerNode::getPlayerPosition_2D()
{
  return m_playerPos_2D;
}


void PlayerNode::Update(Timer* pTime)
{
  // get player transformation
  glm::mat4 ModelMatrix = getAncestorTransformation();

  // set new player position
  glm::vec4 pos = ModelMatrix[3];
  m_playerPos = glm::vec3(pos[0], pos[1], pos[2]);
  m_playerPos_2D = glm::vec2(pos[0], pos[2]); // only x and z
}


void PlayerNode::getMapIndices(int& xInd, int& yInd)
{
  // shift player, s.t. for position at outer corner of the map he will end up at (0,0)
  glm::vec2 shiftPos = m_playerPos_2D + glm::vec2(SEGMENT_WIDTH/2.0f, SEGMENT_WIDTH/2.0f);

  // x-index
  float xInd_ = shiftPos[0] / SEGMENT_WIDTH;
  xInd = int(xInd_);

  // y-index
  float yInd_ = shiftPos[1] / SEGMENT_WIDTH;
  yInd = int(yInd_);

  // test for extrem cases
  if (xInd < 0) xInd = 0;
  if (yInd < 0) yInd = 0;
  if (xInd >= MapWidth) xInd = MapWidth - 1;
  if (yInd >= MapHeight) yInd = MapHeight - 1;
}


/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/