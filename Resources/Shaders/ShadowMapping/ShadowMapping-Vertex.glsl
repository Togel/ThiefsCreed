#version 420 core

layout(location = 0) in vec3 Position;
layout(location = 1) in vec3 Normal;
layout(location = 2) in vec2 TexCoord;
layout(location = 3) in vec3 Tangent;
layout(location = 4) in vec3 Bitangent;

uniform mat4 matModel;
uniform mat4 matInvTransModel;

layout(std140) uniform Camera{
  mat4 matView;
  mat4 matProjection;
  mat4 matInvTransView;
};

out vec3 vNormal;

void main()
{
  vNormal = (matInvTransView * matInvTransModel * vec4(Normal, 0.0)).xyz;

  gl_Position = matProjection * matView * matModel * vec4(Position, 1.0);
}


