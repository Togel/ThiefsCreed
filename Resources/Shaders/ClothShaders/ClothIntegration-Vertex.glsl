#version 420

layout(location = 0) in vec4 position_mass;
layout(location = 1) in vec3 inNormal;
// 2 = Tex
// 3 = Tangent
// 4 = Bitangent
layout(location = 5) in vec3 velocity;
layout(location = 6) in ivec4 connection;
layout(location = 7) in ivec4 connection2;

uniform samplerBuffer tex_position;

out vec4 tf_position_mass;
out vec3 tf_velocity;
out vec3 tf_normal;

// timestep
uniform float t = 0.07;

// spring constant
uniform float k_horizontal = 400.0;
uniform float k_vertical = 400.0;
uniform float k_shearing = 400.0;

// gravity
const vec3 gravity = vec3(0.0, -9.81, 0.0);

// dampening
uniform float c = 30.0;

// rest lenght of springs
uniform float rest_length_vertical = 0.88;
uniform float rest_length_horizontal = 0.88;
uniform float rest_length_shearing = 1.2445;

// vector to the player
uniform vec3 vecToPlayer;

// playerSize
uniform float playerSize = 10.0;


void main(void)
{

	
	vec3 p = position_mass.xyz;
	float m = position_mass.w;
	vec3 u = velocity;
	vec3 F = gravity * m - c * u; // Force on the mass
	bool fixed_node = true; // 'is this node updated'

  if (connection[0] != -1)
  {
    vec3 q = texelFetch(tex_position, connection[0]).xyz;
    vec3 d = q - p;
    float x = length(d);
    F += -k_horizontal * (rest_length_horizontal - x) * normalize(d);

    fixed_node = false;
  }

  if (connection[2] != -1)
  {
    vec3 q = texelFetch(tex_position, connection[2]).xyz;
    vec3 d = q - p;
    float x = length(d);
    F += -k_horizontal * (rest_length_horizontal - x) * normalize(d);

    fixed_node = false;
  }

  if (connection[1] != -1)
  {
    vec3 q = texelFetch(tex_position, connection[1]).xyz;
    vec3 d = q - p;
    float x = length(d);
    F += -k_vertical * (rest_length_vertical - x) * normalize(d);

    fixed_node = false;
  }
  
  if (connection[3] != -1)
  {
    vec3 q = texelFetch(tex_position, connection[3]).xyz;
    vec3 d = q - p;
    float x = length(d);
    F += -k_vertical * (rest_length_vertical - x) * normalize(d);

    fixed_node = false;
  }

	for(int i = 0; i < 4; i++)
	{

		if(connection2[i] != -1)
		{
			vec3 q = texelFetch(tex_position, connection2[i]).xyz;
			vec3 d = q - p;
			float x = length(d);
			F += -k_shearing * (rest_length_shearing - x) * normalize(d);

			fixed_node = false;
		}
	}


  // extend player down to the floor
  vec3 vecToPlayerMod = vecToPlayer;
  if (p[1] < vecToPlayerMod[1])
  {
    vecToPlayerMod[1] = p[1];
  }

  // force introduced by the player
  vec3 particleToPlayer = vecToPlayerMod - p;
  float distToPlayer = length(particleToPlayer);

  if(distToPlayer < playerSize )
  {
    F += 3000.0f * ((playerSize - distToPlayer)/playerSize) * normalize(-1 * particleToPlayer);
  }


	if(fixed_node)
	{
		F = vec3(0.0);
	}

	vec3 a = F / m;

	vec3 s = u * t + 0.5 * a * t * t;

	vec3 v = u + a * t;

	s = clamp(s, vec3(-25.0), vec3(25.0));

	vec3 pos = p + s;
	tf_velocity = v;



	tf_position_mass = vec4(pos, m);
	

	vec3 normal = vec3(0.0);
	int faceCounter = 0;
	
	if(connection[0] != -1 && connection[3] != -1)
	{
		vec3 p = texelFetch(tex_position, connection[0]).xyz;
		vec3 q = texelFetch(tex_position, connection[3]).xyz;
		normal += normalize(cross(q-pos,p-pos));
		faceCounter++;
	}
	if(connection[3] != -1 && connection2[2] != -1)
	{
		vec3 p = texelFetch(tex_position, connection[3]).xyz;
		vec3 q = texelFetch(tex_position, connection2[2]).xyz;
		normal += normalize(cross(q-pos,p-pos));
		faceCounter++;
	}
	if(connection2[2] != -1 && connection[2] != -1)
	{
		vec3 p = texelFetch(tex_position, connection2[2]).xyz;
		vec3 q = texelFetch(tex_position, connection[2]).xyz;
		normal += normalize(cross(q-pos,p-pos));
		faceCounter++;
	}
	if(connection[2] != -1 && connection2[1] != -1)
	{
		vec3 p = texelFetch(tex_position, connection[2]).xyz;
		vec3 q = texelFetch(tex_position, connection2[1]).xyz;
		normal += normalize(cross(q-pos,p-pos));
		faceCounter++;
	}
	if(connection[1] != -1 && connection2[0] != -1)
	{
		vec3 p = texelFetch(tex_position, connection[1]).xyz;
		vec3 q = texelFetch(tex_position, connection2[0]).xyz;
		normal += normalize(cross(q-pos,p-pos));
		faceCounter++;
	}
	if(connection2[0] != -1 && connection[0] != -1)
	{
		vec3 p = texelFetch(tex_position, connection2[0]).xyz;
		vec3 q = texelFetch(tex_position, connection[0]).xyz;
		normal += normalize(cross(q-pos,p-pos));
		faceCounter++;
	}
	
	normal = normal / faceCounter;

	tf_normal = normalize(normal);
}
