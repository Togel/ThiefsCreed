#ifndef _COLLISION_HANDLING_
#define _COLLISION_HANDLING_

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "glm.hpp"

#include <vector>
/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/
class PlayerNode;
class ClothNode;
class Node;
class AI;
class MapSegment;
class MeshNode;

/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class CollisionHandling
{
public:
  /*
  * Constructor.
  */
  CollisionHandling();

  /*
  * Destructor.
  */
  ~CollisionHandling();


  /*
   * Sets the Player Node.
   */
  void setPlayerNode(PlayerNode* p_player);

  /*
   * Sets the sceneGraph root node
   */
  void setRootNode(Node* pRootNode);

  /*
   * Performs all collision Handling for the scene.
   * Calls the respective subroutines for the individual
   * tests.
   */
  void performCollisionHandling();


private:

  PlayerNode* m_player;
  Node* m_pRootNode;

  /*
   * Performs the collision handling for the player
   * and the map, i.e. prevents the player from moving
   * through map segment walls.
   */
  void performCH_player_map();

  /*
   * Performs the collision handling for the AI drones
   * and the map, i.e. prevents the drones from moving
   * into map segment walls.
   */
  void performCH_AI_map(std::vector<AI*>& vecAINodes);

  /*
   * Performs the collision handling for any object, represented as sphere for intersection,
   * and the map.
   * @param v3Point The position of the object.
   * @param currMapSeg The current Map Segment of the object.
   * @param currMapMeshNode The Mesh Node related to the Mesh Segment.
   * @param distanceToKeep The minimal distance to the wall, which has to be enforced.
   * @param v3Push Output parameter: States the necessary translation, which has to be applied
   * to the object s.t. no collision occurs.
   * @return True iff a collision was detected.
   */
  bool performCH_point_map(glm::vec3 v3Point, MapSegment* currMapSeg, MeshNode* currMapMeshNode,
                           float distanceToKeep, glm::vec3& v3Push);

  /*
   * Performs the collision handling for the player with the cloth
   * This informs the cloth about the relative player position
   */
  void performCH_player_cloth();

  /*
   * Performs the collision handling for the player and the
   * AI drones. The Drones can be pushed away by the player.
   */
  void performCH_player_AI(std::vector<AI*>& vecAINodes, bool pushPlayer);

  /*
   * Performs the collision handling among the AI drones.
   */
  void performCH_AI_AI(std::vector<AI*>& vecAINodes);

  /*
   * Traverses the scene graph and collects all cloth nodes
   */
  template<typename NodeType>
  void collectNodes(Node* pNode, std::vector<NodeType*>& vecNodes);


};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/
#endif