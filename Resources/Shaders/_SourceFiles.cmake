set( RelativeDir "Resources/Shaders" )
set( RelativeSourceGroup "ResourceFiles\\Shaders" )

set(Directories
  ClothShaders
  DeferredShading
  ShadowMapping
  Skybox
)

set(DirFiles
  BlinnPhong.glsl
  CookTorrance.glsl
  _SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )
	
set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()

source_group( ${RelativeSourceGroup} FILES ${LocalSourceGroupFiles} )

foreach( subdir ${Directories})
	include("${RelativeDir}/${subdir}/_SourceFiles.cmake")
endforeach()

set( RelativeDir "Resources")

