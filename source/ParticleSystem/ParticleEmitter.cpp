/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "ParticleEmitter.h"
#include <stdlib.h>

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

ParticleEmitter::ParticleEmitter()
{
}

ParticleEmitter::~ParticleEmitter()
{
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

void ParticleEmitter::SetLifeTime(float fMin, float fMax)
{
  m_fParticleMinLifeTime = fMin;
  m_fParticleMaxLifeTime = fMax;
}

void ParticleEmitter::EmitParticle(Particle* pParticle)
{
}

float ParticleEmitter::GetRandomLifeTime()
{
  float fLow   = m_fParticleMinLifeTime;
  float fHeigh = m_fParticleMaxLifeTime;

  return fLow + static_cast<float>(rand()) / static_cast<float>(RAND_MAX / (fHeigh - fLow));
}

float ParticleEmitter::GetAverageLifeTime()
{
  return (m_fParticleMinLifeTime + m_fParticleMaxLifeTime) / 2.0f;
}

void ParticleEmitter::SetPosition(glm::vec3 v3Pos)
{
  m_v3Position = v3Pos;
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/