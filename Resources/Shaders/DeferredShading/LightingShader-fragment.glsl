#version 420 core

// 0 = position
// 1 = diffuse color
// 2 = normal
// 3 = texcoord
layout(binding = 0) uniform sampler2D texture_position;
layout(binding = 1) uniform sampler2D texture_diffuseColor;
layout(binding = 2) uniform sampler2D texture_normal;
layout(binding = 3) uniform sampler2D texture_texCoord;
layout(binding = 4) uniform sampler2D texture_posWorld;

struct LightParams {
  vec3 lightPosition;
  vec3 lightDirection;
  vec3 lightIntensity;
  float lightConeAngle;
  float lightAmbientCoefficient;
  float lightAttenuation;
  float lightType;
};

uniform LightParams inLight;

in vec2 TexCoords;

out vec4 color;

vec3 ComputeLight(LightParams light, vec3 surfaceColor, vec3 normal, vec3 surfacePosCam, vec3 surfacePosWorld, vec3 surfaceToCamera);

void main() {
  vec3 normal = texture2D(texture_normal, TexCoords).xyz;

  if (normal == vec3(0.0, 0.0, 0.0))
  {
    discard;
  }

  vec3 posCamSpace = texture2D(texture_position, TexCoords).xyz;
  vec3 surfaceColor = texture2D(texture_diffuseColor, TexCoords).rgb;
  vec3 posWorld = texture2D(texture_posWorld, TexCoords).xyz;

  float shininess = 16.0;

  vec3 viewDir = normalize(-posCamSpace);

 vec3 v3Color = ComputeLight(inLight, surfaceColor, normal, posCamSpace, posWorld, viewDir);

  vec3 gamma = vec3(1.0 / 2.2);
  color = vec4(pow(v3Color, gamma), 1.0);

  //color = vec4(vec3(texture(texture_shadows, TexCoords).x),1.0
  //color = vec4(v3Color, 1.0);
}
