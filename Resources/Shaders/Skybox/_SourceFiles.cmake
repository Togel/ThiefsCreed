set( RelativeDir "Resources/Shaders/Skybox" )
set( RelativeSourceGroup "ResourceFiles\\Shaders\\Skybox" )

set(Directories
 
)

set(DirFiles
  SkyBoxShader-Fragment.glsl
  SkyBoxShader-Vertex.glsl
  _SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )
	
set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()

source_group( ${RelativeSourceGroup} FILES ${LocalSourceGroupFiles} )

foreach( subdir ${Directories})
	include("${RelativeDir}/${subdir}/_SourceFiles.cmake")
endforeach()

set( RelativeDir "Resources/Shaders")


