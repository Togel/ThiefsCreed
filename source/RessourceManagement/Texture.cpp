/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "Texture.h"
#include "Shader.h"
#include <sstream>

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

Texture::Texture(GLuint iTexID, std::string sPath, GLenum pTextureType)
{
  m_iTexID = iTexID;
  m_sPath  = sPath;
  m_eTextureType = pTextureType;
}

Texture::~Texture()
{
  glDeleteTextures(1, &m_iTexID);

  m_iTexID = 0;
  m_sPath  = "";
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

GLuint Texture::GetID()
{
  return m_iTexID;
}

std::string Texture::GetPath()
{
  return m_sPath;
}

void Texture::Bind(GLenum textureUnit)
{
  glActiveTexture(textureUnit);
  glBindTexture(m_eTextureType, m_iTexID);
}

void Texture::Unbind()
{
  glBindTexture(m_eTextureType, 0);
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/